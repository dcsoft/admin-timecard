﻿using System.Windows.Forms;
using Telerik.WinControls.Enumerations;


namespace AdminTimecard
{
    partial class FormJobReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.radLabelHeading = new Telerik.WinControls.UI.RadLabel();
            this.lbJobs = new Telerik.WinControls.UI.RadListBox();
            this.jobsAllBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.winployDataSet = new AdminTimecard.winployDataSet();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCreateReport = new Telerik.WinControls.UI.RadButton();
            this.btnExport = new Telerik.WinControls.UI.RadButton();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.jobsAllTableAdapter = new AdminTimecard.winployDataSetTableAdapters.JobsAllTableAdapter();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radioSortCompany = new Telerik.WinControls.UI.RadRadioButton();
            this.radioSortManager = new Telerik.WinControls.UI.RadRadioButton();
            this.radioSortContractor = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobsAllBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortContractor)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabelHeading
            // 
            this.radLabelHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelHeading.Location = new System.Drawing.Point(15, 64);
            this.radLabelHeading.Name = "radLabelHeading";
            this.radLabelHeading.Size = new System.Drawing.Size(159, 17);
            this.radLabelHeading.TabIndex = 1;
            this.radLabelHeading.Text = "Please click Create Report";
            // 
            // lbJobs
            // 
            this.lbJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbJobs.DataSource = this.jobsAllBindingSource;
            this.lbJobs.DisplayMember = "JobId";
            this.lbJobs.Location = new System.Drawing.Point(15, 86);
            this.lbJobs.Name = "lbJobs";
            this.lbJobs.Size = new System.Drawing.Size(782, 411);
            this.lbJobs.TabIndex = 2;
            this.lbJobs.ValueMember = "JobId";
            this.lbJobs.ItemDataBound += new Telerik.WinControls.UI.ItemDataBoundEventHandler(this.lbJobs_ItemDataBound);
            // 
            // jobsAllBindingSource
            // 
            this.jobsAllBindingSource.DataMember = "JobsAll";
            this.jobsAllBindingSource.DataSource = this.winployDataSet;
            // 
            // winployDataSet
            // 
            this.winployDataSet.DataSetName = "winployDataSet";
            this.winployDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnCreateReport);
            this.groupBox2.Controls.Add(this.btnExport);
            this.groupBox2.Controls.Add(this.datePicker);
            this.groupBox2.Controls.Add(this.radLabel4);
            this.groupBox2.Location = new System.Drawing.Point(15, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(782, 51);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Show active jobs as of";
            // 
            // btnCreateReport
            // 
            this.btnCreateReport.Location = new System.Drawing.Point(187, 20);
            this.btnCreateReport.Name = "btnCreateReport";
            this.btnCreateReport.Size = new System.Drawing.Size(115, 23);
            this.btnCreateReport.TabIndex = 2;
            this.btnCreateReport.Text = "Create Report";
            this.btnCreateReport.ThemeName = "Office2007Black";
            this.btnCreateReport.Click += new System.EventHandler(this.btnCreateReport_Click);
            // 
            // btnExport
            // 
            this.btnExport.Enabled = false;
            this.btnExport.Location = new System.Drawing.Point(308, 20);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(109, 23);
            this.btnExport.TabIndex = 3;
            this.btnExport.Text = "Export Report ...";
            this.btnExport.ThemeName = "Office2007Black";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // datePicker
            // 
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(86, 20);
            this.datePicker.MaxDate = new System.DateTime(2049, 12, 31, 0, 0, 0, 0);
            this.datePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(95, 21);
            this.datePicker.TabIndex = 1;
            this.datePicker.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(6, 20);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(76, 17);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Report Date";
            // 
            // jobsAllTableAdapter
            // 
            this.jobsAllTableAdapter.ClearBeforeFill = true;
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(12, 503);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(47, 17);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Sort by";
            // 
            // radioSortCompany
            // 
            this.radioSortCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioSortCompany.Location = new System.Drawing.Point(154, 503);
            this.radioSortCompany.Name = "radioSortCompany";
            this.radioSortCompany.Size = new System.Drawing.Size(73, 18);
            this.radioSortCompany.TabIndex = 2;
            this.radioSortCompany.Text = "Company";
            this.radioSortCompany.ThemeName = "Office2007Black";
            this.radioSortCompany.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radioSortCompany_ToggleStateChanged);
            // 
            // radioSortManager
            // 
            this.radioSortManager.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioSortManager.Location = new System.Drawing.Point(233, 503);
            this.radioSortManager.Name = "radioSortManager";
            this.radioSortManager.Size = new System.Drawing.Size(69, 18);
            this.radioSortManager.TabIndex = 3;
            this.radioSortManager.Text = "Manager";
            this.radioSortManager.ThemeName = "Office2007Black";
            this.radioSortManager.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radioSortManager_ToggleStateChanged);
            // 
            // radioSortContractor
            // 
            this.radioSortContractor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioSortContractor.Location = new System.Drawing.Point(70, 503);
            this.radioSortContractor.Name = "radioSortContractor";
            this.radioSortContractor.Size = new System.Drawing.Size(78, 18);
            this.radioSortContractor.TabIndex = 1;
            this.radioSortContractor.Text = "Contractor";
            this.radioSortContractor.ThemeName = "Office2007Black";
            this.radioSortContractor.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radioSortContractor.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radioSortContractor_ToggleStateChanged);
            // 
            // FormJobReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radioSortCompany);
            this.Controls.Add(this.lbJobs);
            this.Controls.Add(this.radioSortManager);
            this.Controls.Add(this.radLabelHeading);
            this.Controls.Add(this.radioSortContractor);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormJobReport";
            this.Text = "FormJobReport";
            this.Load += new System.EventHandler(this.FormJobReport_Load);
            this.VisibleChanged += new System.EventHandler(this.FormJobEmail_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobsAllBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortContractor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadListBox lbJobs;
        private winployDataSet winployDataSet;
        private System.Windows.Forms.BindingSource jobsAllBindingSource;
        private AdminTimecard.winployDataSetTableAdapters.JobsAllTableAdapter jobsAllTableAdapter;
        private System.Windows.Forms.GroupBox groupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private DateTimePicker datePicker;
        private Telerik.WinControls.UI.RadButton btnExport;
        private Telerik.WinControls.UI.RadButton btnCreateReport;
        private Telerik.WinControls.UI.RadLabel radLabelHeading;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadRadioButton radioSortCompany;
        private Telerik.WinControls.UI.RadRadioButton radioSortManager;
        private Telerik.WinControls.UI.RadRadioButton radioSortContractor;
    }
}