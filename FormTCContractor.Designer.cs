﻿using System.Windows.Forms;


namespace AdminTimecard
{
    partial class FormTCContractor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.cbiInvoicedBilled = new Telerik.WinControls.UI.RadComboBoxItem();
            this.cbiApproved = new Telerik.WinControls.UI.RadComboBoxItem();
            this.cbiSubmitted = new Telerik.WinControls.UI.RadComboBoxItem();
            this.cbStatus = new Telerik.WinControls.UI.RadComboBox();
            this.cbBilled = new Telerik.WinControls.UI.RadComboBoxItem();
            this.cbInvoiced = new Telerik.WinControls.UI.RadComboBoxItem();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.cbContractors = new Telerik.WinControls.UI.RadComboBox();
            this.jobsAllBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.winployDataSet = new AdminTimecard.winployDataSet();
            this.lbTimecards = new Telerik.WinControls.UI.RadListBox();
            this.timesheetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnDelTimecard = new Telerik.WinControls.UI.RadButton();
            this.tbWorkDetails = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabelComments = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbComments = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.btnCreateTimecard = new Telerik.WinControls.UI.RadButton();
            this.jobsAllTableAdapter = new AdminTimecard.winployDataSetTableAdapters.JobsAllTableAdapter();
            this.timesheetsTableAdapter = new AdminTimecard.winployDataSetTableAdapters.TimesheetsTableAdapter();
            this.cbSaved = new Telerik.WinControls.UI.RadComboBoxItem();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbContractors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobsAllBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTimecards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesheetsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelTimecard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWorkDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateTimecard)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(7, 23);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(43, 17);
            this.radLabel4.TabIndex = 4;
            this.radLabel4.Text = "Status";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel5.Location = new System.Drawing.Point(6, 51);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(44, 17);
            this.radLabel5.TabIndex = 15;
            this.radLabel5.Text = "Hours:";
            // 
            // cbiInvoicedBilled
            // 
            this.cbiInvoicedBilled.CanFocus = false;
            this.cbiInvoicedBilled.Name = "cbiInvoicedBilled";
            this.cbiInvoicedBilled.Text = "InvoicedBilled (DONE)";
            // 
            // cbiApproved
            // 
            this.cbiApproved.CanFocus = false;
            this.cbiApproved.Name = "cbiApproved";
            this.cbiApproved.Text = "Approved";
            // 
            // cbiSubmitted
            // 
            this.cbiSubmitted.CanFocus = false;
            this.cbiSubmitted.Name = "cbiSubmitted";
            this.cbiSubmitted.Text = "Submitted";
            // 
            // cbStatus
            // 
            this.cbStatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbStatus.FormatString = "{0}";
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cbSaved,
            this.cbiSubmitted,
            this.cbiApproved,
            this.cbiInvoicedBilled,
            this.cbBilled,
            this.cbInvoiced});
            this.cbStatus.Location = new System.Drawing.Point(53, 23);
            this.cbStatus.Name = "cbStatus";
            // 
            // 
            // 
            this.cbStatus.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.cbStatus.Size = new System.Drawing.Size(458, 20);
            this.cbStatus.TabIndex = 5;
            this.cbStatus.TabStop = false;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.cbStatus_SelectedIndexChanged);
            // 
            // cbBilled
            // 
            this.cbBilled.Name = "cbBilled";
            this.cbBilled.Text = "Billed";
            // 
            // cbInvoiced
            // 
            this.cbInvoiced.Name = "cbInvoiced";
            this.cbInvoiced.Text = "Invoiced";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(411, 157);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(2, 2);
            this.radLabel11.TabIndex = 6;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel1.Location = new System.Drawing.Point(21, 8);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(67, 17);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Contractor";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel2.Location = new System.Drawing.Point(21, 35);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(139, 17);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Contractor\'s Timecards";
            // 
            // cbContractors
            // 
            this.cbContractors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbContractors.DataSource = this.jobsAllBindingSource;
            this.cbContractors.DisplayMember = "JobId";
            this.cbContractors.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbContractors.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbContractors.FormatString = "{0}";
            this.cbContractors.Location = new System.Drawing.Point(127, 8);
            this.cbContractors.MaxDropDownItems = 20;
            this.cbContractors.Name = "cbContractors";
            // 
            // 
            // 
            this.cbContractors.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.cbContractors.Size = new System.Drawing.Size(667, 20);
            this.cbContractors.TabIndex = 1;
            this.cbContractors.TabStop = false;
            this.cbContractors.ValueMember = "JobId";
            this.cbContractors.ItemDataBound += new Telerik.WinControls.UI.ItemDataBoundEventHandler(this.cbContractors_ItemDataBound);
            this.cbContractors.SelectedIndexChanged += new System.EventHandler(this.cbContractor_SelectedIndexChanged);
            // 
            // jobsAllBindingSource
            // 
            this.jobsAllBindingSource.DataMember = "JobsAll";
            this.jobsAllBindingSource.DataSource = this.winployDataSet;
            // 
            // winployDataSet
            // 
            this.winployDataSet.DataSetName = "winployDataSet";
            this.winployDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lbTimecards
            // 
            this.lbTimecards.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTimecards.DataSource = this.timesheetsBindingSource;
            this.lbTimecards.DisplayMember = "WeekEndingDate";
            this.lbTimecards.FormatString = "{0}";
            this.lbTimecards.Location = new System.Drawing.Point(21, 57);
            this.lbTimecards.Name = "lbTimecards";
            this.lbTimecards.Size = new System.Drawing.Size(238, 456);
            this.lbTimecards.TabIndex = 3;
            this.lbTimecards.ThemeName = "ERGTheme";
            this.lbTimecards.ValueMember = "TimesheetId";
            this.lbTimecards.SelectedIndexChanged += new System.EventHandler(this.lbTimecards_SelectedIndexChanged);
            this.lbTimecards.ItemDataBound += new Telerik.WinControls.UI.ItemDataBoundEventHandler(this.lbTimecards_ItemDataBound);
            // 
            // timesheetsBindingSource
            // 
            this.timesheetsBindingSource.DataMember = "Timesheets";
            this.timesheetsBindingSource.DataSource = this.winployDataSet;
            // 
            // btnDelTimecard
            // 
            this.btnDelTimecard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelTimecard.Location = new System.Drawing.Point(138, 519);
            this.btnDelTimecard.Name = "btnDelTimecard";
            this.btnDelTimecard.Size = new System.Drawing.Size(121, 27);
            this.btnDelTimecard.TabIndex = 5;
            this.btnDelTimecard.Text = "Delete Timecard";
            this.btnDelTimecard.Click += new System.EventHandler(this.btnDelTimecard_Click);
            // 
            // tbWorkDetails
            // 
            this.tbWorkDetails.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "WorkDetails", true));
            this.tbWorkDetails.Location = new System.Drawing.Point(274, 139);
            this.tbWorkDetails.Multiline = true;
            this.tbWorkDetails.Name = "tbWorkDetails";
            this.tbWorkDetails.ReadOnly = true;
            // 
            // 
            // 
            this.tbWorkDetails.RootElement.StretchVertically = true;
            this.tbWorkDetails.Size = new System.Drawing.Size(394, 320);
            this.tbWorkDetails.TabIndex = 1;
            this.tbWorkDetails.TabStop = false;
            // 
            // radTextBox1
            // 
            this.radTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "WorkDetails", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.radTextBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radTextBox1.Location = new System.Drawing.Point(5, 130);
            this.radTextBox1.Multiline = true;
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.ReadOnly = true;
            // 
            // 
            // 
            this.radTextBox1.RootElement.StretchVertically = true;
            this.radTextBox1.Size = new System.Drawing.Size(504, 131);
            this.radTextBox1.TabIndex = 17;
            this.radTextBox1.TabStop = false;
            this.radTextBox1.Text = "radTextBox1";
            // 
            // radLabelComments
            // 
            this.radLabelComments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelComments.Location = new System.Drawing.Point(5, 280);
            this.radLabelComments.Name = "radLabelComments";
            this.radLabelComments.Size = new System.Drawing.Size(66, 17);
            this.radLabelComments.TabIndex = 19;
            this.radLabelComments.Text = "Comments";
            this.radLabelComments.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.radLabelComments_MouseDoubleClick);
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(5, 108);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(77, 17);
            this.radLabel6.TabIndex = 20;
            this.radLabel6.Text = "Work Details";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbComments);
            this.groupBox1.Controls.Add(this.radLabel23);
            this.groupBox1.Controls.Add(this.radLabel14);
            this.groupBox1.Controls.Add(this.radLabel15);
            this.groupBox1.Controls.Add(this.radLabel22);
            this.groupBox1.Controls.Add(this.radLabel20);
            this.groupBox1.Controls.Add(this.radLabel19);
            this.groupBox1.Controls.Add(this.radLabel18);
            this.groupBox1.Controls.Add(this.radLabel7);
            this.groupBox1.Controls.Add(this.radLabel21);
            this.groupBox1.Controls.Add(this.radLabel16);
            this.groupBox1.Controls.Add(this.radLabel13);
            this.groupBox1.Controls.Add(this.radLabel12);
            this.groupBox1.Controls.Add(this.radLabel10);
            this.groupBox1.Controls.Add(this.radLabel9);
            this.groupBox1.Controls.Add(this.radLabel8);
            this.groupBox1.Controls.Add(this.radLabel6);
            this.groupBox1.Controls.Add(this.radLabelComments);
            this.groupBox1.Controls.Add(this.radLabel4);
            this.groupBox1.Controls.Add(this.radTextBox1);
            this.groupBox1.Controls.Add(this.radLabel5);
            this.groupBox1.Controls.Add(this.cbStatus);
            this.groupBox1.Controls.Add(this.radLabel17);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(278, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(516, 478);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Timecard";
            // 
            // tbComments
            // 
            this.tbComments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbComments.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "Comments", true));
            this.tbComments.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tbComments.Location = new System.Drawing.Point(7, 302);
            this.tbComments.Multiline = true;
            this.tbComments.Name = "tbComments";
            this.tbComments.ReadOnly = true;
            // 
            // 
            // 
            this.tbComments.RootElement.StretchVertically = true;
            this.tbComments.Size = new System.Drawing.Size(502, 170);
            this.tbComments.TabIndex = 24;
            this.tbComments.TabStop = false;
            // 
            // radLabel23
            // 
            this.radLabel23.AutoSize = false;
            this.radLabel23.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "HoursTotal", true));
            this.radLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel23.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel23.Location = new System.Drawing.Point(427, 74);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(48, 16);
            this.radLabel23.TabIndex = 19;
            this.radLabel23.Text = "88.88";
            this.radLabel23.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel14.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel14.Location = new System.Drawing.Point(323, 51);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(48, 16);
            this.radLabel14.TabIndex = 21;
            this.radLabel14.Text = "Sat";
            this.radLabel14.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel15.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel15.Location = new System.Drawing.Point(377, 51);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(48, 16);
            this.radLabel15.TabIndex = 22;
            this.radLabel15.Text = "Sun";
            this.radLabel15.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel22
            // 
            this.radLabel22.AutoSize = false;
            this.radLabel22.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "HoursSun", true));
            this.radLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel22.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel22.Location = new System.Drawing.Point(377, 74);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(44, 16);
            this.radLabel22.TabIndex = 18;
            this.radLabel22.Text = "88.88";
            this.radLabel22.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel20
            // 
            this.radLabel20.AutoSize = false;
            this.radLabel20.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "HoursSat", true));
            this.radLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel20.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel20.Location = new System.Drawing.Point(323, 74);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(48, 16);
            this.radLabel20.TabIndex = 17;
            this.radLabel20.Text = "88.88";
            this.radLabel20.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel19
            // 
            this.radLabel19.AutoSize = false;
            this.radLabel19.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "HoursFri", true));
            this.radLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel19.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel19.Location = new System.Drawing.Point(269, 74);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(48, 16);
            this.radLabel19.TabIndex = 17;
            this.radLabel19.Text = "88.88";
            this.radLabel19.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel18
            // 
            this.radLabel18.AutoSize = false;
            this.radLabel18.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "HoursThu", true));
            this.radLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel18.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel18.Location = new System.Drawing.Point(215, 74);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(48, 16);
            this.radLabel18.TabIndex = 17;
            this.radLabel18.Text = "88.88";
            this.radLabel18.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "HoursWed", true));
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel7.Location = new System.Drawing.Point(161, 74);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(48, 16);
            this.radLabel7.TabIndex = 17;
            this.radLabel7.Text = "88.88";
            this.radLabel7.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel21
            // 
            this.radLabel21.AutoSize = false;
            this.radLabel21.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "HoursMon", true));
            this.radLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel21.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radLabel21.Location = new System.Drawing.Point(53, 74);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(48, 16);
            this.radLabel21.TabIndex = 17;
            this.radLabel21.Text = "88.88";
            this.radLabel21.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel16
            // 
            this.radLabel16.AutoSize = false;
            this.radLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel16.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel16.Location = new System.Drawing.Point(427, 51);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(48, 16);
            this.radLabel16.TabIndex = 23;
            this.radLabel16.Text = "TOTAL";
            this.radLabel16.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel13.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel13.Location = new System.Drawing.Point(269, 51);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(48, 16);
            this.radLabel13.TabIndex = 20;
            this.radLabel13.Text = "Fri";
            this.radLabel13.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel12.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel12.Location = new System.Drawing.Point(215, 51);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(48, 16);
            this.radLabel12.TabIndex = 19;
            this.radLabel12.Text = "Thu";
            this.radLabel12.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel10.Location = new System.Drawing.Point(161, 51);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(48, 16);
            this.radLabel10.TabIndex = 18;
            this.radLabel10.Text = "Wed";
            this.radLabel10.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel9.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel9.Location = new System.Drawing.Point(107, 51);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(48, 16);
            this.radLabel9.TabIndex = 17;
            this.radLabel9.Text = "Tue";
            this.radLabel9.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radLabel8.Location = new System.Drawing.Point(53, 51);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(48, 16);
            this.radLabel8.TabIndex = 16;
            this.radLabel8.Text = "Mon";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel17
            // 
            this.radLabel17.AutoSize = false;
            this.radLabel17.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.timesheetsBindingSource, "HoursTue", true));
            this.radLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel17.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel17.Location = new System.Drawing.Point(107, 74);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(48, 16);
            this.radLabel17.TabIndex = 16;
            this.radLabel17.Text = "88.88";
            this.radLabel17.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnCreateTimecard
            // 
            this.btnCreateTimecard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCreateTimecard.Location = new System.Drawing.Point(22, 519);
            this.btnCreateTimecard.Name = "btnCreateTimecard";
            this.btnCreateTimecard.Size = new System.Drawing.Size(110, 27);
            this.btnCreateTimecard.TabIndex = 7;
            this.btnCreateTimecard.Text = "Create Timecard";
            this.btnCreateTimecard.Visible = false;
            this.btnCreateTimecard.Click += new System.EventHandler(this.btnCreateTimecard_Click);
            // 
            // jobsAllTableAdapter
            // 
            this.jobsAllTableAdapter.ClearBeforeFill = true;
            // 
            // timesheetsTableAdapter
            // 
            this.timesheetsTableAdapter.ClearBeforeFill = true;
            // 
            // cbSaved
            // 
            this.cbSaved.CanFocus = false;
            this.cbSaved.Name = "cbSaved";
            this.cbSaved.Text = "Saved";
            // 
            // FormTCContractor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.btnCreateTimecard);
            this.Controls.Add(this.btnDelTimecard);
            this.Controls.Add(this.lbTimecards);
            this.Controls.Add(this.cbContractors);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormTCContractor";
            this.Text = "FormTCContractor";
            this.VisibleChanged += new System.EventHandler(this.FormTCContractor_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbContractors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobsAllBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTimecards)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesheetsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelTimecard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWorkDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateTimecard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private BindingSource jobsAllBindingSource;
        private BindingSource timesheetsBindingSource;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadComboBoxItem cbiInvoicedBilled;
        private Telerik.WinControls.UI.RadComboBoxItem cbiApproved;
        private Telerik.WinControls.UI.RadComboBoxItem cbiSubmitted;
        private Telerik.WinControls.UI.RadComboBox cbStatus;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadComboBox cbContractors;
        private Telerik.WinControls.UI.RadListBox lbTimecards;
        private Telerik.WinControls.UI.RadButton btnDelTimecard;
        private Telerik.WinControls.UI.RadTextBox tbWorkDetails;
        private winployDataSet winployDataSet;
        private AdminTimecard.winployDataSetTableAdapters.JobsAllTableAdapter jobsAllTableAdapter;
        private AdminTimecard.winployDataSetTableAdapters.TimesheetsTableAdapter timesheetsTableAdapter;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadLabel radLabelComments;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private GroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadTextBox tbComments;
        private Telerik.WinControls.UI.RadComboBoxItem cbBilled;
        private Telerik.WinControls.UI.RadComboBoxItem cbInvoiced;
        private Telerik.WinControls.UI.RadButton btnCreateTimecard;
        private Telerik.WinControls.UI.RadComboBoxItem cbSaved;
    }
}