﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data;


namespace AdminTimecard
{
    public partial class FormQBBill : Form
    {
        public FormQBBill()
        {
            InitializeComponent();
            Visible = false;

            // Scroll last entered status line into view
            lbStatus.AutoScroll = true;
        }

        private void FormQBBill_VisibleChanged(object sender, EventArgs e)
        {
            // this is called several times even when Visible hasn't changed
            if (Visible == m_lastVisible)
                return;

            m_lastVisible = Visible;

            if (!Visible)
                return;

            timesheetsTableAdapter.ConnectionString = Form1.ConnectionString;
            approvedTimesheetsTableAdapter.ConnectionString = Form1.ConnectionString;
            FillGrid();

            lbStatus.Items.Clear();
        }

        private void rlShowUninvoiced_Click(object sender, EventArgs e)
        {
            rlShowUnbilled.Enabled = true;
            rlShowUninvoiced.Enabled = false;

            rlShowingTitle.Text = "Uninvoiced Timecards";
            rlGenerateBills.Enabled = false;
            rlGenerateInvoices.Enabled = true;
            rlGenerateOneInvoice.Enabled = true;

            m_showUninvoiced = true;
            m_showUnbilled = false;
            FilterBilledInvoiced();
            InitBillInvoiceColumns();
        }

        private void rlShowUnbilled_Click(object sender, EventArgs e)
        {
            rlShowUnbilled.Enabled = false;
            rlShowUninvoiced.Enabled = true;

            rlShowingTitle.Text = "Unbilled Timecards";
            rlGenerateBills.Enabled = true;
            rlGenerateInvoices.Enabled = false;
            rlGenerateOneInvoice.Enabled = false;

            m_showUninvoiced = false;
            m_showUnbilled = true;
            FilterBilledInvoiced();
            InitBillInvoiceColumns();
        }

        private void rlSelectAll_Click(object sender, EventArgs e)
        {
            CheckVisible(true);
        }

        private void rlSelectNone_Click(object sender, EventArgs e)
        {
            CheckVisible(false);
        }

        private void radGridView1_ValueChanged(object sender, EventArgs e)
        {
            if ( !m_isFilling && (sender is RadCheckBoxEditor))       // checkbox was changed
            {
                // Stupid RadGridView requires loss of focus before it updates .Cells[0].Value to reflect checkbox!
                // Set focus to the form here so it loses focus, prior to reading .Cells[0].Value in UpdateCountText().
                rlNumSelected.Focus();

                OnVisibleCheckedTimecardsChanged();

                // Put back focus where it was
                radGridView1.Focus();
            }
        }

        private void rlGenerateBills_Click(object sender, EventArgs e)
        {
            lbStatus.Items.Clear();
            if (MessageBox.Show(string.Format("Are you sure you want to create QuickBooks bills for {0} timecard(s)?", NumCheckedTimecards),
                                "Confirm QuickBooks Action",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            AddStatus("Creating Bill(s)...");

            bool? lastTransactionOk = null;
            try
            {
                using (var invoiceGen = new QBInvoiceGenerator())
                {
                    foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
                    {
                        // Skip invisible or unchecked rows
                        if (!rowInfo.IsVisible || !(bool)rowInfo.Cells[0].Value)
                            continue;

                        rowInfo.IsSelected = true;  // highlight current item

                        // Skip this row if QB info is not set up
                        DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
                        winployDataSet.ApprovedTimesheetsRow approvedTimesheetRow = (winployDataSet.ApprovedTimesheetsRow)drv.Row;
                        if (!IsBillable(drv.Row))
                        {
                            AddStatus(string.Format("Skipping timecard because the Customer, Item, and/or Vendor is not mapped in QuickBooks.  {0} ({1} {2}), WE:  {3}",
                                                     drv["VendorName"], drv["ContractorFirstName"], drv["ContractorLastName"],
                                                     ((DateTime)drv["WeekEndingDate"]).ToShortDateString()));
                            continue;
                        }

                        AddStatus(string.Format("Creating Bill for {0} ({1} {2}), WE:  {3}",
                                                 drv["VendorName"], drv["ContractorFirstName"], drv["ContractorLastName"],
                                                 ((DateTime)drv["WeekEndingDate"]).ToShortDateString()));

                        string errorText;
                        lastTransactionOk = invoiceGen.CreateItemReceipt(out errorText,
                                                              drv["VendorQB"].ToString(),
                                                              drv["CustomerQB"].ToString(),
                                                              drv["ContractorQB"].ToString(),
                                                              drv["ContractorFirstName"] + " " +
                                                              drv["ContractorLastName"],
                                                              drv["HoursTotal"].ToString(),
                                                              (DateTime) drv["WeekEndingDate"]);

                        if (!string.IsNullOrEmpty(errorText))
                            AddStatus("  " + errorText);

                        if (lastTransactionOk == false)
                        {
                            // stop processing
                            break;
                        }

                        // Mark this timesheet as billed
                        AddStatus("  Marking Timecard as Billed...");
                        UpdateTimecardStatus(rowInfo, TimecardStage.SetBilled(approvedTimesheetRow.TimecardStatus), false);
                    }
                }
            }
            catch (Exception ex)
            {
                // Show error label with error message
                AddStatus(ex.Message);

                Program.ShowExceptionMessage(ex);
            }


            if (lastTransactionOk == true)  // Some timecards were successfully billed
            {
                AddStatus("Successfully created bill(s).");
            }
            else if (lastTransactionOk == false)
            {
                AddStatus("Stopped creating bills due to an error.");
                AddStatus("Please try again.");
            }

            // Filter out rows that were successfully billed
            FilterBilledInvoiced();

            OnVisibleCheckedTimecardsChanged();
        }

        private void rlGenerateInvoices_Click(object sender, EventArgs e)
        {
            lbStatus.Items.Clear();
            if (MessageBox.Show(string.Format("Are you sure you want to create QuickBooks invoices for {0} timecard(s)?", NumCheckedTimecards),
                                "Confirm QuickBooks Action",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            AddStatus("Creating Invoice(s)...");

            bool? lastTransactionOk = null;
            try
            {
                using (var invoiceGen = new QBInvoiceGenerator())
                {
                    foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
                    {
                        // Skip invisible or unchecked rows
                        if (!rowInfo.IsVisible || !(bool)rowInfo.Cells[0].Value)
                            continue;

                        rowInfo.IsSelected = true;  // highlight current item

                        // Skip this row if QB info is not set up
                        DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
                        winployDataSet.ApprovedTimesheetsRow timecard = (winployDataSet.ApprovedTimesheetsRow)drv.Row;
                        if (!IsInvoicable(drv.Row))
                        {
                            AddStatus(string.Format("Skipping timecard because the Customer and/or Item is not mapped in QuickBooks.  {0} ({1} {2}), WE:  {3}",
                                                     drv["VendorName"], drv["ContractorFirstName"], drv["ContractorLastName"],
                                                     ((DateTime)drv["WeekEndingDate"]).ToShortDateString()));
                            continue;
                        }


                        AddStatus(string.Format("Creating Invoice to {0} ({1} {2}), WE:  {3}",
                                                 drv["CustomerCompany"], drv["ContractorFirstName"],
                                                 drv["ContractorLastName"], ((DateTime)drv["WeekEndingDate"]).ToShortDateString()));

                        string errorText;
                        lastTransactionOk = invoiceGen.CreateInvoice(out errorText, timecard);

                        if (!string.IsNullOrEmpty(errorText))
                            AddStatus("  " + errorText);

                        if (lastTransactionOk == false)
                        {
                            // stop processing
                            break;
                        }

                        // Mark this timesheet as invoiced
                        AddStatus("  Marking Timecard as Invoiced...");
                        UpdateTimecardStatus(rowInfo, TimecardStage.SetInvoiced(timecard.TimecardStatus, ShouldBill(rowInfo)), false);
                    }
                }
            }
            catch (Exception ex)
            {
                // Show error label with error message
                AddStatus(ex.Message);

                Program.ShowExceptionMessage(ex);
            }


            if (lastTransactionOk == true)  // Some timecards were successfully invoiced
            {
                AddStatus("Successfully created invoice(s).");
            }
            else if (lastTransactionOk == false)
            {
                AddStatus("Stopped creating invoices due to an error.");
                AddStatus("Please try again.");
            }

            // Filter rows that were successfully invoiced
            FilterBilledInvoiced();

            OnVisibleCheckedTimecardsChanged();
        }

        private void rlGenerate_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void rlGenerateOneInvoice_Click(object sender, EventArgs e)
        {
            // Generate one invoice with all selected timecards as line items
            lbStatus.Items.Clear();
            if (MessageBox.Show(string.Format("Are you sure you want to create a SINGLE QuickBooks invoice for {0} timecard(s)?", NumCheckedTimecards),
                                "Confirm QuickBooks Action",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            // Show popup so user can temporarily map
            List<winployDataSet.ApprovedTimesheetsRow> approvedTimecards = new List<winployDataSet.ApprovedTimesheetsRow>();
            foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
            {
                // Skip invisible or unchecked rows
                if (!rowInfo.IsVisible || !(bool) rowInfo.Cells[0].Value)
                    continue;

                DataRowView drv = (DataRowView) rowInfo.DataBoundItem;
                winployDataSet.ApprovedTimesheetsRow approvedTimesheetRow = (winployDataSet.ApprovedTimesheetsRow) drv.Row;
                approvedTimecards.Add(approvedTimesheetRow);
            }

            FormQBMap mapForm = new FormQBMap();
            mapForm.setTimecards(approvedTimecards);
            if (mapForm.ShowDialog(this) != DialogResult.OK)
                return;


            // Abort if approvedTimecards are not all invoicable
            foreach (winployDataSet.ApprovedTimesheetsRow timecard in approvedTimecards)
            {
                if ( !IsInvoicable(timecard) )
                {
                    AddStatus(string.Format("Timecard remains uninvoicable, please try again and re-map to QuickBooks: {0} ({1} {2}), WE:  {3}",
                                             timecard["CustomerCompany"], timecard["ContractorFirstName"],
                                             timecard["ContractorLastName"], ((DateTime)timecard["WeekEndingDate"]).ToShortDateString()));

                    return;
                }
            }

            AddStatus("Creating Single Invoice...");

            bool lastTransactionOk = false;
            try
            {
                using (var invoiceGen = new QBInvoiceGenerator())
                {
                    string errorText;
                    lastTransactionOk = invoiceGen.CreateInvoice(out errorText, approvedTimecards);

                    if (!string.IsNullOrEmpty(errorText))
                        AddStatus("  " + errorText);

                    if (lastTransactionOk)
                    {
                        // Mark checked timesheet as invoiced
                        AddStatus("  Marking selected Timecards as Invoiced...");

                        lastTransactionOk = false;
                        foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
                        {
                            // Skip invisible or unchecked rows
                            if (!rowInfo.IsVisible || !(bool)rowInfo.Cells[0].Value)
                                continue;

                            DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
                            winployDataSet.ApprovedTimesheetsRow approvedTimesheetRow = (winployDataSet.ApprovedTimesheetsRow)drv.Row;
                            UpdateTimecardStatus(rowInfo, TimecardStage.SetInvoiced(approvedTimesheetRow.TimecardStatus, ShouldBill(rowInfo)), false);
                        }
                        lastTransactionOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // Show error label with error message
                AddStatus(ex.Message);

                Program.ShowExceptionMessage(ex);
            }


            if (lastTransactionOk)  // Some timecards were successfully invoiced
            {
                AddStatus("Successfully created single invoice.");
            }

            // Filter rows that were successfully invoiced
            FilterBilledInvoiced();

            OnVisibleCheckedTimecardsChanged();
        }

        private void rlSetStatusBilled_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(string.Format("Are you sure you want to mark {0} timecard(s) as Billed?\nYou will need to manually create a QuickBooks Bill for these timecards!\nNOTE:  The timecard(s) Invoiced status will not be changed.", NumCheckedTimecards),
                                "Confirm change",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            try
            {
                foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
                {
                    // Skip invisible or unchecked rows
                    if (!rowInfo.IsVisible || !(bool)rowInfo.Cells[0].Value)
                        continue;

                    DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
                    winployDataSet.ApprovedTimesheetsRow approvedTimesheetRow = (winployDataSet.ApprovedTimesheetsRow)drv.Row;
                    AddStatus(string.Format("Marking timecard as billed: {0} ({1} {2}), WE:  {3}",
                                             drv["CustomerCompany"], drv["ContractorFirstName"],
                                             drv["ContractorLastName"], ((DateTime)drv["WeekEndingDate"]).ToShortDateString()));


                    UpdateTimecardStatus(rowInfo, TimecardStage.SetBilled(approvedTimesheetRow.TimecardStatus), true);
                }
            }
            catch (System.Exception ex)
            {
                // Show error label with error message
                AddStatus(ex.Message);

                Program.ShowExceptionMessage(ex);
            }

            AddStatus("Successfully marked timecard(s) as Invoiced.");

            FilterBilledInvoiced();
            OnVisibleCheckedTimecardsChanged();
        }

        private void FillGrid()
        {
            // Loading the Approved Timesheets data source causes the grid to fill
            // Force loading so any changes made in other pages to the approved 
            // timesheets are shown
            m_isFilling = true;
            approvedTimesheetsTableAdapter.Fill(winployDataSet.ApprovedTimesheets);

            // Set billable and invoiceable columns
            InitBillInvoiceColumns();

            // Hide filtered rows
            FilterBilledInvoiced();

            // Set all checkboxes to false; if uninitialized it is in an indeterminate state that won't return
            // a bool value!
            foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
                rowInfo.Cells[0].Value = false;

            m_isFilling = false;
        }

        private void InitBillInvoiceColumns()
        {
            foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
            {
                DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
                DataRow row = drv.Row;

                if (m_showUnbilled)
                    rowInfo.Cells[1].Value = IsBillable(row) ? "YES" : "no";
                else
                    rowInfo.Cells[1].Value = string.Empty;

                if ( m_showUninvoiced )
                    rowInfo.Cells[2].Value = IsInvoicable(row) ? "YES" : "no";
                else
                    rowInfo.Cells[2].Value = string.Empty;
            }
        }

        private void CheckVisible(bool check)
        {
            // Check (or uncheck) all the visible rows

            // Suppress action in radGridView1_ValueChanged
            m_isFilling = true;

            foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
            {
                if (rowInfo.IsVisible)
                    rowInfo.Cells[0].Value = check;
            }

            m_isFilling = false;

            OnVisibleCheckedTimecardsChanged();
        }

        private void FillTimesheet(int timesheetId)
        {
            m_isFilling = true;
            timesheetsTableAdapter.FillByTimesheetId(winployDataSet.Timesheets, timesheetId);
            m_isFilling = false;
        }

        protected uint NumCheckedTimecards
        {
            get
            {
                if (m_isFilling)
                    return 0;       // rowInfo.Cells[0].Value is System.DBNull (can't cast to bool!)

                // Return number of visible timecards that are checked in the first column (column 0)
                uint count = 0;
                foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
                {
                    if (rowInfo.IsVisible && (bool)rowInfo.Cells[0].Value)
                        ++count;
                }

                return count;
            }
        }

        protected uint NumVisibleTimecards
        {
            get
            {
                // Return number of visible timecards
                uint count = 0;
                foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
                {
                    if (rowInfo.IsVisible)
                        ++count;
                }

                return count;
            }
        }


        /// <summary>
        /// Append a line of text to Status listbox
        /// </summary>
        private void AddStatus(string appendLine)
        {
            lbStatus.BeginUpdate();
            int i = lbStatus.Items.Add(new RadListBoxItem()
            {
                Text = appendLine
            });

            // Cause list to show new line right away
            lbStatus.SelectedIndex = i;
            lbStatus.EndUpdate();
        }

        private void UpdateTimecardStatus(GridViewDataRowInfo rowInfo, TimecardStage.TimecardStatus newStatus, bool manual)
        {
            try
            {
                DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
                winployDataSet.ApprovedTimesheetsRow approvedTimesheetRow = (winployDataSet.ApprovedTimesheetsRow)drv.Row;
                
                FillTimesheet(approvedTimesheetRow.TimesheetId);
                winployDataSet.TimesheetsRow timesheetRow = (winployDataSet.TimesheetsRow)winployDataSet.Timesheets.Rows[0];
                timesheetRow.TimecardStatus = (short)newStatus;

                string newComment = string.Format((manual) ? "Set {0} on {1}" : "{0} on {1}", newStatus, DateTime.Now);

                if (timesheetRow.IsCommentsNull())
                    timesheetRow.Comments = newComment;
                else
                    timesheetRow.Comments += "\r\n" + newComment;

                timesheetsTableAdapter.Update(timesheetRow);

                // Ensure cached row reflects new status
                approvedTimesheetRow.TimecardStatus = (short)newStatus;
            }
            catch (System.Exception ex)
            {
                // Show error label with error message
                AddStatus(string.Format("Error updating timecard status; set the timecard status individually:  {0}", ex.Message));

                Program.ShowExceptionMessage(ex);
            }

            // Hide row that is finished
            rowInfo.IsSelected = false;  // unhighlight current item
            rowInfo.IsVisible = false;
        }

        private void FilterBilledInvoiced()
        {
            foreach (GridViewDataRowInfo rowInfo in radGridView1.Rows)
            {
                // Show this row if showing billable items and it is billable, or
                // if showing invoicable items and it is invoicable
                bool showRow = ( m_showUnbilled && !IsBilled(rowInfo) && ShouldBill(rowInfo)) ||
                               ( m_showUninvoiced && !IsInvoiced(rowInfo) );

                if ( showRow )
                {
                    rowInfo.IsVisible = true;
                }
                else
                {
                    rowInfo.IsSelected = false;
                    rowInfo.IsVisible = false;
                }
            }

            radGridView1.MasterGridViewTemplate.BestFitColumns();

            OnVisibleCheckedTimecardsChanged();
        }

        private void OnVisibleCheckedTimecardsChanged()
        {
            uint numChecked = NumCheckedTimecards;
            uint numVisible = NumVisibleTimecards;
            rlNumSelected.Text = string.Format("{0} timecard(s), {1} checked", numVisible, numChecked);

            // Enable items based on whether any timecards are visible
            rlSelectAll.Enabled = rlSelectNone.Enabled = (numVisible > 0);

            // Enable items based on whether any timecards are checked
            if (m_showUnbilled)
            {
                rlGenerateBills.Enabled = rlSetStatusBilled.Enabled = (numChecked > 0);
                rlGenerateInvoices.Enabled = rlGenerateOneInvoice.Enabled = false;
            }
            else if (m_showUninvoiced)
            {
                rlGenerateBills.Enabled = rlSetStatusBilled.Enabled = false;
                rlGenerateInvoices.Enabled = rlGenerateOneInvoice.Enabled = (numChecked > 0);
            }
        }

        private bool IsInvoicable(DataRow row)
        {
            // Invoicable, if the QB ID's are not set up for it

            // NOTE:  The Link screen resets the string empty and not null, so there is no guid defined even if
            // e.g. approvedTimesheetRow.IsCustomerQBNull() returns false --- it is not null, but it is an empty string!
            // Unfortunately, it is the easiest way to test this condition using the old style generic DataRowView.
            bool customerDefined = !string.IsNullOrEmpty(row["CustomerQB"].ToString());
            bool contractorDefined = !string.IsNullOrEmpty(row["ContractorQB"].ToString());
            bool salesOrderDefined = !string.IsNullOrEmpty(row["SalesOrderQB"].ToString()) && !string.IsNullOrEmpty(row["SalesOrderItemQB"].ToString());  // both required
            return (customerDefined && (contractorDefined || salesOrderDefined));
        }

        private bool IsBillable(DataRow row)
        {
            // Billable, if the QB ID's are set up
            bool vendorDefined = !string.IsNullOrEmpty(row["VendorQB"].ToString());
            bool itemDefined = !string.IsNullOrEmpty(row["ContractorQB"].ToString());
            bool customerDefined = !string.IsNullOrEmpty(row["CustomerQB"].ToString());
            return (vendorDefined && itemDefined && customerDefined);
        }

        private bool ShouldBill(GridViewDataRowInfo rowInfo)
        {
            // Should bill, if there is a vendor
            DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
            bool hasVendor = !string.IsNullOrEmpty(drv["VendorName"].ToString());
            return (hasVendor);
        }

        private bool IsInvoiced(GridViewDataRowInfo rowInfo)
        {
            DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
            winployDataSet.ApprovedTimesheetsRow approvedTimesheetRow = (winployDataSet.ApprovedTimesheetsRow)drv.Row;
            return TimecardStage.IsInvoiced(approvedTimesheetRow.TimecardStatus);
        }

        private bool IsBilled(GridViewDataRowInfo rowInfo)
        {
            DataRowView drv = (DataRowView)rowInfo.DataBoundItem;
            winployDataSet.ApprovedTimesheetsRow approvedTimesheetRow = (winployDataSet.ApprovedTimesheetsRow)drv.Row;
            return TimecardStage.IsBilled(approvedTimesheetRow.TimecardStatus);
        }

        bool m_lastVisible;
        bool m_isFilling;

        bool m_showUninvoiced;
        bool m_showUnbilled;
    }
}
