﻿using System.Windows.Forms;


namespace AdminTimecard
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.ThemeSource themeSource1 = new Telerik.WinControls.ThemeSource();
            Telerik.WinControls.ThemeSource themeSource2 = new Telerik.WinControls.ThemeSource();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.radFileMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.rmiSeparator = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.rmiExit = new Telerik.WinControls.UI.RadMenuItem();
            this.shortcuts1 = new Telerik.WinControls.Keyboard.Shortcuts(this.components);
            this.lblStatus = new Telerik.WinControls.UI.RadLabelElement();
            this.radToolStrip1 = new Telerik.WinControls.UI.RadToolStrip();
            this.radToolStripElement1 = new Telerik.WinControls.UI.RadToolStripElement();
            this.radToolStripItem1 = new Telerik.WinControls.UI.RadToolStripItem();
            this.cbToolbarConnections = new Telerik.WinControls.UI.RadComboBoxElement();
            this.connectionWinploy = new Telerik.WinControls.UI.RadComboBoxItem();
            this.connectionDCSoft = new Telerik.WinControls.UI.RadComboBoxItem();
            this.connectionErgTimecard = new Telerik.WinControls.UI.RadComboBoxItem();
            this.btnToolbarRefresh = new Telerik.WinControls.UI.RadButtonElement();
            this.btnToolbarExit = new Telerik.WinControls.UI.RadButtonElement();
            this.radToolStripItem2 = new Telerik.WinControls.UI.RadToolStripItem();
            this.cbToolbarTheme = new Telerik.WinControls.UI.RadComboBoxElement();
            this.radComboBoxItem1 = new Telerik.WinControls.UI.RadComboBoxItem();
            this.radComboBoxItem2 = new Telerik.WinControls.UI.RadComboBoxItem();
            this.radComboBoxItem3 = new Telerik.WinControls.UI.RadComboBoxItem();
            this.radComboBoxItem4 = new Telerik.WinControls.UI.RadComboBoxItem();
            this.radComboBoxItem25 = new Telerik.WinControls.UI.RadComboBoxItem();
            this.radComboBoxItem26 = new Telerik.WinControls.UI.RadComboBoxItem();
            this.radComboBoxItem5 = new Telerik.WinControls.UI.RadComboBoxItem();
            this.radPanelBar1 = new Telerik.WinControls.UI.RadPanelBar();
            this.radPanelBarGroupElementTimecards = new Telerik.WinControls.UI.RadPanelBarGroupElement();
            this.radLabelElementContractor = new Telerik.WinControls.UI.RadLabelElement();
            this.radLabelElementNotSubmitted = new Telerik.WinControls.UI.RadLabelElement();
            this.radLabelElementNotApproved = new Telerik.WinControls.UI.RadLabelElement();
            this.radLabelElementNotInvoiced = new Telerik.WinControls.UI.RadLabelElement();
            this.radPanelBarGroupElementQuickBooks = new Telerik.WinControls.UI.RadPanelBarGroupElement();
            this.radLabelElementQuickbooksInvoices = new Telerik.WinControls.UI.RadLabelElement();
            this.radLabelElementQuickBooksMap = new Telerik.WinControls.UI.RadLabelElement();
            this.radPanelBarGroupElementJobs = new Telerik.WinControls.UI.RadPanelBarGroupElement();
            this.radLabelElementJobSettings = new Telerik.WinControls.UI.RadLabelElement();
            this.radLabelElementActiveJobsReport = new Telerik.WinControls.UI.RadLabelElement();
            this.radPanelBarGroupElementReports = new Telerik.WinControls.UI.RadPanelBarGroupElement();
            this.radLabelElementHealth = new Telerik.WinControls.UI.RadLabelElement();
            this.radTextBoxElement1 = new Telerik.WinControls.UI.RadTextBoxElement();
            this.radLabelElement1 = new Telerik.WinControls.UI.RadLabelElement();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToolStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbToolbarConnections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbToolbarTheme)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radMenu1
            // 
            this.radMenu1.AllowMerge = false;
            this.radMenu1.BackColor = System.Drawing.Color.Transparent;
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radFileMenuItem});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radMenu1.Name = "radMenu1";
            // 
            // 
            // 
            this.radMenu1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radMenu1.Size = new System.Drawing.Size(1077, 24);
            this.radMenu1.TabIndex = 0;
            this.radMenu1.Text = "radMainMenu";
            this.radMenu1.ThemeName = "Office2007Black";
            // 
            // radFileMenuItem
            // 
            this.radFileMenuItem.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radFileMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.rmiSeparator,
            this.rmiExit});
            this.radFileMenuItem.Name = "radFileMenuItem";
            this.radFileMenuItem.PopupDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radFileMenuItem.Text = "&File";
            this.radFileMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radFileMenuItem.ToolTipText = "Exit application";
            // 
            // rmiSeparator
            // 
            this.rmiSeparator.AutoSize = false;
            this.rmiSeparator.Bounds = new System.Drawing.Rectangle(0, 0, 66, 1);
            this.rmiSeparator.Class = "RadMenuItem";
            this.rmiSeparator.Name = "rmiSeparator";
            this.rmiSeparator.PositionOffset = new System.Drawing.SizeF(26F, 0F);
            this.rmiSeparator.Text = "radMenuSeparatorItem1";
            // 
            // rmiExit
            // 
            this.rmiExit.HintText = "Alt+F4";
            this.rmiExit.Name = "rmiExit";
            this.rmiExit.Text = "&Exit";
            this.rmiExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rmiExit.ToolTipText = "Exit application";
            this.rmiExit.Click += new System.EventHandler(this.MenuExit_Click);
            // 
            // shortcuts1
            // 
            this.shortcuts1.Owner = this;
            // 
            // lblStatus
            // 
            this.lblStatus.Margin = new System.Windows.Forms.Padding(1);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Text = "Current:";
            this.lblStatus.TextWrap = true;
            // 
            // radToolStrip1
            // 
            this.radToolStrip1.AllowDragging = false;
            this.radToolStrip1.AllowFloating = false;
            this.radToolStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radToolStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radToolStripElement1});
            this.radToolStrip1.Location = new System.Drawing.Point(0, 24);
            this.radToolStrip1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radToolStrip1.MinimumSize = new System.Drawing.Size(5, 6);
            this.radToolStrip1.Name = "radToolStrip1";
            this.radToolStrip1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radToolStrip1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radToolStrip1.RootElement.MinSize = new System.Drawing.Size(5, 6);
            this.radToolStrip1.ShowOverFlowButton = false;
            this.radToolStrip1.Size = new System.Drawing.Size(1077, 46);
            this.radToolStrip1.TabIndex = 1;
            this.radToolStrip1.Text = "radToolStrip1";
            this.radToolStrip1.ThemeName = "Office2007Black";
            // 
            // radToolStripElement1
            // 
            this.radToolStripElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.radToolStripElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radToolStripItem1,
            this.radToolStripItem2});
            this.radToolStripElement1.Margin = new System.Windows.Forms.Padding(2, 2, 2, -3);
            this.radToolStripElement1.MinSize = new System.Drawing.Size(0, 0);
            this.radToolStripElement1.Name = "radToolStripElement1";
            this.radToolStripElement1.Text = "radToolStripElement1";
            // 
            // radToolStripItem1
            // 
            this.radToolStripItem1.Class = "ToolStripItem";
            this.radToolStripItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cbToolbarConnections,
            this.btnToolbarRefresh,
            this.btnToolbarExit});
            this.radToolStripItem1.Key = "0";
            this.radToolStripItem1.MinSize = new System.Drawing.Size(25, 21);
            this.radToolStripItem1.Name = "radToolStripItem1";
            this.radToolStripItem1.Text = "radToolStripItem1";
            // 
            // cbToolbarConnections
            // 
            this.cbToolbarConnections.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbToolbarConnections.ArrowButtonMinWidth = 16;
            this.cbToolbarConnections.AutoSize = true;
            this.cbToolbarConnections.DblClickRotate = true;
            this.cbToolbarConnections.DefaultValue = null;
            this.cbToolbarConnections.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbToolbarConnections.EditorElement = this.cbToolbarConnections;
            this.cbToolbarConnections.EditorManager = null;
            this.cbToolbarConnections.Focusable = true;
            this.cbToolbarConnections.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbToolbarConnections.FormatString = "{0}";
            this.cbToolbarConnections.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.connectionWinploy,
            this.connectionDCSoft,
            this.connectionErgTimecard});
            this.cbToolbarConnections.MaxSize = new System.Drawing.Size(118, 20);
            this.cbToolbarConnections.MaxValue = null;
            this.cbToolbarConnections.MinSize = new System.Drawing.Size(118, 17);
            this.cbToolbarConnections.MinValue = null;
            this.cbToolbarConnections.Name = "cbToolbarConnections";
            this.cbToolbarConnections.NullTextColor = System.Drawing.SystemColors.GrayText;
            this.cbToolbarConnections.NullValue = null;
            this.cbToolbarConnections.OwnerOffset = 0;
            this.cbToolbarConnections.Padding = new System.Windows.Forms.Padding(0);
            this.cbToolbarConnections.Text = "winploy.com";
            this.cbToolbarConnections.Value = null;
            this.cbToolbarConnections.SelectedIndexChanged += new System.EventHandler(this.cbToolbarConnections_SelectedIndexChanged);
            // 
            // connectionWinploy
            // 
            this.connectionWinploy.Name = "connectionWinploy";
            this.connectionWinploy.Text = "winploy.com";
            // 
            // connectionDCSoft
            // 
            this.connectionDCSoft.Name = "connectionDCSoft";
            this.connectionDCSoft.Text = "dcsoft.com";
            // 
            // connectionErgTimecard
            // 
            this.connectionErgTimecard.Name = "connectionErgTimecard";
            this.connectionErgTimecard.Text = "ergtimecard.com";
            // 
            // btnToolbarRefresh
            // 
            this.btnToolbarRefresh.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnToolbarRefresh.CanFocus = true;
            this.btnToolbarRefresh.Name = "btnToolbarRefresh";
            this.btnToolbarRefresh.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnToolbarRefresh.ShowBorder = false;
            this.btnToolbarRefresh.Text = "Refresh";
            this.btnToolbarRefresh.Click += new System.EventHandler(this.btnToolbarRefresh_Click);
            // 
            // btnToolbarExit
            // 
            this.btnToolbarExit.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnToolbarExit.CanFocus = true;
            this.btnToolbarExit.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.btnToolbarExit.Margin = new System.Windows.Forms.Padding(1);
            this.btnToolbarExit.Name = "btnToolbarExit";
            this.btnToolbarExit.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnToolbarExit.ShowBorder = false;
            this.btnToolbarExit.Text = "Exit";
            this.btnToolbarExit.Click += new System.EventHandler(this.ToolbarExit_Click);
            // 
            // radToolStripItem2
            // 
            this.radToolStripItem2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cbToolbarTheme});
            this.radToolStripItem2.Key = "1";
            this.radToolStripItem2.Name = "radToolStripItem2";
            this.radToolStripItem2.Text = "radToolStripItem2";
            // 
            // cbToolbarTheme
            // 
            this.cbToolbarTheme.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbToolbarTheme.ArrowButtonMinWidth = 16;
            this.cbToolbarTheme.AutoSize = true;
            this.cbToolbarTheme.DefaultValue = null;
            this.cbToolbarTheme.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbToolbarTheme.EditorElement = this.cbToolbarTheme;
            this.cbToolbarTheme.EditorManager = null;
            this.cbToolbarTheme.Focusable = true;
            this.cbToolbarTheme.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbToolbarTheme.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radComboBoxItem1,
            this.radComboBoxItem2,
            this.radComboBoxItem3,
            this.radComboBoxItem4,
            this.radComboBoxItem25,
            this.radComboBoxItem26,
            this.radComboBoxItem5});
            this.cbToolbarTheme.MaxSize = new System.Drawing.Size(118, 20);
            this.cbToolbarTheme.MaxValue = null;
            this.cbToolbarTheme.MinSize = new System.Drawing.Size(118, 17);
            this.cbToolbarTheme.MinValue = null;
            this.cbToolbarTheme.Name = "cbToolbarTheme";
            this.cbToolbarTheme.NullTextColor = System.Drawing.SystemColors.GrayText;
            this.cbToolbarTheme.NullValue = null;
            this.cbToolbarTheme.OwnerOffset = 0;
            this.cbToolbarTheme.Text = "radComboBoxElement1";
            this.cbToolbarTheme.Value = null;
            this.cbToolbarTheme.SelectedIndexChanged += new System.EventHandler(this.cbToolbarTheme_SelectedIndexChanged);
            // 
            // radComboBoxItem1
            // 
            this.radComboBoxItem1.Name = "radComboBoxItem1";
            this.radComboBoxItem1.Text = "ERG Black";
            // 
            // radComboBoxItem2
            // 
            this.radComboBoxItem2.Name = "radComboBoxItem2";
            this.radComboBoxItem2.Text = "Office 2007 Blue";
            // 
            // radComboBoxItem3
            // 
            this.radComboBoxItem3.Name = "radComboBoxItem3";
            this.radComboBoxItem3.Text = "Office 2007 Silver";
            // 
            // radComboBoxItem4
            // 
            this.radComboBoxItem4.Name = "radComboBoxItem4";
            this.radComboBoxItem4.Text = "Office 2010";
            // 
            // radComboBoxItem25
            // 
            this.radComboBoxItem25.Name = "radComboBoxItem25";
            this.radComboBoxItem25.Text = "Vista";
            // 
            // radComboBoxItem26
            // 
            this.radComboBoxItem26.Name = "radComboBoxItem26";
            this.radComboBoxItem26.Text = "Telerik";
            // 
            // radComboBoxItem5
            // 
            this.radComboBoxItem5.Name = "radComboBoxItem5";
            this.radComboBoxItem5.Text = "Desert";
            // 
            // radPanelBar1
            // 
            this.radPanelBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radPanelBar1.AutoSize = true;
            this.radPanelBar1.CanScroll = false;
            this.radPanelBar1.Caption = "Default Caption";
            this.radPanelBar1.CaptionTextFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.radPanelBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radPanelBar1.GroupStyle = Telerik.WinControls.PanelBarStyles.OutlookNavPane;
            this.radPanelBar1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radPanelBarGroupElementTimecards,
            this.radPanelBarGroupElementQuickBooks,
            this.radPanelBarGroupElementJobs,
            this.radPanelBarGroupElementReports});
            this.radPanelBar1.Location = new System.Drawing.Point(0, 53);
            this.radPanelBar1.Name = "radPanelBar1";
            this.radPanelBar1.Size = new System.Drawing.Size(254, 596);
            this.radPanelBar1.TabIndex = 3;
            this.radPanelBar1.Text = "radPanelBar1";
            this.radPanelBar1.ThemeName = "Office2007Black";
            this.radPanelBar1.PanelBarGroupSelected += new Telerik.WinControls.UI.PanelBarGroupEventHandler(this.radPanelBar1_PanelBarGroupSelected);
            // 
            // radPanelBarGroupElementTimecards
            // 
            this.radPanelBarGroupElementTimecards.Caption = "Timecards";
            // 
            // radPanelBarGroupElementTimecards.ContentPanel
            // 
            this.radPanelBarGroupElementTimecards.ContentPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radPanelBarGroupElementTimecards.ContentPanel.CausesValidation = true;
            this.radPanelBarGroupElementTimecards.ContentPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanelBarGroupElementTimecards.ContentPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radPanelBarGroupElementTimecards.ContentPanel.Location = new System.Drawing.Point(0, 0);
            this.radPanelBarGroupElementTimecards.ContentPanel.Size = new System.Drawing.Size(200, 100);
            this.radPanelBarGroupElementTimecards.Expanded = true;
            this.radPanelBarGroupElementTimecards.GroupCollapseExplorerBarImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementTimecards.GroupCollapseExplorerBarImage")));
            this.radPanelBarGroupElementTimecards.GroupCollapseVSImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementTimecards.GroupCollapseVSImage")));
            this.radPanelBarGroupElementTimecards.GroupExpandExplorerBarImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementTimecards.GroupExpandExplorerBarImage")));
            this.radPanelBarGroupElementTimecards.GroupExpandVSImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementTimecards.GroupExpandVSImage")));
            this.radPanelBarGroupElementTimecards.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElementContractor,
            this.radLabelElementNotSubmitted,
            this.radLabelElementNotApproved,
            this.radLabelElementNotInvoiced});
            this.radPanelBarGroupElementTimecards.Name = "radPanelBarGroupElementTimecards";
            // 
            // radLabelElementContractor
            // 
            this.radLabelElementContractor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementContractor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementContractor.Name = "radLabelElementContractor";
            this.radLabelElementContractor.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementContractor.StretchHorizontally = false;
            this.radLabelElementContractor.StretchVertically = false;
            this.radLabelElementContractor.Text = "View By Contractor";
            this.radLabelElementContractor.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabelElementContractor.TextWrap = true;
            this.radLabelElementContractor.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementContractor.Click += new System.EventHandler(this.Timecards_Contractor_Click);
            // 
            // radLabelElementNotSubmitted
            // 
            this.radLabelElementNotSubmitted.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementNotSubmitted.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementNotSubmitted.Name = "radLabelElementNotSubmitted";
            this.radLabelElementNotSubmitted.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementNotSubmitted.StretchHorizontally = false;
            this.radLabelElementNotSubmitted.StretchVertically = false;
            this.radLabelElementNotSubmitted.Text = "Not Submitted Report";
            this.radLabelElementNotSubmitted.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabelElementNotSubmitted.TextWrap = true;
            this.radLabelElementNotSubmitted.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementNotSubmitted.Click += new System.EventHandler(this.Timecards_NotSubmitted_Click);
            // 
            // radLabelElementNotApproved
            // 
            this.radLabelElementNotApproved.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementNotApproved.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementNotApproved.Name = "radLabelElementNotApproved";
            this.radLabelElementNotApproved.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementNotApproved.StretchHorizontally = false;
            this.radLabelElementNotApproved.StretchVertically = false;
            this.radLabelElementNotApproved.Text = "Not Approved Report";
            this.radLabelElementNotApproved.TextWrap = true;
            this.radLabelElementNotApproved.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementNotApproved.Click += new System.EventHandler(this.Timecards_NotApproved_Click);
            // 
            // radLabelElementNotInvoiced
            // 
            this.radLabelElementNotInvoiced.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementNotInvoiced.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementNotInvoiced.Name = "radLabelElementNotInvoiced";
            this.radLabelElementNotInvoiced.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementNotInvoiced.StretchHorizontally = false;
            this.radLabelElementNotInvoiced.StretchVertically = false;
            this.radLabelElementNotInvoiced.Text = "Not Invoiced Report";
            this.radLabelElementNotInvoiced.TextWrap = true;
            this.radLabelElementNotInvoiced.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementNotInvoiced.Click += new System.EventHandler(this.TimecardsNotInvoiced_Click);
            // 
            // radPanelBarGroupElementQuickBooks
            // 
            this.radPanelBarGroupElementQuickBooks.Caption = "QuickBooks";
            // 
            // radPanelBarGroupElementQuickBooks.ContentPanel
            // 
            this.radPanelBarGroupElementQuickBooks.ContentPanel.BackColor = System.Drawing.SystemColors.Control;
            this.radPanelBarGroupElementQuickBooks.ContentPanel.CausesValidation = true;
            this.radPanelBarGroupElementQuickBooks.ContentPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanelBarGroupElementQuickBooks.ContentPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radPanelBarGroupElementQuickBooks.ContentPanel.Location = new System.Drawing.Point(0, 0);
            this.radPanelBarGroupElementQuickBooks.ContentPanel.Size = new System.Drawing.Size(200, 100);
            this.radPanelBarGroupElementQuickBooks.GroupCollapseExplorerBarImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementQuickBooks.GroupCollapseExplorerBarImage")));
            this.radPanelBarGroupElementQuickBooks.GroupCollapseVSImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementQuickBooks.GroupCollapseVSImage")));
            this.radPanelBarGroupElementQuickBooks.GroupExpandExplorerBarImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementQuickBooks.GroupExpandExplorerBarImage")));
            this.radPanelBarGroupElementQuickBooks.GroupExpandVSImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementQuickBooks.GroupExpandVSImage")));
            this.radPanelBarGroupElementQuickBooks.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElementQuickbooksInvoices,
            this.radLabelElementQuickBooksMap});
            this.radPanelBarGroupElementQuickBooks.Name = "radPanelBarGroupElementQuickBooks";
            // 
            // radLabelElementQuickbooksInvoices
            // 
            this.radLabelElementQuickbooksInvoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementQuickbooksInvoices.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementQuickbooksInvoices.Name = "radLabelElementQuickbooksInvoices";
            this.radLabelElementQuickbooksInvoices.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementQuickbooksInvoices.StretchHorizontally = false;
            this.radLabelElementQuickbooksInvoices.StretchVertically = false;
            this.radLabelElementQuickbooksInvoices.Text = "Create Invoices and Bills";
            this.radLabelElementQuickbooksInvoices.TextWrap = true;
            this.radLabelElementQuickbooksInvoices.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementQuickbooksInvoices.Click += new System.EventHandler(this.Quickbooks_Bill_Click);
            // 
            // radLabelElementQuickBooksMap
            // 
            this.radLabelElementQuickBooksMap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementQuickBooksMap.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementQuickBooksMap.Name = "radLabelElementQuickBooksMap";
            this.radLabelElementQuickBooksMap.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementQuickBooksMap.StretchHorizontally = false;
            this.radLabelElementQuickBooksMap.StretchVertically = false;
            this.radLabelElementQuickBooksMap.Text = "Link QuickBooks to Website";
            this.radLabelElementQuickBooksMap.TextWrap = true;
            this.radLabelElementQuickBooksMap.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementQuickBooksMap.Click += new System.EventHandler(this.Quickbooks_Map_Click);
            // 
            // radPanelBarGroupElementJobs
            // 
            this.radPanelBarGroupElementJobs.Caption = "Jobs";
            // 
            // radPanelBarGroupElementJobs.ContentPanel
            // 
            this.radPanelBarGroupElementJobs.ContentPanel.BackColor = System.Drawing.SystemColors.Control;
            this.radPanelBarGroupElementJobs.ContentPanel.CausesValidation = true;
            this.radPanelBarGroupElementJobs.ContentPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanelBarGroupElementJobs.ContentPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radPanelBarGroupElementJobs.ContentPanel.Location = new System.Drawing.Point(0, 0);
            this.radPanelBarGroupElementJobs.ContentPanel.Size = new System.Drawing.Size(200, 100);
            this.radPanelBarGroupElementJobs.Expanded = true;
            this.radPanelBarGroupElementJobs.GroupCollapseExplorerBarImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementJobs.GroupCollapseExplorerBarImage")));
            this.radPanelBarGroupElementJobs.GroupCollapseVSImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementJobs.GroupCollapseVSImage")));
            this.radPanelBarGroupElementJobs.GroupExpandExplorerBarImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementJobs.GroupExpandExplorerBarImage")));
            this.radPanelBarGroupElementJobs.GroupExpandVSImage = ((System.Drawing.Image)(resources.GetObject("radPanelBarGroupElementJobs.GroupExpandVSImage")));
            this.radPanelBarGroupElementJobs.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElementJobSettings,
            this.radLabelElementActiveJobsReport});
            this.radPanelBarGroupElementJobs.Name = "radPanelBarGroupElementJobs";
            // 
            // radLabelElementJobSettings
            // 
            this.radLabelElementJobSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementJobSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementJobSettings.Name = "radLabelElementJobSettings";
            this.radLabelElementJobSettings.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementJobSettings.StretchHorizontally = false;
            this.radLabelElementJobSettings.StretchVertically = false;
            this.radLabelElementJobSettings.Text = "Job Settings and E-mails";
            this.radLabelElementJobSettings.TextWrap = true;
            this.radLabelElementJobSettings.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementJobSettings.Click += new System.EventHandler(this.Jobs_Administer_Click);
            // 
            // radLabelElementActiveJobsReport
            // 
            this.radLabelElementActiveJobsReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementActiveJobsReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementActiveJobsReport.Name = "radLabelElementActiveJobsReport";
            this.radLabelElementActiveJobsReport.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementActiveJobsReport.StretchHorizontally = false;
            this.radLabelElementActiveJobsReport.StretchVertically = false;
            this.radLabelElementActiveJobsReport.Text = "Active Jobs Report";
            this.radLabelElementActiveJobsReport.TextWrap = true;
            this.radLabelElementActiveJobsReport.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementActiveJobsReport.Click += new System.EventHandler(this.Jobs_Report_Click);
            // 
            // radPanelBarGroupElementReports
            // 
            this.radPanelBarGroupElementReports.Caption = "Reports";
            // 
            // radPanelBarGroupElementReports.ContentPanel
            // 
            this.radPanelBarGroupElementReports.ContentPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radPanelBarGroupElementReports.ContentPanel.CausesValidation = true;
            this.radPanelBarGroupElementReports.ContentPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanelBarGroupElementReports.ContentPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radPanelBarGroupElementReports.ContentPanel.Location = new System.Drawing.Point(0, 0);
            this.radPanelBarGroupElementReports.ContentPanel.Size = new System.Drawing.Size(200, 100);
            this.radPanelBarGroupElementReports.Expanded = true;
            this.radPanelBarGroupElementReports.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElementHealth});
            this.radPanelBarGroupElementReports.Name = "radPanelBarGroupElementReports";
            this.radPanelBarGroupElementReports.Text = global::AdminTimecard.Properties.Resources.String1;
            // 
            // radLabelElementHealth
            // 
            this.radLabelElementHealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElementHealth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElementHealth.Name = "radLabelElementHealth";
            this.radLabelElementHealth.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElementHealth.StretchHorizontally = false;
            this.radLabelElementHealth.StretchVertically = false;
            this.radLabelElementHealth.Text = "View Monthly Health";
            this.radLabelElementHealth.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabelElementHealth.TextWrap = true;
            this.radLabelElementHealth.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radLabelElementHealth.Click += new System.EventHandler(this.Reports_Health_Click);
            // 
            // radTextBoxElement1
            // 
            this.radTextBoxElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.radTextBoxElement1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBoxElement1.MaxSize = new System.Drawing.Size(118, 19);
            this.radTextBoxElement1.MinSize = new System.Drawing.Size(118, 19);
            this.radTextBoxElement1.Name = "radTextBoxElement1";
            this.radTextBoxElement1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 3);
            this.radTextBoxElement1.ShowBorder = true;
            this.radTextBoxElement1.StretchVertically = false;
            this.radTextBoxElement1.Text = "winploy.com";
            // 
            // radLabelElement1
            // 
            this.radLabelElement1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElement1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.radLabelElement1.Name = "radLabelElement1";
            this.radLabelElement1.PositionOffset = new System.Drawing.SizeF(10F, 0F);
            this.radLabelElement1.StretchHorizontally = false;
            this.radLabelElement1.StretchVertically = false;
            this.radLabelElement1.Text = "Not Approved Report";
            this.radLabelElement1.TextWrap = true;
            // 
            // radThemeManager1
            // 
            themeSource1.StorageType = Telerik.WinControls.ThemeStorageType.Resource;
            themeSource1.ThemeLocation = "AdminTimecard.ERGTheme_Telerik_WinControls_UI_RadListBox.xml";
            themeSource2.StorageType = Telerik.WinControls.ThemeStorageType.Resource;
            themeSource2.ThemeLocation = "AdminTimecard.ERGTheme_Repository.xml";
            this.radThemeManager1.LoadedThemes.AddRange(new Telerik.WinControls.ThemeSource[] {
            themeSource1,
            themeSource2});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 646);
            this.Controls.Add(this.radPanelBar1);
            this.Controls.Add(this.radToolStrip1);
            this.Controls.Add(this.radMenu1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "ERG Timecard Builder";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToolStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbToolbarConnections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbToolbarTheme)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem radFileMenuItem;
        private Telerik.WinControls.UI.RadMenuItem rmiExit;
        private Telerik.WinControls.Keyboard.Shortcuts shortcuts1;
        private Telerik.WinControls.UI.RadMenuSeparatorItem rmiSeparator;
        private Telerik.WinControls.UI.RadLabelElement lblStatus;
        private Telerik.WinControls.UI.RadToolStrip radToolStrip1;
        private Telerik.WinControls.UI.RadToolStripElement radToolStripElement1;
        private Telerik.WinControls.UI.RadToolStripItem radToolStripItem1;
        private Telerik.WinControls.UI.RadButtonElement btnToolbarExit;

        private Telerik.WinControls.UI.RadPanelBar radPanelBar1;
        private Telerik.WinControls.UI.RadPanelBarGroupElement radPanelBarGroupElementTimecards;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementNotSubmitted;
        private Telerik.WinControls.UI.RadPanelBarGroupElement radPanelBarGroupElementQuickBooks;
        private Telerik.WinControls.UI.RadPanelBarGroupElement radPanelBarGroupElementJobs;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementContractor;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementNotApproved;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementQuickBooksMap;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementQuickbooksInvoices;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementJobSettings;
        private Telerik.WinControls.UI.RadTextBoxElement radTextBoxElement1;
        private Telerik.WinControls.UI.RadComboBoxElement cbToolbarConnections;
        private Telerik.WinControls.UI.RadComboBoxItem connectionWinploy;
        private Telerik.WinControls.UI.RadComboBoxItem connectionErgTimecard;
        private Telerik.WinControls.UI.RadButtonElement btnToolbarRefresh;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementActiveJobsReport;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementNotInvoiced;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement1;
        private Telerik.WinControls.UI.RadComboBoxItem connectionDCSoft;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private Telerik.WinControls.UI.RadToolStripItem radToolStripItem2;
        private Telerik.WinControls.UI.RadComboBoxElement cbToolbarTheme;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem1;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem2;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem3;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem4;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem25;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem26;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem5;
        private Telerik.WinControls.UI.RadPanelBarGroupElement radPanelBarGroupElementReports;
        private Telerik.WinControls.UI.RadLabelElement radLabelElementHealth;
    }
}

