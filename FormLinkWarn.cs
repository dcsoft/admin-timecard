﻿using System;
using System.Windows.Forms;

namespace AdminTimecard
{
    public partial class FormLinkWarn : Form
    {
        public FormLinkWarn(string heading, string message)
        {
            InitializeComponent();
            lblHeading.Text = heading;
            lblMessage.Text = message;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
