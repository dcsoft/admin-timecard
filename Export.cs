using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;


namespace AdminTimecard
{
	public class FileExport
	{
        public static bool ShowSaveFileDialog(ref string filePath)
        {
            // Prompt for file path, return it in filePath
            var dlg = new SaveFileDialog
            {
                Filter = @"Comma Separated Value Files (.csv)|*.csv|All Files (*.*)|*.*",
                OverwritePrompt = false,
                CreatePrompt = false,
                AddExtension = true,
                CheckFileExists = false,
                CheckPathExists = true,
                DefaultExt = ".csv",
                Title = "ERG Timecard Builder - Export Report"
            };

            if (!string.IsNullOrEmpty(filePath))
                dlg.InitialDirectory = Path.GetDirectoryName(filePath);

            if (dlg.ShowDialog() != DialogResult.Cancel)
            {
                filePath = dlg.FileName;
                return true;
            }

            return false;
        }

        public static void OpenExportedFile(string filePath)
        {
            // Inform successful export, open file if user wants
            if (MessageBox.Show(filePath, "Open exported file?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var csvProcess = new Process
                {
                    StartInfo =
                    {
                        FileName = filePath,
                        // Arguments = ,
                        WindowStyle = ProcessWindowStyle.Normal,
                        ErrorDialog = true
                    },

                    EnableRaisingEvents = false
                };
                try
                {
                    csvProcess.Start();
                }
                catch (Exception)
                {
                    // Process should show error dialog if there is a problem,
                    // so don't show any UI here.
                }
            }
        }

        public static string QuoteCSVItem(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            // Quote s if it contains embedded quotes
            return (s.IndexOfAny("\",\x0A\x0D".ToCharArray()) >= 0) ?
                    string.Format("\"" + s.Replace("\"", "\"\"") + "\"") :
                    s;
        }
    }
}