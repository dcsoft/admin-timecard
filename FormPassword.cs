﻿using System;
using System.Windows.Forms;
using AdminTimecard.Properties;


namespace AdminTimecard
{
    public partial class FormPassword : Form
    {
        public FormPassword()
        {
            InitializeComponent();
        }

        private void radButtonOK_Click(object sender, EventArgs e)
        {
            // OK button was clicked; Show error if password does not match, and prevent closing of dialog
            if (tbPassword.Text != RealPassword)
            {
                MessageBox.Show("The password is incorrect.  Please try again.", "Incorrect Password");
            }
            else
            {
                DialogResult = DialogResult.OK;     // report to caller that OK was clicked
                Close();                            // Close dialog
            }
        }

        private string RealPassword
        {
            get
            {
                return Settings.Default.superAdminPassword;
            }
        }
    }
}
