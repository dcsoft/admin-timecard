﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;



namespace AdminTimecard
{
    public partial class FormTCUnsubmitted : Form
    {
        public FormTCUnsubmitted()
        {
            InitializeComponent();
            Visible = false;

            radGridView1.MultiSelect = false;
            radGridView1.DataSource = unsubmittedTimesheets;
            radGridView1.MasterGridViewTemplate.Columns["Ending"].FormatString = @"{0:d}";

            radGridView1.MasterGridViewTemplate.Columns["Ending"].TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radGridView1.MasterGridViewTemplate.Columns["First"].TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radGridView1.MasterGridViewTemplate.Columns["Last"].TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radGridView1.MasterGridViewTemplate.Columns["Company"].TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
        }

        private void FormTCUnsubmitted_Load(object sender, EventArgs e)
        {
            // Default Report date to today
            datePicker.Value = DateTime.Now;
        }

        private void FormTCUnsubmitted_VisibleChanged(object sender, EventArgs e)
        {
            // this is called several times even when Visible hasn't changed
            if (Visible == lastVisible)
                return;

            lastVisible = Visible;

            if (!Visible)
                return;

            // Re-run current report (if any) to reflect latest changes
            // (possibly made in other screens before switching back to this one.)
            if (gridFilled)
                FillGrid();
        }

        private void btnCreateReport_Click(object sender, EventArgs e)
        {
            FillGrid();
        }


        private void FillGrid()
        {
            // Loading the Approved Timesheets data source causes the grid to fill
            // Force loading so any changes made in other pages to the approved 
            // timesheets are shown
            DateTime reportDate = datePicker.Value.Date;
            using (WaitCursor wc = new WaitCursor())
            {
                gridFilled = true;
                isFilling = true;
                unsubmittedTimesheets.Clear();

                // Iterate jobs, insert missing timecards for all weeks starting from ActiveStartDate
                // thru report 'as of' date.  Inserted timecards have a TimecardStatus of -1 meaning 'Saved'.
                try
                {
                    var jobsAdapter = new winployDataSetTableAdapters.JobsAllTableAdapter();
                    jobsAdapter.ConnectionString = Form1.ConnectionString;
                    winployDataSet.JobsAllDataTable jobs = jobsAdapter.GetData();
                    foreach (winployDataSet.JobsAllRow job in jobs)
                    {
                        var submittedAdapter = new winployDataSetTableAdapters.SubmittedTimesheetsTableAdapter();
                        submittedAdapter.ConnectionString = Form1.ConnectionString;
                        winployDataSet.SubmittedTimesheetsDataTable jobSubmits = submittedAdapter.GetByJob(job.JobId, reportDate);

                        // Skip this job if ActiveStartDate == ActiveStopDate;
                        // this means the job wasn't active at all, so no timesheets should have been submitted
                        // for this job.  
                        DateTime dStart = job.ActiveStartDate;
                        DateTime dStop = job.ActiveStopDate;
                        if (dStop <= dStart)
                            continue;

                        // First report needs to be first Sunday after ActiveStartDate
                        int addDays = 0;
                        switch (dStart.DayOfWeek)
                        {
                            case DayOfWeek.Sunday: addDays = 0; break;
                            case DayOfWeek.Saturday: addDays = 1; break;
                            case DayOfWeek.Friday: addDays = 2; break;
                            case DayOfWeek.Thursday: addDays = 3; break;
                            case DayOfWeek.Wednesday: addDays = 4; break;
                            case DayOfWeek.Tuesday: addDays = 5; break;
                            case DayOfWeek.Monday: addDays = 6; break;
                        }
                        dStart = dStart.AddDays(addDays);


                        // Last report needs to be first Sunday after ActiveStopDate...
                        addDays = 0;
                        switch (dStop.DayOfWeek)
                        {
                            case DayOfWeek.Sunday: addDays = 0; break;
                            case DayOfWeek.Saturday: addDays = 1; break;
                            case DayOfWeek.Friday: addDays = 2; break;
                            case DayOfWeek.Thursday: addDays = 3; break;
                            case DayOfWeek.Wednesday: addDays = 4; break;
                            case DayOfWeek.Tuesday: addDays = 5; break;
                            case DayOfWeek.Monday: addDays = 6; break;
                        }
                        dStop = dStop.AddDays(addDays);

                        // ... but never more than the Report Date
                        if (reportDate < dStop)
                            dStop = reportDate;


                        // Check each Week Ending date in period
                        for (DateTime d = dStart; d <= dStop; d = d.AddDays(7))
                        {
                            // Search jobTimesheets for weekending in d
                            bool unsubmitted = true;
                            bool searchSubmits = true;
                            for (int i = 0; searchSubmits && (i < jobSubmits.Count); i++)
                            {
                                winployDataSet.SubmittedTimesheetsRow timesheet = jobSubmits[i];
                                DateTime dSheet = timesheet.WeekEndingDate;
                                if (dSheet >= d)         // found d or went past it because it is missing
                                {
                                    searchSubmits = false;          // stop iterating jobSubmits
                                    unsubmitted = (dSheet > d);     // date is not submitted if we went past it
                                }
                            }

                            if (unsubmitted)   // Date was not submitted
                            {
                                unsubmittedTimesheets.Add(new UnsubmittedTimesheet(d, job.ContractorFirstName,
                                                                                   job.ContractorLastName,
                                                                                   job.CustomerCompany));
                            }
                        }
                    }
                }
                finally
                {
                    isFilling = false;
                }
            }

            int numUnSubmitted = unsubmittedTimesheets.Count;
            if (numUnSubmitted == 0)
                radLabelHeading.Text = string.Format("No unsubmitted timecards as of {0}", reportDate.ToString("d"));
            else if (numUnSubmitted == 1)
                radLabelHeading.Text = string.Format("1 unsubmitted timecard as of {0}", reportDate.ToString("d"));
            else
                radLabelHeading.Text = string.Format("{0} unsubmitted timecards as of {1}", numUnSubmitted, reportDate.ToString("d"));

            // Enable or disable Export button depending if there are any jobs in the report
            btnExport.Enabled = (numUnSubmitted >= 0);

            radGridView1.MasterGridViewTemplate.BestFitColumns();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            // Get filename to export to
            if (!FileExport.ShowSaveFileDialog(ref csvFilePath))
                return;

            // Write export file
            string header = @"Ending,First,Last,Company";
            try
            {
                using (FileStream fs = new FileStream(csvFilePath, FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs))
                    {
                        // Write header
                        w.WriteLine(header);

                        // Write each row
                        StringBuilder b = new StringBuilder();
                        foreach (UnsubmittedTimesheet row in unsubmittedTimesheets)
                        {
                            b.AppendFormat("{0}", FileExport.QuoteCSVItem(row.Ending.ToString("d")));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.First));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.Last));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.Company));
                            b.AppendLine();
                        }
                        w.WriteLine(b);
                    }
                }
            }
            catch (Exception ex)
            {
                // Inform error
                MessageBox.Show(ex.Message, "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Prompt user to open newly exported file
            FileExport.OpenExportedFile(csvFilePath);
        }


        BindingList<UnsubmittedTimesheet> unsubmittedTimesheets = new BindingList<UnsubmittedTimesheet>();


        bool lastVisible;
        bool isFilling;
        bool gridFilled;
        string csvFilePath;
    }

    class UnsubmittedTimesheet
    {
        public UnsubmittedTimesheet(DateTime weekEndingDate, string contractorFirstName, string contractorLastName, string customerCompany)
        {
            this.Ending = weekEndingDate;
            this.First = contractorFirstName;
            this.Last = contractorLastName;
            this.Company = customerCompany;
        }

        public DateTime Ending
        {
            get;
            set;
        }

        public string First
        {
            get;
            set;
        }
        public string Last
        {
            get;
            set;
        }

        public string Company
        {
            get;
            set;
        }
    }
}
