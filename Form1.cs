﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using AdminTimecard.Properties;
using Telerik.WinControls;
using Telerik.WinControls.UI;


namespace AdminTimecard
{
    public partial class Form1 : Form
    {
        enum Server
        {
            WinployServer,
            DCSoftServer,
            ErgServer,
        }

        public Form1()
        {
            InitializeComponent();

            // Add server-related items in order of Server enum
            cbToolbarConnections.Items.Clear();     // clear Designer items
            cbToolbarConnections.Items.Add(new RadComboBoxItem("winploy.com", new ServerInfo(Settings.Default.winployConnectionString, Settings.Default.winployTimecardDbEntities, Settings.Default.winployServicesUrl)));
            cbToolbarConnections.Items.Add(new RadComboBoxItem("dcsoft.com", new ServerInfo(Settings.Default.dcsoftConnectionString, Settings.Default.dcsoftTimecardDbEntities, Settings.Default.dcsoftServiceUrl)));
//#if !DEBUG
            cbToolbarConnections.Items.Add(new RadComboBoxItem("ergtimecard.com", new ServerInfo(Settings.Default.ergConnectionString, Settings.Default.ergTimecardDbEntities, Settings.Default.ergServiceUrl)));
//#endif
        }

        static public string ConnectionString
        {
            private set;
            get;
        }

        static public string EntitiesConnectionString
        {
            private set;
            get;
        }

        static public string AdminServicesUrl
        {
            private set;
            get;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Update caption text
            // Append the AssemblyFileVersion (not the Publish build, because this app is not published via ClickOnce)
            Text = String.Format("ERG Timecard Builder - Build {0}",
                                  Process.GetCurrentProcess().MainModule.FileVersionInfo.FilePrivatePart);

            // Init website-based settings - default to ergtimecard.com unless "/dcsoft" is on command-line
            // or it is debug build
#if DEBUG
            cbToolbarConnections.SelectedIndex = (int) Server.WinployServer;
#else
            cbToolbarConnections.SelectedIndex = (int) Server.ErgServer;
#endif

            // Load our theme instead of default Office2007Black (our theme fixes some styles)
            ThemeResolutionService.LoadPackageResource("AdminTimecard.ERGBlackTheme.tssp");
            cbToolbarTheme.SelectedIndex = 0;   // select first theme in combo
            //Theme = ETheme.ERGBlack;

//             foreach (Theme theme in ThemeResolutionService.GetAvailableThemes())
//                 Debug.WriteLine(theme.ThemeName);

            // Show Timecards Contractor panel
            radPanelBarGroupElementTimecards.Selected = true;
            radPanelBarGroupElementTimecards.Expanded = true;
            WorkForm = frmTCContractor;
            Timecards_Contractor_Click(null, null);
        }

        private void btnToolbarRefresh_Click(object sender, EventArgs e)
        {
            // Slight hack - re-show current form in order to update it
            Form curForm = WorkForm;
            WorkForm = null;
            WorkForm = curForm;
        }

        private void MenuExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ToolbarExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbToolbarConnections_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set new connection string for selected connection
            ServerInfo serverInfo = (ServerInfo) ((RadComboBoxItem) cbToolbarConnections.Items[cbToolbarConnections.SelectedIndex]).Value;
            ConnectionString = serverInfo.m_connectionString;
            EntitiesConnectionString = serverInfo.m_entitiesConnectionString;
            AdminServicesUrl = serverInfo.m_servicesUrl;

            // Redraw active form with data from new connection
            btnToolbarRefresh_Click(null, null);
        }

        private void radPanelBar1_PanelBarGroupSelected(object sender, Telerik.WinControls.UI.PanelBarGroupEventArgs args)
        {
            // Show default pane of group
            switch (args.Group.Caption.ToLower())
            {
                case "timecards":
                    Timecards_Contractor_Click(null, null);
                    break;

                case "quickbooks":
                    Quickbooks_Bill_Click(null, null);
                    break;

                case "jobs":
                    Jobs_Administer_Click(null, null);
                    break;

                case "reports":
                    Reports_Health_Click(null, null);
                    break;
            }
        }

        // RadPanelBar Events
        private void Timecards_Contractor_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmTCContractor == null)
                    frmTCContractor = new FormTCContractor();

                WorkForm = frmTCContractor;
            }
        }

        private void Timecards_NotApproved_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmTCSubmitted == null)
                    frmTCSubmitted = new FormTCSubmitted();

                WorkForm = frmTCSubmitted;
            }
        }

        private void Timecards_NotSubmitted_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmTCUnsubmitted == null)
                    frmTCUnsubmitted = new FormTCUnsubmitted();

                WorkForm = frmTCUnsubmitted;
            }
        }


        private void TimecardsNotInvoiced_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmTCApproved== null)
                    frmTCApproved = new FormTCApproved();

                WorkForm = frmTCApproved;
            }
        }

        private void Quickbooks_Map_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmQBMap == null)
                    frmQBMap = new FormQBMap();

                WorkForm = frmQBMap;
            }
        }

        private void Quickbooks_Bill_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmQBBill == null)
                    frmQBBill = new FormQBBill();

                WorkForm = frmQBBill;
            }
        }

        private void Jobs_Administer_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmJobAdmin == null)
                    frmJobAdmin = new FormJobAdmin();

                WorkForm = frmJobAdmin;
            }
        }

        private void Jobs_Report_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmJobReport == null)
                    frmJobReport = new FormJobReport();

                WorkForm = frmJobReport;
            }
        }

        private void Reports_Health_Click(object sender, EventArgs e)
        {
            using (WaitCursor wc = new WaitCursor())
            {
                if (frmHealth == null)
                    frmHealth = new FormReportHealth();

                WorkForm = frmHealth;
            }
        }


        /// <summary>
        /// The visible panel corresponding to the task clicked in the RadPanelBar
        /// </summary>
        private Form workForm;
        private Form WorkForm
        {
            get { return workForm; }
            set
            {
                if (workForm == value) return;

                // Hide and Remove current active panel
                if (workForm != null)
                {
                    workForm.Visible = false;
                    Controls.Remove(workForm);
                }

                // Activate new one
                workForm = value;
                if (workForm == null) return;

                // Add new active panel
                workForm.TopLevel = false;
                workForm.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
                workForm.FormBorderStyle = FormBorderStyle.None;
                Controls.Add(workForm);

                // Anchor workForm to this form so that it fills area when this form is resized

                // Ensure form is positioned and sized to fill current area
                workForm.Left = 252;
                workForm.Top = 50;
                workForm.Width = 825 + (this.Width - 1093);
                workForm.Height = radPanelBar1.Height;  // 596 + (this.Height - 655 - 50);       // - 24

                workForm.Visible = true;
            }
        }

        private FormTCContractor frmTCContractor;
        private FormTCApproved frmTCApproved;
        private FormTCUnsubmitted frmTCUnsubmitted;
        private FormTCSubmitted frmTCSubmitted;
        private FormQBMap frmQBMap;
        private FormQBBill frmQBBill;
        private FormJobAdmin frmJobAdmin;
        private FormJobReport frmJobReport;
        private FormReportHealth frmHealth;

        private void cbToolbarTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Must match above enum and the contents of the cbToolbarTheme.
            string[] themeNames = 
            {
                "ERGBlackTheme",
                "ControlDefault",
                "Office2007Silver",
                "Office2010",
                "Vista",
                "Telerik",
                "Desert",
            };

            Debug.Assert(cbToolbarTheme.SelectedIndex < themeNames.GetLength(0));
            ThemeResolutionService.ApplicationThemeName = themeNames[cbToolbarTheme.SelectedIndex];
        }

        // Always instantiated (slow by easy)
        private Telerik.WinControls.Themes.Office2007BlackTheme m_officeBlackTheme = new Telerik.WinControls.Themes.Office2007BlackTheme();
        private Telerik.WinControls.Themes.Office2007SilverTheme m_officeSilverTheme = new Telerik.WinControls.Themes.Office2007SilverTheme();
        private Telerik.WinControls.Themes.Office2010Theme m_office2010Theme = new Telerik.WinControls.Themes.Office2010Theme();

        private Telerik.WinControls.Themes.VistaTheme m_vistaTheme = new Telerik.WinControls.Themes.VistaTheme();
        private Telerik.WinControls.Themes.TelerikTheme m_telerikTheme = new Telerik.WinControls.Themes.TelerikTheme();
        private Telerik.WinControls.Themes.DesertTheme m_desertTheme = new Telerik.WinControls.Themes.DesertTheme();
        //         private Telerik.WinControls.Themes.Windows7Theme m_win7Theme = new Telerik.WinControls.Themes.Windows7Theme();

    }

    class ServerInfo
    {
        public ServerInfo (string connectionString, string entitiesConnectionString, string servicesUrl)
        {
            m_connectionString = connectionString;
            m_entitiesConnectionString = entitiesConnectionString;
            m_servicesUrl = servicesUrl;
        }

        public string m_connectionString;
        public string m_entitiesConnectionString;
        public string m_servicesUrl;
    }

    public class TimecardStage
    {
        public enum TimecardStatus : short
        {
            Saved = -1,
            Submitted = 0,
            Approved = 1,
            InvoicedBilled = 2,
            Billed = 3,
            Invoiced = 4
        }

        static public bool IsInvoiced(TimecardStatus status)
        {
            return (status == TimecardStatus.Invoiced || status == TimecardStatus.InvoicedBilled);
        }

        static public bool IsInvoiced(short status)
        {
            return IsInvoiced( (TimecardStatus) status);
        }

        static public bool IsBilled(TimecardStatus status)
        {
            return (status == TimecardStatus.Billed || status == TimecardStatus.InvoicedBilled);
        }

        static public bool IsBilled(short status)
        {
            return IsBilled((TimecardStatus)status);
        }

        static public TimecardStatus SetBilled(TimecardStatus status)
        {
            switch (status)
            {
                case TimecardStatus.Billed:
                case TimecardStatus.InvoicedBilled:
                    break;          // already "billed"; nothing to do

                case TimecardStatus.Invoiced:
                    status = TimecardStatus.InvoicedBilled;
                    break;

                default:
                    status = TimecardStatus.Billed;
                    break;
            }

            return status;
        }

        static public TimecardStatus SetBilled(short status)
        {
            return SetBilled((TimecardStatus)status);
        }

        static public TimecardStatus SetInvoiced(TimecardStatus status, bool needBill)
        {
            switch (status)
            {
                case TimecardStatus.Invoiced:
                case TimecardStatus.InvoicedBilled:
                    break;          // already "invoiced"; nothing to do

                case TimecardStatus.Billed:
                    status = TimecardStatus.InvoicedBilled;
                    break;

                default:
                    // If a bill is needed, set to Invoiced to so indicate; else only an invoice
                    // is needed, so we are done and set to InvoicedBilled to so indicate.
                    status = (needBill) ? TimecardStatus.Invoiced : TimecardStatus.InvoicedBilled;
                    break;
            }

            return status;
        }

        static public TimecardStatus SetInvoiced(short status, bool needBill)
        {
            return SetInvoiced((TimecardStatus)status, needBill);
        }

    }
}
