﻿using System.Windows.Forms;


namespace AdminTimecard
{
    partial class FormTCApproved
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radLabelHeading = new Telerik.WinControls.UI.RadLabel();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.approvedTimesheetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.winployDataSet = new AdminTimecard.winployDataSet();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCreateReport = new Telerik.WinControls.UI.RadButton();
            this.btnExport = new Telerik.WinControls.UI.RadButton();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.approvedTimesheetsTableAdapter = new AdminTimecard.winployDataSetTableAdapters.ApprovedTimesheetsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.approvedTimesheetsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabelHeading
            // 
            this.radLabelHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelHeading.Location = new System.Drawing.Point(12, 69);
            this.radLabelHeading.Name = "radLabelHeading";
            this.radLabelHeading.Size = new System.Drawing.Size(159, 17);
            this.radLabelHeading.TabIndex = 1;
            this.radLabelHeading.Text = "Please click Create Report";
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.BackColor = System.Drawing.SystemColors.Control;
            this.radGridView1.BeginEditMode = Telerik.WinControls.RadGridViewBeginEditMode.BeginEditProgrammatically;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(14, 91);
            // 
            // 
            // 
            this.radGridView1.MasterGridViewTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterGridViewTemplate.AllowCellContextMenu = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnChooser = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView1.MasterGridViewTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterGridViewTemplate.AllowDragToGroup = false;
            this.radGridView1.MasterGridViewTemplate.AllowRowResize = false;
            gridViewDateTimeColumn1.DataType = typeof(System.DateTime);
            gridViewDateTimeColumn1.FieldAlias = "WeekEndingDate";
            gridViewDateTimeColumn1.FieldName = "WeekEndingDate";
            gridViewDateTimeColumn1.FormatString = "{0:d}";
            gridViewDateTimeColumn1.HeaderText = "Ending";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.UniqueName = "WeekEndingDate";
            gridViewDateTimeColumn1.Width = 95;
            gridViewDateTimeColumn2.DataType = typeof(System.DateTime);
            gridViewDateTimeColumn2.FieldName = "SubmitalTimeStamp";
            gridViewDateTimeColumn2.FormatString = "{0:d}";
            gridViewDateTimeColumn2.HeaderText = "Submited";
            gridViewDateTimeColumn2.ReadOnly = true;
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.UniqueName = "SubmitalTimeStamp";
            gridViewDateTimeColumn2.Width = 95;
            gridViewTextBoxColumn1.FieldAlias = "Approved";
            gridViewTextBoxColumn1.FieldName = "ApprovalTimestamp";
            gridViewTextBoxColumn1.FormatString = "{0:d}";
            gridViewTextBoxColumn1.HeaderText = "Approved";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.UniqueName = "Approved";
            gridViewTextBoxColumn2.FieldAlias = "Hours";
            gridViewTextBoxColumn2.FieldName = "HoursTotal";
            gridViewTextBoxColumn2.HeaderText = "Hours";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.UniqueName = "Hours";
            gridViewTextBoxColumn2.Width = 95;
            gridViewTextBoxColumn3.FieldAlias = "ContractorFirstName";
            gridViewTextBoxColumn3.FieldName = "ContractorFirstName";
            gridViewTextBoxColumn3.HeaderText = "First";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.UniqueName = "ContractorFirstName";
            gridViewTextBoxColumn3.Width = 95;
            gridViewTextBoxColumn4.FieldAlias = "ContractorLastName";
            gridViewTextBoxColumn4.FieldName = "ContractorLastName";
            gridViewTextBoxColumn4.HeaderText = "Last";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.UniqueName = "ContractorLastName";
            gridViewTextBoxColumn4.Width = 95;
            gridViewTextBoxColumn5.FieldAlias = "CustomerCompany";
            gridViewTextBoxColumn5.FieldName = "CustomerCompany";
            gridViewTextBoxColumn5.HeaderText = "Customer";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.UniqueName = "CustomerCompany";
            gridViewTextBoxColumn5.Width = 95;
            gridViewTextBoxColumn6.FieldAlias = "Vendor";
            gridViewTextBoxColumn6.FieldName = "VendorName";
            gridViewTextBoxColumn6.HeaderText = "Vendor";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.UniqueName = "Vendor";
            gridViewTextBoxColumn6.Width = 95;
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewDateTimeColumn1);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewDateTimeColumn2);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn1);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn2);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn3);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn4);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn5);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn6);
            this.radGridView1.MasterGridViewTemplate.DataSource = this.approvedTimesheetsBindingSource;
            this.radGridView1.MasterGridViewTemplate.EnableGrouping = false;
            this.radGridView1.MasterGridViewTemplate.ShowFilteringRow = false;
            this.radGridView1.MasterGridViewTemplate.ShowRowHeaderColumn = false;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radGridView1.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.Size = new System.Drawing.Size(780, 428);
            this.radGridView1.TabIndex = 2;
            this.radGridView1.Text = "radGridViewPreview";
            this.radGridView1.ThemeName = "Office2007Black";
            // 
            // approvedTimesheetsBindingSource
            // 
            this.approvedTimesheetsBindingSource.DataMember = "ApprovedTimesheets";
            this.approvedTimesheetsBindingSource.DataSource = this.winployDataSet;
            // 
            // winployDataSet
            // 
            this.winployDataSet.DataSetName = "winployDataSet";
            this.winployDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnCreateReport);
            this.groupBox2.Controls.Add(this.btnExport);
            this.groupBox2.Controls.Add(this.datePicker);
            this.groupBox2.Controls.Add(this.radLabel4);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(782, 51);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Show uninvoiced timecards as of";
            // 
            // btnCreateReport
            // 
            this.btnCreateReport.Location = new System.Drawing.Point(187, 20);
            this.btnCreateReport.Name = "btnCreateReport";
            this.btnCreateReport.Size = new System.Drawing.Size(115, 21);
            this.btnCreateReport.TabIndex = 2;
            this.btnCreateReport.Text = "Create Report";
            this.btnCreateReport.ThemeName = "Office2007Black";
            this.btnCreateReport.Click += new System.EventHandler(this.btnCreateReport_Click);
            // 
            // btnExport
            // 
            this.btnExport.Enabled = false;
            this.btnExport.Location = new System.Drawing.Point(308, 20);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(109, 21);
            this.btnExport.TabIndex = 3;
            this.btnExport.Text = "Export Report ...";
            this.btnExport.ThemeName = "Office2007Black";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // datePicker
            // 
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(86, 20);
            this.datePicker.MaxDate = new System.DateTime(2049, 12, 31, 0, 0, 0, 0);
            this.datePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(95, 21);
            this.datePicker.TabIndex = 1;
            this.datePicker.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(6, 20);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(76, 17);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Report Date";
            // 
            // approvedTimesheetsTableAdapter
            // 
            this.approvedTimesheetsTableAdapter.ClearBeforeFill = true;
            // 
            // FormTCApproved
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.radLabelHeading);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormTCApproved";
            this.Text = "FormTCApproved";
            this.Load += new System.EventHandler(this.FormTCApproved_Load);
            this.VisibleChanged += new System.EventHandler(this.FormTCApproved_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.approvedTimesheetsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabelHeading;
        private Telerik.WinControls.UI.RadGridView radGridView1;

        private winployDataSet winployDataSet;
        private winployDataSetTableAdapters.ApprovedTimesheetsTableAdapter approvedTimesheetsTableAdapter;
        private BindingSource approvedTimesheetsBindingSource;
        private GroupBox groupBox2;
        private Telerik.WinControls.UI.RadButton btnCreateReport;
        private Telerik.WinControls.UI.RadButton btnExport;
        private DateTimePicker datePicker;
        private Telerik.WinControls.UI.RadLabel radLabel4;
    }
}