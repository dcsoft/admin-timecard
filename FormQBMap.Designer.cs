﻿using System.Windows.Forms;


namespace AdminTimecard
{
    partial class FormQBMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbQBItems = new Telerik.WinControls.UI.RadListBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.rlQBError = new Telerik.WinControls.UI.RadLabel();
            this.rlRetryQB = new Telerik.WinControls.UI.RadLabel();
            this.rlListTitle = new Telerik.WinControls.UI.RadLabel();
            this.jobsAllBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.winployDataSet = new AdminTimecard.winployDataSet();
            this.cbShowOnlyMislinkedJobs = new Telerik.WinControls.UI.RadCheckBox();
            this.jobsAllTableAdapter = new AdminTimecard.winployDataSetTableAdapters.JobsAllTableAdapter();
            this.lbJobs = new Telerik.WinControls.UI.RadListBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tvQBCustSalesOrders = new Telerik.WinControls.UI.RadTreeView();
            this.lbQBVendors = new Telerik.WinControls.UI.RadListBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.rlCompareVendor = new Telerik.WinControls.UI.RadLabel();
            this.rlCompareItem = new Telerik.WinControls.UI.RadLabel();
            this.rlCompareCustomer = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.rlCompareSalesOrder = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.cbShowActiveOnly = new Telerik.WinControls.UI.RadCheckBox();
            this.labelHorizontalLine = new System.Windows.Forms.Label();
            this.rbOK = new Telerik.WinControls.UI.RadButton();
            this.rbCancel = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.lbQBItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlQBError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlRetryQB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlListTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobsAllBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbShowOnlyMislinkedJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvQBCustSalesOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbQBVendors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlCompareVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlCompareItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlCompareCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlCompareSalesOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbShowActiveOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbCancel)).BeginInit();
            this.SuspendLayout();
            // 
            // lbQBItems
            // 
            this.lbQBItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbQBItems.FormatString = "{0}";
            this.lbQBItems.Location = new System.Drawing.Point(316, 345);
            this.lbQBItems.Name = "lbQBItems";
            this.lbQBItems.Size = new System.Drawing.Size(233, 170);
            this.lbQBItems.TabIndex = 3;
            this.lbQBItems.SelectedIndexChanged += new System.EventHandler(this.lbQBItems_SelectedIndexChanged);
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(566, 323);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(53, 17);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "Vendors";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rlQBError
            // 
            this.rlQBError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rlQBError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlQBError.ForeColor = System.Drawing.Color.Maroon;
            this.rlQBError.Location = new System.Drawing.Point(14, 518);
            this.rlQBError.Name = "rlQBError";
            // 
            // 
            // 
            this.rlQBError.RootElement.ForeColor = System.Drawing.Color.Maroon;
            this.rlQBError.Size = new System.Drawing.Size(147, 17);
            this.rlQBError.TabIndex = 3;
            this.rlQBError.Text = "QuickBooks not running.";
            this.rlQBError.Visible = false;
            // 
            // rlRetryQB
            // 
            this.rlRetryQB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rlRetryQB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlRetryQB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.rlRetryQB.Location = new System.Drawing.Point(14, 535);
            this.rlRetryQB.Name = "rlRetryQB";
            // 
            // 
            // 
            this.rlRetryQB.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(0)))), ((int)(((byte)(129)))));
            this.rlRetryQB.Size = new System.Drawing.Size(362, 17);
            this.rlRetryQB.TabIndex = 2;
            this.rlRetryQB.Text = "Start QuickBooks and load your company file.  Click here to retry.";
            this.rlRetryQB.Visible = false;
            this.rlRetryQB.Click += new System.EventHandler(this.rlRetryQB_Click);
            // 
            // rlListTitle
            // 
            this.rlListTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlListTitle.Location = new System.Drawing.Point(14, 13);
            this.rlListTitle.Name = "rlListTitle";
            this.rlListTitle.Size = new System.Drawing.Size(34, 17);
            this.rlListTitle.TabIndex = 6;
            this.rlListTitle.Text = "Jobs";
            // 
            // jobsAllBindingSource
            // 
            this.jobsAllBindingSource.DataMember = "JobsAll";
            this.jobsAllBindingSource.DataSource = this.winployDataSet;
            // 
            // winployDataSet
            // 
            this.winployDataSet.DataSetName = "winployDataSet";
            this.winployDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cbShowOnlyMislinkedJobs
            // 
            this.cbShowOnlyMislinkedJobs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbShowOnlyMislinkedJobs.Location = new System.Drawing.Point(148, 12);
            this.cbShowOnlyMislinkedJobs.Name = "cbShowOnlyMislinkedJobs";
            // 
            // 
            // 
            this.cbShowOnlyMislinkedJobs.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbShowOnlyMislinkedJobs.Size = new System.Drawing.Size(140, 18);
            this.cbShowOnlyMislinkedJobs.TabIndex = 8;
            this.cbShowOnlyMislinkedJobs.Text = "Possibly mis-linked only";
            this.cbShowOnlyMislinkedJobs.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbShowOnlyMislinkedJobs_ToggleStateChanged);
            // 
            // jobsAllTableAdapter
            // 
            this.jobsAllTableAdapter.ClearBeforeFill = true;
            // 
            // lbJobs
            // 
            this.lbJobs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbJobs.DataSource = this.jobsAllBindingSource;
            this.lbJobs.DisplayMember = "JobId";
            this.lbJobs.FormatString = "{0}";
            this.lbJobs.Location = new System.Drawing.Point(12, 35);
            this.lbJobs.Name = "lbJobs";
            this.lbJobs.Size = new System.Drawing.Size(290, 480);
            this.lbJobs.TabIndex = 12;
            this.lbJobs.ThemeName = "ERGTheme";
            this.lbJobs.ValueMember = "JobId";
            this.lbJobs.SelectedIndexChanged += new System.EventHandler(this.lbJobs_SelectedIndexChanged);
            this.lbJobs.ItemDataBound += new Telerik.WinControls.UI.ItemDataBoundEventHandler(this.lbJobs_ItemDataBound);
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(315, 323);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(37, 17);
            this.radLabel4.TabIndex = 12;
            this.radLabel4.Text = "Items";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tvQBCustSalesOrders
            // 
            this.tvQBCustSalesOrders.AllowIncrementalSearch = true;
            this.tvQBCustSalesOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvQBCustSalesOrders.BackColor = System.Drawing.Color.White;
            this.tvQBCustSalesOrders.Cursor = System.Windows.Forms.Cursors.Default;
            this.tvQBCustSalesOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvQBCustSalesOrders.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tvQBCustSalesOrders.Location = new System.Drawing.Point(315, 149);
            this.tvQBCustSalesOrders.Name = "tvQBCustSalesOrders";
            this.tvQBCustSalesOrders.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tvQBCustSalesOrders.Size = new System.Drawing.Size(484, 168);
            this.tvQBCustSalesOrders.TabIndex = 0;
            this.tvQBCustSalesOrders.Text = "tvQBCustSalesOrders";
            this.tvQBCustSalesOrders.UseDefaultSelection = true;
            this.tvQBCustSalesOrders.Selected += new System.EventHandler(this.tvQBCustSalesOrders_Selected);
            // 
            // lbQBVendors
            // 
            this.lbQBVendors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbQBVendors.FormatString = "{0}";
            this.lbQBVendors.Location = new System.Drawing.Point(566, 345);
            this.lbQBVendors.Name = "lbQBVendors";
            this.lbQBVendors.Size = new System.Drawing.Size(233, 170);
            this.lbQBVendors.TabIndex = 5;
            this.lbQBVendors.SelectedIndexChanged += new System.EventHandler(this.lbQBVendors_SelectedIndexChanged);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(315, 13);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(109, 17);
            this.radLabel5.TabIndex = 7;
            this.radLabel5.Text = "QuickBooks Links";
            // 
            // radLabel7
            // 
            this.radLabel7.ForeColor = System.Drawing.Color.Navy;
            this.radLabel7.Location = new System.Drawing.Point(316, 35);
            this.radLabel7.Name = "radLabel7";
            // 
            // 
            // 
            this.radLabel7.RootElement.ForeColor = System.Drawing.Color.Navy;
            this.radLabel7.Size = new System.Drawing.Size(60, 17);
            this.radLabel7.TabIndex = 13;
            this.radLabel7.Text = "Customer";
            // 
            // radLabel10
            // 
            this.radLabel10.ForeColor = System.Drawing.Color.Navy;
            this.radLabel10.Location = new System.Drawing.Point(315, 89);
            this.radLabel10.Name = "radLabel10";
            // 
            // 
            // 
            this.radLabel10.RootElement.ForeColor = System.Drawing.Color.Navy;
            this.radLabel10.Size = new System.Drawing.Size(46, 17);
            this.radLabel10.TabIndex = 16;
            this.radLabel10.Text = "Vendor";
            // 
            // rlCompareVendor
            // 
            this.rlCompareVendor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rlCompareVendor.AutoSize = false;
            this.rlCompareVendor.Location = new System.Drawing.Point(407, 89);
            this.rlCompareVendor.Name = "rlCompareVendor";
            this.rlCompareVendor.Size = new System.Drawing.Size(390, 16);
            this.rlCompareVendor.TabIndex = 21;
            this.rlCompareVendor.Text = "rlCompareVendor";
            // 
            // rlCompareItem
            // 
            this.rlCompareItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rlCompareItem.AutoSize = false;
            this.rlCompareItem.Location = new System.Drawing.Point(407, 71);
            this.rlCompareItem.Name = "rlCompareItem";
            this.rlCompareItem.Size = new System.Drawing.Size(390, 16);
            this.rlCompareItem.TabIndex = 22;
            this.rlCompareItem.Text = "rlCompareItem";
            // 
            // rlCompareCustomer
            // 
            this.rlCompareCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rlCompareCustomer.AutoSize = false;
            this.rlCompareCustomer.Location = new System.Drawing.Point(407, 35);
            this.rlCompareCustomer.Name = "rlCompareCustomer";
            this.rlCompareCustomer.Size = new System.Drawing.Size(390, 16);
            this.rlCompareCustomer.TabIndex = 24;
            this.rlCompareCustomer.Text = "rlCompareCustomer";
            // 
            // radLabel18
            // 
            this.radLabel18.ForeColor = System.Drawing.Color.Navy;
            this.radLabel18.Location = new System.Drawing.Point(316, 53);
            this.radLabel18.Name = "radLabel18";
            // 
            // 
            // 
            this.radLabel18.RootElement.ForeColor = System.Drawing.Color.Navy;
            this.radLabel18.Size = new System.Drawing.Size(72, 17);
            this.radLabel18.TabIndex = 14;
            this.radLabel18.Text = "Sales Order";
            // 
            // radLabel19
            // 
            this.radLabel19.ForeColor = System.Drawing.Color.Navy;
            this.radLabel19.Location = new System.Drawing.Point(315, 71);
            this.radLabel19.Name = "radLabel19";
            // 
            // 
            // 
            this.radLabel19.RootElement.ForeColor = System.Drawing.Color.Navy;
            this.radLabel19.Size = new System.Drawing.Size(31, 17);
            this.radLabel19.TabIndex = 15;
            this.radLabel19.Text = "Item";
            // 
            // rlCompareSalesOrder
            // 
            this.rlCompareSalesOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rlCompareSalesOrder.AutoSize = false;
            this.rlCompareSalesOrder.Location = new System.Drawing.Point(407, 53);
            this.rlCompareSalesOrder.Name = "rlCompareSalesOrder";
            this.rlCompareSalesOrder.Size = new System.Drawing.Size(392, 16);
            this.rlCompareSalesOrder.TabIndex = 23;
            this.rlCompareSalesOrder.Text = "rlCompareSalesOrder";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(316, 127);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(148, 17);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Customers / Sales Orders";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel3.VisibleChanged += new System.EventHandler(this.FormQBMap_VisibleChanged);
            // 
            // cbShowActiveOnly
            // 
            this.cbShowActiveOnly.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbShowActiveOnly.Location = new System.Drawing.Point(64, 12);
            this.cbShowActiveOnly.Name = "cbShowActiveOnly";
            // 
            // 
            // 
            this.cbShowActiveOnly.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbShowActiveOnly.Size = new System.Drawing.Size(75, 18);
            this.cbShowActiveOnly.TabIndex = 25;
            this.cbShowActiveOnly.Text = "Active only";
            this.cbShowActiveOnly.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.cbShowActiveOnly.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbShowActiveOnly_ToggleStateChanged);
            // 
            // labelHorizontalLine
            // 
            this.labelHorizontalLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHorizontalLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelHorizontalLine.Location = new System.Drawing.Point(428, 22);
            this.labelHorizontalLine.Name = "labelHorizontalLine";
            this.labelHorizontalLine.Size = new System.Drawing.Size(371, 2);
            this.labelHorizontalLine.TabIndex = 26;
            // 
            // rbOK
            // 
            this.rbOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rbOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.rbOK.Location = new System.Drawing.Point(641, 523);
            this.rbOK.Name = "rbOK";
            this.rbOK.Size = new System.Drawing.Size(75, 23);
            this.rbOK.TabIndex = 27;
            this.rbOK.Text = "OK";
            this.rbOK.ThemeName = "Office2007Black";
            this.rbOK.Visible = false;
            // 
            // rbCancel
            // 
            this.rbCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rbCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.rbCancel.Location = new System.Drawing.Point(722, 523);
            this.rbCancel.Name = "rbCancel";
            this.rbCancel.Size = new System.Drawing.Size(75, 23);
            this.rbCancel.TabIndex = 28;
            this.rbCancel.Text = "&Cancel";
            this.rbCancel.ThemeName = "Office2007Black";
            this.rbCancel.Visible = false;
            // 
            // FormQBMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.rbCancel);
            this.Controls.Add(this.rbOK);
            this.Controls.Add(this.labelHorizontalLine);
            this.Controls.Add(this.cbShowActiveOnly);
            this.Controls.Add(this.rlCompareCustomer);
            this.Controls.Add(this.rlCompareSalesOrder);
            this.Controls.Add(this.rlCompareItem);
            this.Controls.Add(this.rlCompareVendor);
            this.Controls.Add(this.radLabel19);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.radLabel18);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.lbQBItems);
            this.Controls.Add(this.tvQBCustSalesOrders);
            this.Controls.Add(this.lbJobs);
            this.Controls.Add(this.cbShowOnlyMislinkedJobs);
            this.Controls.Add(this.rlListTitle);
            this.Controls.Add(this.lbQBVendors);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.rlQBError);
            this.Controls.Add(this.rlRetryQB);
            this.Controls.Add(this.radLabel3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormQBMap";
            this.Text = "Map QuickBooks For Single Invoice";
            this.VisibleChanged += new System.EventHandler(this.FormQBMap_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.lbQBItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlQBError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlRetryQB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlListTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobsAllBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbShowOnlyMislinkedJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvQBCustSalesOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbQBVendors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlCompareVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlCompareItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlCompareCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlCompareSalesOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbShowActiveOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbCancel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel rlRetryQB;
        private Telerik.WinControls.UI.RadLabel rlQBError;
        private Telerik.WinControls.UI.RadListBox lbQBItems;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel rlListTitle;

        private BindingSource jobsAllBindingSource;
        private Telerik.WinControls.UI.RadCheckBox cbShowOnlyMislinkedJobs;
        private winployDataSet winployDataSet;
        private AdminTimecard.winployDataSetTableAdapters.JobsAllTableAdapter jobsAllTableAdapter;
        private Telerik.WinControls.UI.RadListBox lbJobs;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTreeView tvQBCustSalesOrders;
        private Telerik.WinControls.UI.RadListBox lbQBVendors;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel rlCompareVendor;
        private Telerik.WinControls.UI.RadLabel rlCompareItem;
        private Telerik.WinControls.UI.RadLabel rlCompareCustomer;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel rlCompareSalesOrder;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadCheckBox cbShowActiveOnly;
        private Label labelHorizontalLine;
        private Telerik.WinControls.UI.RadButton rbOK;
        private Telerik.WinControls.UI.RadButton rbCancel;
    }
}