﻿using System;
using System.Collections.Generic;
using QBFC7Lib;


namespace AdminTimecard
{
    class QBInvoiceGenerator : IDisposable
    {
        public void Dispose()
        {
            // Close the session and connection with QuickBooks
            if (m_sessionManager != null)
            {
                m_sessionManager.EndSession();
                m_sessionManager.CloseConnection();
            }
        }

        public QBInvoiceGenerator()
        {
            // Open the connection and begin a session to QuickBooks
            m_sessionManager = new QBSessionManager();
            m_sessionManager.OpenConnection("", "ERG Timecard Builder");
            m_sessionManager.BeginSession("", ENOpenMode.omDontCare);
        }

        public bool CreateInvoice(out string errorText, winployDataSet.ApprovedTimesheetsRow timecard)
        {
            // Create a single invoice containing a single timecard
            // is a special case of a single invoice for a list of timecards
            List<winployDataSet.ApprovedTimesheetsRow> timecards = new List<winployDataSet.ApprovedTimesheetsRow>();
            timecards.Add(timecard);
            return CreateInvoice(out errorText, timecards);
        }

        public bool CreateInvoice(out string errorText, List<winployDataSet.ApprovedTimesheetsRow> timecards)
        {
            // Verify all timecards contain the same values for:  custQB, poNumber;
            // these are shared by all invoice lines
            string globalCustName = null;       // also shared, but does not have to be unique, just custQB does
            string globalCustQB = null;
            string globalPONumber = null;
            string globalWeekEnding = null;
            foreach (winployDataSet.ApprovedTimesheetsRow timecard in timecards)
            {
                string custName = timecard["CustomerCompany"].ToString();
                string custQB = timecard["CustomerQB"].ToString();
                string poNumber = timecard["PONumber"].ToString();

                string weekEnding = ((DateTime)timecard["WeekEndingDate"]).ToShortDateString().Replace("/", "-");
                if ( globalWeekEnding == null )                 // not init
                    globalWeekEnding = weekEnding;              //   so init with first date
                else if ( globalWeekEnding != weekEnding )      // init and different
                    globalWeekEnding = "none";                  //    so set to "none" which will mismatch all subsequent timecards and remain "none"                    

                bool match = true;
                if ( string.IsNullOrEmpty(globalCustName) )     // use the customer name from the first timecard
                    globalCustName = custName;
//                 else
//                     match = match && ( globalCustName == custName );

                if ( string.IsNullOrEmpty(globalCustQB) )
                    globalCustQB = custQB;
                else
                    match = match && ( globalCustQB == custQB );

                if ( string.IsNullOrEmpty(globalPONumber) )
                    globalPONumber = poNumber;
                else
                    match = match && ( globalPONumber == poNumber );

                if ( !match )
                {
                    errorText = "All selected timecards must have the same Customer QuickBooks Mapping and PO Number to be on the same single invoice.  Single invoice not created.";
                    return false;
                }
            }

            // Verify that none of the timecards have the same Sales Order/Sales Order item guids;
            // QB disallows
            foreach (winployDataSet.ApprovedTimesheetsRow timecard in timecards)
            {
                string salesOrderQB = timecard["SalesOrderQB"].ToString();
                string salesOrderItemQB = timecard["SalesOrderItemQB"].ToString();
                if (string.IsNullOrEmpty(salesOrderQB) || string.IsNullOrEmpty(salesOrderItemQB))   // skip if not using sales order 
                    continue;

                foreach (winployDataSet.ApprovedTimesheetsRow timecard2 in timecards)
                {
                    if (timecard.TimesheetId == timecard2.TimesheetId) // don't compare to itself
                        continue;

                    string salesOrderQB2 = timecard2["SalesOrderQB"].ToString();
                    string salesOrderItemQB2 = timecard2["SalesOrderItemQB"].ToString();
                    if (string.IsNullOrEmpty(salesOrderQB2) || string.IsNullOrEmpty(salesOrderItemQB2))   // skip if not using sales order 
                        continue;

                    if ((salesOrderQB == salesOrderQB2) && (salesOrderItemQB == salesOrderItemQB2))
                    {
                        errorText = "QuickBooks does not allow one invoice to contain multiple lines referencing the same sales order/sales order item.  Single invoice not created.";
                        return false;
                    }
                }
            }

            // Get the RequestMsgSet based on the correct QB Version
            IMsgSetRequest requestMsgSet = QBUtils.GetLatestMsgSetRequest(m_sessionManager);
            requestMsgSet.Attributes.OnError = ENRqOnError.roeStop;

            // Add the request to the message set request object
            IInvoiceAdd invoiceAdd = requestMsgSet.AppendInvoiceAddRq();
            invoiceAdd.CustomerRef.FullName.SetValue(globalCustName);
            invoiceAdd.CustomerRef.ListID.SetValue(globalCustQB);


            // Invoice Date is today's date
            DateTime thisDate = DateTime.Now;
            string invoiceDate = Convert.ToString(thisDate.Year) + "-";
            if (thisDate.Month < 10)
                invoiceDate = invoiceDate + "0";
            invoiceDate = invoiceDate + Convert.ToString(thisDate.Month) + "-";
            if (thisDate.Day < 10)
                invoiceDate = invoiceDate + "0";
            invoiceDate = invoiceDate + Convert.ToString(thisDate.Day);

            invoiceAdd.TxnDate.SetValue(Convert.ToDateTime(invoiceDate));

            // Not setting RefNumber causes QuickBooks to auto increment the invoice
            //invoiceAdd.RefNumber.SetValue(invoiceNum);

            invoiceAdd.TemplateRef.FullName.SetValue("Contract Invoice");   //("ERG Invoice Template");


            // P.O. Number
            invoiceAdd.PONumber.SetValue(globalPONumber);

            //Memo - WE: only if all timecards have same WeekEnding
            // QB won't add the invoice if the invoice contains lines referencing sales order/sales order items
            if ( (globalWeekEnding != null) && (globalWeekEnding != "none") )
            {
                string memo = "WE: " + globalWeekEnding;
                invoiceAdd.Memo.SetValue(memo);
            }

            invoiceAdd.IsToBePrinted.SetValue(true);

            // Add each timecard on a separate line
            foreach (winployDataSet.ApprovedTimesheetsRow timecard in timecards)
            {
                string itemQB = timecard["ContractorQB"].ToString();
                string itemName = timecard["ContractorFirstName"] + " " + timecard["ContractorLastName"];
                string salesOrderQB = timecard["SalesOrderQB"].ToString();
                string salesOrderItemQB = timecard["SalesOrderItemQB"].ToString();
                string weekEnding = ((DateTime)timecard["WeekEndingDate"]).ToShortDateString().Replace("/", "-");
                string totalHours = timecard["HoursTotal"].ToString();

                // InvoiceLineAdd
                IInvoiceLineAdd invoiceLineAdd = invoiceAdd.ORInvoiceLineAddList.Append().InvoiceLineAdd;

                string strDesc = string.Format("Contract services for {0} WE {1}, {2} hours",
                                               itemName, weekEnding, totalHours);
                invoiceLineAdd.Desc.SetValue(strDesc);

                invoiceLineAdd.Quantity.SetValue(Convert.ToDouble(totalHours));

                // Specify either the Sales order info, or the Item info directly (but not both)
                // Source:  http://idnforums2.intuit.com/messageview.aspx?catid=7&threadid=5805&STARTPAGE=1
                if (!string.IsNullOrEmpty(salesOrderQB) && !string.IsNullOrEmpty(salesOrderItemQB)) // both are specified
                {
                    invoiceLineAdd.LinkToTxn.TxnID.SetValue(salesOrderQB);
                    invoiceLineAdd.LinkToTxn.TxnLineID.SetValue(salesOrderItemQB);
                }
                else
                {
                    invoiceLineAdd.ItemRef.FullName.SetValue(itemName);
                    invoiceLineAdd.ItemRef.ListID.SetValue(itemQB);
                }
            }


            // If all inputs are in, perform the request and obtain a response from QuickBooks
            IMsgSetResponse responseMsgSet = m_sessionManager.DoRequests(requestMsgSet);
            IResponse response = responseMsgSet.ResponseList.GetAt(0);
            errorText = ResponseText(response);

            //string responseXML = responseMsgSet.ToXMLString();
            //MessageBox.Show(responseXML);

            return (response.StatusCode == 0);
        }

        public bool CreateItemReceipt(out string errorText,
                                string vendorQB, string customerQB,
                                string itemQB, string itemName,
                                string totalHours, DateTime weekEndingDate)
        {
            // Get the RequestMsgSet based on the correct QB Version
            IMsgSetRequest requestMsgSet = QBUtils.GetLatestMsgSetRequest(m_sessionManager);
            requestMsgSet.Attributes.OnError = ENRqOnError.roeStop;

            // Add the request to the message set request object                
            IItemReceiptAdd billAdd = requestMsgSet.AppendItemReceiptAddRq();

            // Set the IInvoiceAdd field values
            // Vendor:id
            billAdd.VendorRef.ListID.SetValue(vendorQB);

            DateTime invoiceDate = weekEndingDate.AddDays(1);
            billAdd.TxnDate.SetValue(invoiceDate);


            //Memo 
            string strMemo = string.Format(" WE: {0}", weekEndingDate.ToShortDateString());
            billAdd.Memo.SetValue(strMemo);

            // InvoiceLineAdd
            IItemLineAdd billAddLine = billAdd.ORItemLineAddList.Append().ItemLineAdd;
            billAddLine.ItemRef.FullName.SetValue(itemName);
            billAddLine.ItemRef.ListID.SetValue(itemQB);
            billAddLine.BillableStatus.SetValue(ENBillableStatus.bsNotBillable);

            string strDesc = "WE " + weekEndingDate.ToShortDateString();
            billAddLine.Desc.SetValue(strDesc);

            billAddLine.Quantity.SetValue(Convert.ToDouble(totalHours));

            billAddLine.CustomerRef.ListID.SetValue(customerQB);

            // If all inputs are in, perform the request and obtain a response from QuickBooks
            IMsgSetResponse responseMsgSet = m_sessionManager.DoRequests(requestMsgSet);
            IResponse response = responseMsgSet.ResponseList.GetAt(0);
            errorText = ResponseText(response);

            return (response.StatusCode == 0);
        }


        private string ResponseText(IResponse response)
        {
            return (response.StatusCode != 0) ? response.StatusMessage : null;
        }


        // Create the session manager object using QBFC
        QBSessionManager m_sessionManager;

    }
}
