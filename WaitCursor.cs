using System;
using System.Windows.Forms;


namespace AdminTimecard
{
	public class WaitCursor : IDisposable
	{
        public WaitCursor()
        {
            // Save current cursor
            currentCursor = Cursor.Current;

            // Show hour glass cursor
            Cursor.Current = Cursors.WaitCursor;
        }

	    public void Dispose()
	    {
            // Restore saved cursor
            Cursor.Current = currentCursor;
        }

        private readonly Cursor currentCursor;
	}
}