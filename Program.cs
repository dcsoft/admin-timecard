﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace AdminTimecard
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AppDomain.CurrentDomain.UnhandledException += delegate(object sender, UnhandledExceptionEventArgs args)
            {
                // Application has crashed and will show Windows crash dialog (no way to stop it).
                // Attempt to show our own message before that.  Sometimes it doesn't appear since
                // Windows terminates our app before it is shown.
                ShowExceptionMessage((Exception)args.ExceptionObject);
            };

            Application.ThreadException += delegate(object sender, ThreadExceptionEventArgs args)
            {
                // Main WinForms thread has crashed.  Show the exception.  Our app doesn't necessarily crash
                // but continues.
                ShowExceptionMessage(args.Exception);
            };

            mainForm = new Form1();
            Application.Run(mainForm);
        }

        static public void ShowExceptionMessage(Exception e)
        {
            // Show MessageBox for any unhandled exception in the entire domain
            // and not just for the UI thread as for Application.DispatcherUnhandledException
            MessageBox.Show(e.Message + "\n\n" + e.StackTrace, "ERG Timecard Builder - Unexpected Result - Please capture with Alt+PrintScreen");
        }

        static private bool isLoggedIn;
        static public bool IsLoggedIn
        {
            get
            {
                if (!isLoggedIn)
                {
                    // Prompt for password
                    isLoggedIn = PromptLogin();
                }

                return isLoggedIn;
            }
        }

        static private bool PromptLogin()
        {
            // Show password dialog
            using (FormPassword dlg = new FormPassword())
            {
                // Return whether user successfully entered password and pressed OK
                return (dlg.ShowDialog(mainForm) == DialogResult.OK);
            }
        }

        public static Form1 mainForm;
    }
}
