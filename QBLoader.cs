﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using QBFC7Lib;


namespace AdminTimecard
{
    class QBLoader : IDisposable
    {
        public void Dispose()
        {
            // Close the session and connection with QuickBooks
            if (m_sessionManager != null)
            {
                m_sessionManager.EndSession();
                m_sessionManager.CloseConnection();
            }
        }

        public QBLoader()
        {
            // Open the connection and begin a session to QuickBooks
            m_sessionManager = new QBSessionManager();
            m_sessionManager.OpenConnection("", "ERG Timecard Builder");
            m_sessionManager.BeginSession("", ENOpenMode.omDontCare);
        }

        public void FillCustomers(out List<string> customers, out List<string> custValues)
        {
            customers = new List<string>();
            custValues = new List<string>();

            // Get the RequestMsgSet based on the correct QB Version
            IMsgSetRequest requestSet = QBUtils.GetLatestMsgSetRequest(m_sessionManager);
            requestSet.Attributes.OnError = ENRqOnError.roeStop;
            requestSet.AppendCustomerQueryRq();

            // Optionally, you can put filter on it.
            // CustQ.ORCustomerListQuery.CustomerListFilter.MaxReturned.SetValue(50);

            // Do the request and get the response message set object
            IMsgSetResponse responseSet = m_sessionManager.DoRequests(requestSet);
            // string responseXML = responseSet.ToXMLString();
            IResponse response = responseSet.ResponseList.GetAt(0);
            ICustomerRetList customerRetList = response.Detail as ICustomerRetList;
            if (customerRetList != null)
            {
                for (int i = 0; i < customerRetList.Count; i++)
                {
                    ICustomerRet customerRet = customerRetList.GetAt(i);
                    customers.Add(customerRet.Name.GetValue());
                    custValues.Add(customerRet.ListID.GetValue());
                }
            }
        }

        public void FillSalesOrders(out List<SalesOrderDisplayItem> salesOrderDisplayItems)
        {
            salesOrderDisplayItems = new List<SalesOrderDisplayItem>();

            // Get the RequestMsgSet based on the correct QB Version
            IMsgSetRequest requestSet = QBUtils.GetLatestMsgSetRequest(m_sessionManager);
            requestSet.Attributes.OnError = ENRqOnError.roeStop;
            ISalesOrderQuery salesOrderQuery = requestSet.AppendSalesOrderQueryRq();
            salesOrderQuery.IncludeLineItems.SetValue(true);

            // Do the request and get the response message set object
            IMsgSetResponse responseSet = m_sessionManager.DoRequests(requestSet);
            //string responseXML = responseSet.ToXMLString();
            IResponse response = responseSet.ResponseList.GetAt(0);
            ISalesOrderRetList salesOrderRetList = response.Detail as ISalesOrderRetList;
            if (salesOrderRetList != null)
            {
                for (int i = 0; i < salesOrderRetList.Count; i++)
                {
                    ISalesOrderRet salesOrderRet = salesOrderRetList.GetAt(i);
                    if (salesOrderRet != null)
                    {
                        // Iterate line items
                        IORSalesOrderLineRetList lineRetList = salesOrderRet.ORSalesOrderLineRetList;
                        if (lineRetList != null)
                        {
                            for (int j = 0; j < lineRetList.Count; j++)
                            {
                                try
                                {
                                    // Fill out and add a new SalesOrderDisplayItem for each line item of this sales order
                                    SalesOrderDisplayItem displayItem = new SalesOrderDisplayItem()
                                    {
                                        // Set SalesOrderDisplayItem fields from sales order fields
                                        customerGuid = salesOrderRet.CustomerRef.ListID.GetValue(),
                                        txnId = salesOrderRet.TxnID.GetValue(),
                                        salesOrderNumber = salesOrderRet.RefNumber.GetValue()
                                    };

                                    // Set SalesOrderDisplayItem fields from line item fields
                                    IORSalesOrderLineRet ORLineRet = lineRetList.GetAt(j);
                                    if (ORLineRet != null)
                                    {
                                        ISalesOrderLineRet lineRet = ORLineRet.SalesOrderLineRet;
                                        if (lineRet != null && lineRet.Quantity != null)    // known sales order with Item of "Reimb Subt" has no quantity
                                        {
                                            displayItem.txnLineId = lineRet.TxnLineID.GetValue();
                                            displayItem.itemName = lineRet.ItemRef.FullName.GetValue();
                                            displayItem.numOrdered = (int)lineRet.Quantity.GetValue();
                                            displayItem.numInvoiced = (int)lineRet.Invoiced.GetValue();

                                            salesOrderDisplayItems.Add(displayItem);
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    // If sales order has null fields (such as lineRet.Quantity), just don't use it
                                    // and continue
                                }
                            }
                        }
                    }
                }
            }
        }

        public void FillItems(out List<string> items, out List<string> itemValues)
        {
            items = new List<string>();
            itemValues = new List<string>();

            IMsgSetRequest requestSet = QBUtils.GetLatestMsgSetRequest(m_sessionManager);
            requestSet.Attributes.OnError = ENRqOnError.roeStop;
            requestSet.AppendItemQueryRq();

            IMsgSetResponse responseSet = m_sessionManager.DoRequests(requestSet);

            IResponse response = responseSet.ResponseList.GetAt(0);
            IORItemRetList orItemRetList = response.Detail as IORItemRetList;
            if ( (orItemRetList == null) || orItemRetList.Count == 0 )
            {
                throw new Exception("Warning:  No items are defined in your QuickBooks file.");
            }

            for (int i = 0; i < orItemRetList.Count; i++)
            {
                IORItemRet orItemRet = orItemRetList.GetAt(i);
                switch ( orItemRet.ortype ) // an enum of the elements that can be contained in the OR object
                {
                    case ENORItemRet.orirItemServiceRet:
                    {
                        // orir prefix comes from OR + Item + Ret
                        IItemServiceRet ItemServiceRet = orItemRet.ItemServiceRet;
                        items.Add(ItemServiceRet.Name.GetValue());
                        itemValues.Add(ItemServiceRet.ListID.GetValue());

                        break;
                    }

                    case ENORItemRet.orirItemInventoryRet:
                    {
                        IItemInventoryRet ItemInventoryRet = orItemRet.ItemInventoryRet;
                        items.Add(ItemInventoryRet.Name.GetValue());
                        itemValues.Add(ItemInventoryRet.ListID.GetValue());

                        break;
                    }

                    case ENORItemRet.orirItemNonInventoryRet:
                    {
                        IItemNonInventoryRet ItemNonInventoryRet = orItemRet.ItemNonInventoryRet;
                        items.Add(ItemNonInventoryRet.Name.GetValue());
                        itemValues.Add(ItemNonInventoryRet.ListID.GetValue());

                        break;
                    }
                }
            }
        }

#if false
        public void ShowTemplates()
        {
            IMsgSetRequest requestSet = QBUtils.GetLatestMsgSetRequest(m_sessionManager);
            requestSet.Attributes.OnError = ENRqOnError.roeStop;
            requestSet.AppendTemplateQueryRq();

            IMsgSetResponse responseSet = m_sessionManager.DoRequests(requestSet);

            IResponse response = responseSet.ResponseList.GetAt(0);
            ITemplateRetList retList = response.Detail as ITemplateRetList;
            if ( (retList == null) || retList.Count == 0 )
            {
                throw new Exception("Warning:  No templates are defined in your QuickBooks file.");
            }

            for (int i = 0; i < retList.Count; i++)
            {
                ITemplateRet templateRet = retList.GetAt(i);
                string name = templateRet.Name.GetValue();
                MessageBox.Show(name);
            }
        }
#endif

        public void FillVendors(out List<string> vendors, out List<string> vendValues)
        {
            vendors = new List<string>();
            vendValues = new List<string>();

            IMsgSetRequest requestSet = QBUtils.GetLatestMsgSetRequest(m_sessionManager);
            requestSet.Attributes.OnError = ENRqOnError.roeStop;
            IVendorQuery VendorQ = requestSet.AppendVendorQueryRq();
            // VendorQ.ORVendorListQuery.VendorListFilter.MaxReturned.SetValue(50);

            // Do the request and get the response message set object
            IMsgSetResponse responseSet = m_sessionManager.DoRequests(requestSet);
            IResponse response = responseSet.ResponseList.GetAt(0);
            IVendorRetList vendorRetList = response.Detail as IVendorRetList;

            // OK to not have any vendors
            if (vendorRetList == null)
                return;

            for (int i = 0; i < vendorRetList.Count; i++)
            {
                IVendorRet vendorRet = vendorRetList.GetAt(i);
                vendors.Add(vendorRet.Name.GetValue());
                vendValues.Add(vendorRet.ListID.GetValue());
            }
        }

        public bool GetItemRate(string itemQb, out double purchaseCost, out double salesPrice)
        {
            purchaseCost = salesPrice = 0.0;

            // QB returns bogus data if the guid is blank!
            if (string.IsNullOrEmpty(itemQb))
                return false;

            // Get the RequestMsgSet based on the correct QB Version
            IMsgSetRequest requestSet = QBUtils.GetLatestMsgSetRequest(m_sessionManager);
            requestSet.Attributes.OnError = ENRqOnError.roeStop;
            IItemQuery itemQuery = requestSet.AppendItemQueryRq();

            // Retrieve only the desired Item
            itemQuery.ORListQuery.ListIDList.Add(itemQb);

            // Do the request and get the response message set object
            IMsgSetResponse responseSet = m_sessionManager.DoRequests(requestSet);
            IResponse response = responseSet.ResponseList.GetAt(0);
            IORItemRetList itemRetList = response.Detail as IORItemRetList;
            if (itemRetList != null)
            {
                for (int i = 0; i < itemRetList.Count; i++)
                {
                    IORItemRet itemRet = itemRetList.GetAt(i);
                    if (itemRet != null)
                    {
                        IItemServiceRet itemServiceRet = itemRet.ItemServiceRet;
                        if (itemServiceRet != null)
                        {
                            IORSalesPurchase salesPurchase = itemServiceRet.ORSalesPurchase;
                            if (salesPurchase != null)
                            {
                                if (salesPurchase.SalesAndPurchase != null)
                                {
                                    purchaseCost = salesPurchase.SalesAndPurchase.PurchaseCost.GetValue();
                                    salesPrice = salesPurchase.SalesAndPurchase.SalesPrice.GetValue();
                                    return true;
                                }

                                if (salesPurchase.SalesOrPurchase != null)
                                {
                                    purchaseCost = salesPrice = salesPurchase.SalesOrPurchase.ORPrice.Price.GetValue();
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            // did not find it
            return false;
        }

    
        QBSessionManager m_sessionManager;
    }
}
