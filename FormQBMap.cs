﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data;


namespace AdminTimecard
{
    public partial class FormQBMap : Form
    {
        public FormQBMap()
        {
            InitializeComponent();
            Visible = false;

            tvQBCustSalesOrders.Sorted = true;
        }

        public void setTimecards(List<winployDataSet.ApprovedTimesheetsRow> timecards)
        {
            // Init this dialog for popup of temporarily assigning QB items
            m_timecardMode = true;

            m_timecards = timecards;

            rlListTitle.Text = "Timescards for Invoice";
            cbShowActiveOnly.Visible = false;
            cbShowOnlyMislinkedJobs.Visible = false;
            rbOK.Visible = rbCancel.Visible = true;
        }

        private void FormQBMap_VisibleChanged(object sender, EventArgs e)
        {
            // this is called several times even when Visible hasn't changed
            if (Visible == m_lastVisible)
                return;

            m_lastVisible = Visible;

            if (!Visible)
                return;

            jobsAllTableAdapter.ConnectionString = Form1.ConnectionString;

            FillQBControls();
            FillList();
        }

        private void cbShowActiveOnly_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            FillList();
        }

        private void cbShowOnlyMislinkedJobs_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            FillList();
        }

        private void lbJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Abort if data is not yet bound
            if (m_isFilling)
                return;

            int i = lbJobs.SelectedIndex;
            if (i < 0)
                return;

            string contractorQB;
            string vendorQB;
            string vendorName;
            string customerQB;
            string salesOrderQB;
            string salesOrderItemQB;
            bool vendorDefined;

            if ( lbJobs.Items[i].Tag is winployDataSet.JobsAllRow )
            {
                winployDataSet.JobsAllRow jobRow = (winployDataSet.JobsAllRow)lbJobs.Items[i].Tag;

                // Telerik WinForms Q3 2009:  get called when Jobs item is clicked on RadPanelBar
                // i == 0, but jobRow is NULL!
                if (jobRow == null)
                    return;

                contractorQB = jobRow.IsContractorQBNull() ? string.Empty : jobRow.ContractorQB;
                vendorQB = jobRow.IsVendorQBNull() ? string.Empty : jobRow.VendorQB;
                vendorName = jobRow.IsVendorNameNull() ? string.Empty : jobRow.VendorName;
                customerQB = jobRow.IsCustomerQBNull() ? string.Empty : jobRow.CustomerQB;
                salesOrderQB = jobRow.IsSalesOrderQBNull() ? string.Empty : jobRow.SalesOrderQB;
                salesOrderItemQB = jobRow.IsSalesOrderItemQBNull() ? string.Empty : jobRow.SalesOrderItemQB;
                vendorDefined = !string.IsNullOrEmpty(jobRow["VendorName"].ToString());
            }
            else        // ApprovedTimesheetRow
            {
                winployDataSet.ApprovedTimesheetsRow timesheet = (winployDataSet.ApprovedTimesheetsRow)lbJobs.Items[i].Tag;

                // Telerik WinForms Q3 2009:  get called when Jobs item is clicked on RadPanelBar
                // i == 0, but jobRow is NULL!
                if (timesheet == null)
                    return;

                contractorQB = timesheet.IsContractorQBNull() ? string.Empty : timesheet.ContractorQB;
                vendorQB = timesheet.IsVendorQBNull() ? string.Empty : timesheet.VendorQB;
                vendorName = timesheet.IsVendorNameNull() ? string.Empty : timesheet.VendorName;
                customerQB = timesheet.IsCustomerQBNull() ? string.Empty : timesheet.CustomerQB;
                salesOrderQB = timesheet.IsSalesOrderQBNull() ? string.Empty : timesheet.SalesOrderQB;
                salesOrderItemQB = timesheet.IsSalesOrderItemQBNull() ? string.Empty : timesheet.SalesOrderItemQB;
                vendorDefined = !string.IsNullOrEmpty(timesheet["VendorName"].ToString());
            }

            // Select item in QB lists
            SelectQBItemByGuid(lbQBItems, contractorQB);
            SelectQBItemByGuid(lbQBVendors, vendorQB);

            // QB Vendor is only relevant when Job has a vendor
            lbQBVendors.Enabled = !string.IsNullOrEmpty(vendorName);

            // Select Customer/Sales Order in Tree
            bool isSalesOrder = !string.IsNullOrEmpty(salesOrderQB) && !string.IsNullOrEmpty(salesOrderItemQB);
            bool salesOrderSelected = SelectQBCustSalesOrderByGuid(customerQB, salesOrderQB, salesOrderItemQB) && isSalesOrder;

            // QB Item is relevant when a Sales Order isn't selected and or when a vendor is defined
            lbQBItems.Enabled = !salesOrderSelected || vendorDefined;

            UpdateLinkComparison();
        }


        private void lbQBItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateQBGuidInJob(lbQBItems, "ContractorQB");
        }

        private void lbQBVendors_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateQBGuidInJob(lbQBVendors, "VendorQB");
        }

        private void tvQBCustSalesOrders_Selected(object sender, EventArgs e)
        {
            // Replace deprecated AfterSelect with Selected
            int i = lbJobs.SelectedIndex;
            if (m_isFilling || i < 0)
                return;

            string customerQB, salesOrderQB, salesOrderItemQB;
            bool vendorDefined;

            RadTreeNode node = tvQBCustSalesOrders.SelectedNode;
            bool isSalesOrder = (node.Level > 0);
            if (isSalesOrder)
            {
                SalesOrderDisplayItem displayItem = (SalesOrderDisplayItem)node.Tag;
                customerQB = node.Parent.Tag.ToString();  // parent node is the Customer
                salesOrderQB = displayItem.txnId;
                salesOrderItemQB = displayItem.txnLineId;
            }
            else                       // Customer or None was clicked
            {
                customerQB = (node.Tag != null) ? node.Tag.ToString() : string.Empty;
                salesOrderQB = string.Empty;
                salesOrderItemQB = string.Empty;
            }

            if ( lbJobs.Items[i].Tag is winployDataSet.JobsAllRow )
            {
                winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow)lbJobs.Items[i].Tag;
                row.CustomerQB = customerQB;
                row.SalesOrderQB = salesOrderQB;
                row.SalesOrderItemQB = salesOrderItemQB;

                try
                {
                    jobsAllTableAdapter.Update(row);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                vendorDefined = !string.IsNullOrEmpty(row["VendorName"].ToString());
            }
            else    // Timesheet
            {
                winployDataSet.ApprovedTimesheetsRow row = (winployDataSet.ApprovedTimesheetsRow)lbJobs.Items[i].Tag;
                row.CustomerQB = customerQB;
                row.SalesOrderQB = salesOrderQB;
                row.SalesOrderItemQB = salesOrderItemQB;

                vendorDefined = !string.IsNullOrEmpty(row["VendorName"].ToString());
            }


            // !!!!
            //                     // We just assigned current job, may need to hide it
            //                     if (cbHideAssignedJobs.IsChecked)
            //                         ShowHideGridRows();
            // 
            //                     // We just assigned QB id's, may need to hide it
            //                     if (cbHideAssignedQB.IsChecked)
            //                         FillQBControls();

            // QB Item is relevant only when a Sales Order isn't selected, or there is a vendor
            lbQBItems.Enabled = !isSalesOrder || vendorDefined;

            UpdateLinkComparison();
        }



        private void UpdateQBGuidInJob(RadListBox lb, string colName)
        {
            // Save newly selected guid in job
            int lbIndex = lb.SelectedIndex;
            int i = lbJobs.SelectedIndex;
            if (m_isFilling || (lbIndex < 0) || (i < 0))
                return;

            if ( lbJobs.Items[i].Tag is winployDataSet.JobsAllRow )
            {
                winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow)lbJobs.Items[i].Tag;
                row[colName] = (lb.Items[lbIndex].Tag != null) ? lb.Items[lbIndex].Tag.ToString() : "";
                try
                {
                    jobsAllTableAdapter.Update(row);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            else
            {
                winployDataSet.ApprovedTimesheetsRow row = (winployDataSet.ApprovedTimesheetsRow)lbJobs.Items[i].Tag;
                row[colName] = (lb.Items[lbIndex].Tag != null) ? lb.Items[lbIndex].Tag.ToString() : "";
            }

            // !!!!
            //                     // We just assigned current job, may need to hide it
            //                     if (cbHideAssignedJobs.IsChecked)
            //                         ShowHideGridRows();
            // 
            //                     // We just assigned QB id's, may need to hide it
            //                     if (cbHideAssignedQB.IsChecked)
            //                         FillQBControls();
            UpdateLinkComparison();
        }

        private void FillList()
        {
            using (WaitCursor wc = new WaitCursor())
            {
                m_isFilling = true;
                try
                {
                    if (m_timecardMode)
                        FillListTimesheets();
                    else
                        FillListJobs();
                }
                finally
                {
                    m_isFilling = false;
                }
            }

            // Select first visible item
            bool itemVisible = false;
            bool changedSelection = false;
            for (int i = 0; i < lbJobs.Items.Count; i++)
            {
                if (lbJobs.Items[i].Visibility == ElementVisibility.Visible)
                {
                    itemVisible = true;

                    if (lbJobs.SelectedIndex != i)
                    {
                        lbJobs.SelectedIndex = i;   // causes lbJobs_SelectedIndexChanged() to be called
                        changedSelection = true;
                    }

                    break;
                }
            }

            if (!itemVisible && (lbJobs.SelectedIndex >= 0))    // no items are visible; clear selection
            {
                lbJobs.SelectedIndex = -1;
                changedSelection = true;
            }

            if (!changedSelection)  // lbJobs_SelectedIndexChanged() is not yet called
                lbJobs_SelectedIndexChanged(null, null);
        }

        private void FillListJobs()
        {
            lbJobs.DataSource = winployDataSet.JobsAll.DefaultView;
            winployDataSet.JobsAll.DefaultView.Sort = "CustomerCompany, ContractorFirstName, ContractorLastName";

            if (cbShowActiveOnly.Checked)
                jobsAllTableAdapter.FillActiveJobs(winployDataSet.JobsAll, DateTime.Now.ToString("d"));
            else
                jobsAllTableAdapter.Fill(winployDataSet.JobsAll);
        }

        private void FillListTimesheets()
        {
            lbJobs.DataSource = m_timecards;
        }

        private void lbJobs_ItemDataBound(object sender, ItemDataBoundEventArgs e)
        {
            // hack
            if (!Visible)
                return;

            // Format listbox items
            if (m_timecardMode)
                BindTimesheetItem(e);
            else
                BindJobItem(e);
        }

        private void BindJobItem(ItemDataBoundEventArgs e)
        {
            winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow)((DataRowView)e.DataItem).Row;
            if (e.DataBoundItem != null && row != null)
            {
                // First line is the current sort order
                // Second line is remaining fields
                string contractor = string.Format("{0} {1}", row["ContractorFirstName"], row["ContractorLastName"]);
                string company = row["CustomerCompany"].ToString();
                string vendor = row["VendorName"].ToString();

                string text = string.Format("<html><b>{0}</b>", company);
                string descriptionText = string.Format("{0}", contractor);
                if (!string.IsNullOrEmpty(vendor))
                    descriptionText += string.Format(", ({0})", vendor);

                RadListBoxItem item = (RadListBoxItem)e.DataBoundItem;
                item.Text = text;
                item.DescriptionText = descriptionText;

                // Put the data row corresponding to this item in the tag
                item.Tag = row;

                // Hide jobs with no link warnings
                if (cbShowOnlyMislinkedJobs.Checked && !IsJobMislinked(row))
                    item.Visibility = ElementVisibility.Collapsed;
            }
        }

        private void BindTimesheetItem(ItemDataBoundEventArgs e)
        {
            // Assume there are no null or empty strings since timesheets should be filled in
            winployDataSet.ApprovedTimesheetsRow approvedTimesheet = (winployDataSet.ApprovedTimesheetsRow)e.DataItem;
            RadListBoxItem item = (RadListBoxItem)e.DataBoundItem;
            item.Text = string.Format("{0} - {1} {2} ({3} hours)",
                                        approvedTimesheet.WeekEndingDate.ToShortDateString(),
                                        approvedTimesheet.ContractorFirstName,
                                        approvedTimesheet.ContractorLastName,
                                        approvedTimesheet.HoursTotal);
            item.DescriptionText = approvedTimesheet.CustomerCompany;
            item.Tag = approvedTimesheet;
        }

        private void FillQBControls()
        {
            using (WaitCursor wc = new WaitCursor())
            {
                m_isFilling = true;

                // Turn off error label
                ShowQBError(false);

                InitQBCustTree();
                InitQBListbox(lbQBItems);
                InitQBListbox(lbQBVendors);

                try
                {
                    using (var qbLoader = new QBLoader())
                    {
                        FillCustomers(qbLoader);
                        FillItems(qbLoader);
                        FillVendors(qbLoader);

                        CacheSalesOrders(qbLoader);
                    }
                }
                catch (Exception e)
                {
                    // Show error label with error message
                    ShowQBError(e.Message);

                    Program.ShowExceptionMessage(e);

                    // Clear any garbage from partially filled QB listboxes before error
                    // occurred
                    InitQBCustTree();
                    InitQBListbox(lbQBItems);
                    InitQBListbox(lbQBVendors);
                }
                finally
                {
                    m_isFilling = false;
                }
            }
        }


        private void CacheSalesOrders(QBLoader qbLoader)
        {
            qbLoader.FillSalesOrders(out salesOrderDisplayItems);
            FillQBCustTreeSalesOrders();
        }

        private void FillCustomers(QBLoader qbLoader)
        {
            List<string> customers, custGuids;
            qbLoader.FillCustomers(out customers, out custGuids);
            FillQBCustTree(customers, custGuids);
        }

        private void FillItems(QBLoader qbLoader)
        {
            List<string> items, itemGuids;
            qbLoader.FillItems(out items, out itemGuids);
            FillQBListbox(lbQBItems, "ContractorQB", items, itemGuids);
        }

        private void FillVendors(QBLoader qbLoader)
        {
            List<string> vendors, vendGuids;
            qbLoader.FillVendors(out vendors, out vendGuids);
            FillQBListbox(lbQBVendors, "VendorQB", vendors, vendGuids);
        }


        private void InitQBListbox(RadListBox lb)
        {
            lb.Items.Clear();

            // First add (none)
            lb.Items.Add(new RadListBoxItem { Text = "(None or Other)", Tag = null });
        }

        private void InitQBCustTree()
        {
            tvQBCustSalesOrders.Nodes.Clear();

            // First add (none)
            tvQBCustSalesOrders.Nodes.Add(new RadTreeNode{ Text = "(None or Other)", Tag = null });
        }

        private void FillQBListbox(RadListBox lb, string gridColName, List<string> items, List<string> guids)
        {
            InitQBListbox(lb);

            for (int i = 0; i < items.Count; i++)
            {
                // Add this item if user does not want to hide items, or (user does want to hide assigned items)
                // but the item is not assigned
                //if ( !cbHideAssignedQB.IsChecked || !GuidInGrid(gridColName, guids[i]) )
                    lb.Items.Add(new RadListBoxItem {Text = items[i], Tag = guids[i]});
            }
        }


        private void FillQBCustTree(List<string> customers, List<string> guids)
        {
            InitQBCustTree();

            tvQBCustSalesOrders.BeginUpdate();

            for (int i = 0; i < customers.Count; i++)
            {
                // Add this item if user does not want to hide items, or (user does want to hide assigned items)
                // but the item is not assigned
                //if ( !cbHideAssignedQB.IsChecked || !GuidInGrid(gridColName, guids[i]) )

                // Node of customer has Name field set to customer QB guid so that
                // RadTreeView.Find() can be used to find the sales order line
                tvQBCustSalesOrders.Nodes.Add(new RadTreeNode { Text = customers[i], Tag = guids[i], Name = guids[i] });
            }

            tvQBCustSalesOrders.EndUpdate();
        }

        private void FillQBCustTreeSalesOrders()
        {
            // Add sales orders for specified customer
            if (salesOrderDisplayItems != null)
            {
                tvQBCustSalesOrders.BeginUpdate();
                foreach (SalesOrderDisplayItem item in salesOrderDisplayItems)
                {
                    // RadTreeView.operator[] searches RadTreeNode.Text instead of RadTreeNode.Name, how stupid!
                    // RadTreeNode custNode = tvQBCustSalesOrders[item.customerGuid];

                    foreach (RadTreeNode node in tvQBCustSalesOrders.Nodes)
                    {
                        if ( (node.Tag != null) && (node.Tag.ToString() == item.customerGuid) )
                        {
                            // Node of sales order has Name field set to sales order line QB guid so that
                            // RadTreeView.Find() can be used to find the sales order line
                            string description = GetSalesOrderDescription(item);
                            node.Nodes.Add(new RadTreeNode { Text = description, Tag = item, Name = item.txnLineId });

                            break;
                        }
                    }
                }

                tvQBCustSalesOrders.EndUpdate();
            }
        }

        private void rlRetryQB_Click(object sender, EventArgs e)
        {
            FillQBControls();
        }

        private void ShowQBError(bool visible)
        {
            rlQBError.Visible = visible;
            rlRetryQB.Visible = visible;
        }

        private void ShowQBError(string QBErrorMsg)
        {
            rlQBError.Text = QBErrorMsg;
            rlQBError.Visible = true;
            rlRetryQB.Visible = true;
        }

        private void SelectQBItemByGuid(RadListBox lb, string guid)
        {
            // abort if not yet init
            if (lb.Items.Count == 0)
                return;

            // Set to "(None)" in case guid is not found in list.
            // Set Program.isFilling to true to prevent the dataset from being updated
            // since guid is not changing.
            m_isFilling = true;
            lb.SelectedIndex = 0;       // "(None")

            // Abort if no guid
            if (!string.IsNullOrEmpty(guid))
            {
                // Search list items for a tag of guid
                foreach (RadListBoxItem item in lb.Items)
                {
                    if ((item.Tag != null) && (item.Tag.ToString() == guid))
                    {
                        lb.SelectedItem = item;
                        break;
                    }
                }
            }

            m_isFilling = false;
        }

        private string GetCustomerFromTree(string customerGuid)
        {
            string customer = null;
            RadTreeNode[] nodes = tvQBCustSalesOrders.Nodes.Find(customerGuid, false);
            Debug.Assert(nodes.Length == 0 || nodes.Length == 1);
            if (nodes.Length > 0)
                customer = nodes[0].Text;

            return customer;
        }

        private string GetSelectedCustomer()
        {
            // The current customer in the tree is the selected node (if it is in the first level), or the parent of
            // the selected node (if it is in the first level, a sales order)
            string customer = string.Empty;
            RadTreeNode selectedNode = tvQBCustSalesOrders.SelectedNode;
            if ( selectedNode != null )
            {
                customer = (tvQBCustSalesOrders.SelectedNode.Level <= 0) ?
                                tvQBCustSalesOrders.SelectedNode.Text :
                                tvQBCustSalesOrders.SelectedNode.Parent.Text;
            }

            return customer;
        }

        private bool SelectQBCustSalesOrderByGuid(string customerGuid, string salesOrderGuid, string salesOrderItemGuid)
        {
            // Set Program.isFilling to true to prevent the dataset from being updated
            // since guid is not changing.
            m_isFilling = true;

            // Set to "(None)" in case guid is not found in list.
            tvQBCustSalesOrders.SelectedNode = tvQBCustSalesOrders.Nodes[0];

            // Hide all sales orders
            tvQBCustSalesOrders.CollapseLevel(0);

            bool selected = false;
            if (!string.IsNullOrEmpty(customerGuid))
            {
                // Search tree items either a customer guid or a sales order item guid
                bool isSalesOrder = !string.IsNullOrEmpty(salesOrderGuid) && !string.IsNullOrEmpty(salesOrderItemGuid);
                RadTreeNode[] nodes = (isSalesOrder) ? tvQBCustSalesOrders.Nodes.Find(salesOrderItemGuid, true) :
                                                       tvQBCustSalesOrders.Nodes.Find(customerGuid, false);
                if (nodes.Length > 0)
                {
                    selected = true;

                    tvQBCustSalesOrders.SelectedNode = nodes[0];

                    // Ensure selected sales order is visible by expanding its parent node
                    if ( isSalesOrder )
                        tvQBCustSalesOrders.SelectedNode.Parent.Expand();
                }
            }

            m_isFilling = false;

            return selected;
        }

        private static string GetQBItemByGuid(RadListBox lb, string guid)
        {
            if (string.IsNullOrEmpty(guid))
                return null;

            // Search list items for a tag of guid
            foreach (RadItem item in lb.Items)
            {
                if ((item.Tag != null) && (item.Tag.ToString() == guid))
                    return item.Text;
            }

            return null;
        }

        private string GetSalesOrderDescription(string salesOrderItemQB)
        {
            // Search cached sales order items looking for specified ItemQB
            if (string.IsNullOrEmpty(salesOrderItemQB) || salesOrderDisplayItems == null)
                return null;

            // Search list items for a tag of guid
            foreach (SalesOrderDisplayItem item in salesOrderDisplayItems)
            {
                if (item.txnLineId == salesOrderItemQB)
                    return GetSalesOrderDescription(item);
            }

            return null;
        }


        private static string GetSalesOrderDescription(SalesOrderDisplayItem item)
        {
            return string.Format(@"[{0}] {1}, {2} of {3} units left", item.salesOrderNumber, item.itemName, (item.numOrdered - item.numInvoiced), item.numOrdered);
        }

        private static bool CheckPart(ref string jobPart, ref string qbPart, LinkCheck linkCheckType)
        {
            const int numFirstCharsToCompare = 3;

            if (string.IsNullOrEmpty(jobPart) || string.IsNullOrEmpty(qbPart))
            {
                if (string.IsNullOrEmpty(jobPart))
                    jobPart = "(None)";
                if (string.IsNullOrEmpty(qbPart))
                    qbPart = "(None or Other)";

                return false;
            }

            // Search does not include whitespace; remove whitespace and make 
            // Search case insensitive
            string jobPartNormalized = jobPart.Replace(" ", "").ToLower();
            string qbPartNormalized = qbPart.Replace(" ", "").ToLower();

            bool match = true;
            switch (linkCheckType)
            {
                case LinkCheck.JobIsQB:
                    // Compare first few characters at start of job and qb part
                    match = (string.Compare(jobPartNormalized, 0, qbPartNormalized, 0, numFirstCharsToCompare) == 0);
                    break;

                case LinkCheck.JobInQB:
                    // Search qb part for job part
                    match = (qbPartNormalized.IndexOf(jobPartNormalized) >= 0);
                    break;

                default:
                    Debug.Assert(false);
                    break;
            }

            return match;
        }

        private bool ShowResults(string title, string jobPart, string qbPart, int i)
        {
            // Select job causing the error
            if (i >= 0)
                lbJobs.SelectedIndex = i;

            // Show error
            string results = GetResultString(jobPart, qbPart, false);
            using (FormLinkWarn linkWarnForm = new FormLinkWarn(title, results))
            {
                return linkWarnForm.ShowDialog(Program.mainForm) == DialogResult.OK;
            }
        }


        private static string GetResultString(string jobPart, string qbPart, bool match)
        {
            string htmlColor = (match) ? "<color=0,0,0>" : "<color=192,0,0>";
            return string.Format("<html>{0}{1} <color=128,128,128>/<color=0,0,0>{0}{2}</html>", htmlColor, jobPart, qbPart);
        }

        private void UpdateLinkComparison()
        {
            // Initially blank out all link comparisons
            rlCompareCustomer.Text = string.Empty;
            rlCompareSalesOrder.Text = string.Empty;
            rlCompareItem.Text = string.Empty;
            rlCompareVendor.Text = string.Empty;

            if (lbJobs.SelectedIndex >= 0)
            {
                DataRow row = (DataRow)lbJobs.Items[lbJobs.SelectedIndex].Tag;

                // Check Company
                string jobPart = row["CustomerCompany"].ToString();
                string qbPart = GetSelectedCustomer();
                bool match = CheckPart(ref jobPart, ref qbPart, LinkCheck.JobIsQB);
                rlCompareCustomer.Text = GetResultString(jobPart, qbPart, match);

                bool vendorDefined = !string.IsNullOrEmpty(row["VendorName"].ToString());
                bool salesOrderDefined = !string.IsNullOrEmpty(row["SalesOrderQB"].ToString()) && !string.IsNullOrEmpty(row["SalesOrderItemQB"].ToString());

                // Check Sales order, if defined
                if (salesOrderDefined)
                {
                    jobPart = row["ContractorFirstName"] + " " + row["ContractorLastName"];
                    qbPart = GetSalesOrderDescription(row["SalesOrderItemQB"].ToString());
                    match = CheckPart(ref jobPart, ref qbPart, LinkCheck.JobInQB);
                    rlCompareSalesOrder.Text = GetResultString(jobPart, qbPart, match);
                }

                // Check Item if no sales order or a vendor (billing vendor requires Item)
                if (!salesOrderDefined || vendorDefined)
                {
                    jobPart = row["ContractorFirstName"] + " " + row["ContractorLastName"];
                    qbPart = GetQBItemByGuid(lbQBItems, row["ContractorQB"].ToString());
                    match = CheckPart(ref jobPart, ref qbPart, LinkCheck.JobIsQB);
                    rlCompareItem.Text = GetResultString(jobPart, qbPart, match);
                }

                // Check Vendor, if defined
                if (vendorDefined)
                {
                    jobPart = row["VendorName"].ToString();
                    qbPart = GetQBItemByGuid(lbQBVendors, row["VendorQB"].ToString());
                    match = CheckPart(ref jobPart, ref qbPart, LinkCheck.JobIsQB);
                    rlCompareVendor.Text = GetResultString(jobPart, qbPart, match);
                }
            }
        }

        private bool IsJobMislinked(winployDataSet.JobsAllRow row)
        {
            // Return true if the specified job has suspected problems with QB linking; false if not

            // Check Company
            string jobPart = row["CustomerCompany"].ToString();
            string qbPart = GetCustomerFromTree(row["CustomerQB"].ToString());
            bool match = CheckPart(ref jobPart, ref qbPart, LinkCheck.JobIsQB);
            if (!match)
                return true;

            // Check Sales order, if defined
            if (!string.IsNullOrEmpty(row["SalesOrderQB"].ToString()) && !string.IsNullOrEmpty(row["SalesOrderItemQB"].ToString()))
            {
                jobPart = row["ContractorFirstName"] + " " + row["ContractorLastName"];
                qbPart = GetSalesOrderDescription(row.SalesOrderItemQB);
                match = CheckPart(ref jobPart, ref qbPart, LinkCheck.JobInQB);
                if (!match)
                    return true;
            }
            else
            {
                // Check Item
                jobPart = row["ContractorFirstName"] + " " + row["ContractorLastName"];
                qbPart = GetQBItemByGuid(lbQBItems, row["ContractorQB"].ToString());
                match = CheckPart(ref jobPart, ref qbPart, LinkCheck.JobIsQB);
                if (!match)
                    return true;
            }

            // Check Vendor, if defined
            if (!row.IsVendorNameNull())
            {
                jobPart = row.VendorName;
                qbPart = GetQBItemByGuid(lbQBVendors, row["VendorQB"].ToString());
                match = CheckPart(ref jobPart, ref qbPart, LinkCheck.JobIsQB);
                if (!match)
                    return true;
            }

            // All passed, no warnings detected
            return false;
        }


        private bool m_timecardMode;
        private bool m_lastVisible;
        private bool m_isFilling;

        private List<SalesOrderDisplayItem> salesOrderDisplayItems;
        private List<winployDataSet.ApprovedTimesheetsRow> m_timecards;

        enum LinkCheck
        {
            JobInQB,    // Check that Job part is contained within QB part
            JobIsQB     // Check that Job part is same as QB part
        }

    }

    class SalesOrderDisplayItem
    {
        // From Sales order
        public string customerGuid;     // Customer of the sales order
        public string txnId;            // ID of sales order
        public string salesOrderNumber; // e.g. 1, 2, ...

        // From Sales order Line Item
        public string txnLineId;       // ID of sales order line item
        public string itemName;        // e.g. "David Ching"
        public int numOrdered;
        public int numInvoiced;
    };
}
