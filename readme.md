# ADMIN TIMECARD

A Winforms over data application, which I wrote for Embedded Resource Group starting around 2008.  It uses ADO.NET datasets and adapters instead of Linq/Entity Framework.  It also uses the Telerik WinForms controls:  

Unfortunately, the application is not hosted live.  Some screenshots follow.

![alt text](Images/admintimecard1.png "")
![alt text](Images/admintimecard2.png "")
![alt text](Images/admintimecard3.jpg "")
![alt text](Images/admintimecard4.jpg "")