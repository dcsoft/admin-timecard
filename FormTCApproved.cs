﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using System.Data;


namespace AdminTimecard
{
    public partial class FormTCApproved : Form
    {
        public FormTCApproved()
        {
            InitializeComponent();
            Visible = false;
        }

        private void FormTCApproved_Load(object sender, EventArgs e)
        {
            // Default Report date to today
            datePicker.Value = DateTime.Now;    
        }

        private void FormTCApproved_VisibleChanged(object sender, EventArgs e)
        {
            // this is called several times even when Visible hasn't changed
            if (Visible == lastVisible)
                return;

            lastVisible = Visible;

            if (!Visible)
                return;

            approvedTimesheetsTableAdapter.ConnectionString = Form1.ConnectionString;

            // Re-run current report (if any) to reflect latest changes
            // (possibly made in other screens before switching back to this one.)
            if (gridFilled)
                FillGrid();
        }

        private void btnCreateReport_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            // Loading the Approved Timesheets data source causes the grid to fill
            // Force loading so any changes made in other pages to the approved 
            // timesheets are shown
            using (WaitCursor wc = new WaitCursor())
            {
                gridFilled = true;
                isFilling = true;

                try
                {
                    approvedTimesheetsTableAdapter.FillBefore(winployDataSet.ApprovedTimesheets, datePicker.Value);
                }
                finally
                {
                    isFilling = false;
                }

                int numApproved = winployDataSet.ApprovedTimesheets.Count;
                if (numApproved == 0)
                    radLabelHeading.Text = string.Format("No uninvoiced timecards as of {0}", datePicker.Value.ToString("d"));
                else if (numApproved == 1)
                    radLabelHeading.Text = string.Format("1 uninvoiced timecard as of {0}", datePicker.Value.ToString("d"));
                else
                    radLabelHeading.Text = string.Format("{0} uninvoiced timecards as of {1}", numApproved, datePicker.Value.ToString("d"));

                // Enable or disable Export button depending if there are any jobs in the report
                btnExport.Enabled = (numApproved >= 0);

                radGridView1.MasterGridViewTemplate.BestFitColumns();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            // Get filename to export to
            if (!FileExport.ShowSaveFileDialog(ref csvFilePath))
                return;

            // Write export file
            string header = @"Ending,Submitted,Approved,Hours,First,Last,Company,Vendor";
            try
            {
                using (FileStream fs = new FileStream(csvFilePath, FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs))
                    {
                        // Write header
                        w.WriteLine(header);

                        // Write each row
                        StringBuilder b = new StringBuilder();
                        foreach (winployDataSet.ApprovedTimesheetsRow row in winployDataSet.ApprovedTimesheets)
                        {
                            b.AppendFormat("{0}", FileExport.QuoteCSVItem(row.WeekEndingDate.ToString("d")));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.SubmitalTimeStamp.ToString("d")));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.ApprovalTimeStamp.ToString("d")));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.HoursTotal.ToString()));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.ContractorFirstName));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.ContractorLastName));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.CustomerCompany));

                            if (!row.IsVendorNameNull())    // only vendor can be NULL, rest of fields above are fine
                                b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.VendorName));

                            b.AppendLine();
                        }
                        w.WriteLine(b);
                    }
                }
            }
            catch (Exception ex)
            {
                // Inform error
                MessageBox.Show(ex.Message, "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Prompt user to open newly exported file
            FileExport.OpenExportedFile(csvFilePath);
        }


        bool lastVisible;
        bool isFilling;
        bool gridFilled;
        string csvFilePath;

    }
}
