﻿using System.Windows.Forms;


namespace AdminTimecard
{
    partial class FormReportHealth
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn4 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.ContractorGrid = new Telerik.WinControls.UI.RadGridView();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.W2Grid = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.W2Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radButton1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ContractorGrid, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.W2Grid, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(809, 558);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(71, 16);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "<html><b>Contractors";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(812, 3);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(1, 14);
            this.radButton1.TabIndex = 1;
            this.radButton1.Text = "radButton1";
            // 
            // ContractorGrid
            // 
            this.ContractorGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContractorGrid.Location = new System.Drawing.Point(3, 27);
            // 
            // 
            // 
            this.ContractorGrid.MasterGridViewTemplate.AllowAddNewRow = false;
            this.ContractorGrid.MasterGridViewTemplate.AllowDeleteRow = false;
            this.ContractorGrid.MasterGridViewTemplate.AllowEditRow = false;
            this.ContractorGrid.MasterGridViewTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.HeaderText = "Num";
            gridViewTextBoxColumn1.UniqueName = "column1";
            gridViewTextBoxColumn2.DisableHTMLRendering = false;
            gridViewTextBoxColumn2.HeaderText = "Contractor";
            gridViewTextBoxColumn2.UniqueName = "column2";
            gridViewTextBoxColumn3.HeaderText = "Started";
            gridViewTextBoxColumn3.UniqueName = "column3";
            gridViewTextBoxColumn4.FieldAlias = "week1";
            gridViewTextBoxColumn4.HeaderText = "Week1";
            gridViewTextBoxColumn4.UniqueName = "week1";
            gridViewTextBoxColumn5.FieldAlias = "week2";
            gridViewTextBoxColumn5.HeaderText = "Week2";
            gridViewTextBoxColumn5.UniqueName = "week2";
            gridViewTextBoxColumn6.FieldAlias = "week3";
            gridViewTextBoxColumn6.HeaderText = "Week3";
            gridViewTextBoxColumn6.UniqueName = "week3";
            gridViewTextBoxColumn7.FieldAlias = "week4";
            gridViewTextBoxColumn7.HeaderText = "Week4";
            gridViewTextBoxColumn7.UniqueName = "week4";
            gridViewTextBoxColumn8.FieldAlias = "week5";
            gridViewTextBoxColumn8.HeaderText = "Week5";
            gridViewTextBoxColumn8.UniqueName = "week5";
            gridViewTextBoxColumn9.HeaderText = "Customer";
            gridViewTextBoxColumn9.UniqueName = "column9";
            gridViewTextBoxColumn10.HeaderText = "Vendor";
            gridViewTextBoxColumn10.UniqueName = "column10";
            gridViewTextBoxColumn11.HeaderText = "SR / RC";
            gridViewTextBoxColumn11.UniqueName = "column11";
            gridViewDecimalColumn1.DataType = typeof(decimal);
            gridViewDecimalColumn1.HeaderText = "Bill Rate";
            gridViewDecimalColumn1.UniqueName = "column12";
            gridViewDecimalColumn2.DataType = typeof(decimal);
            gridViewDecimalColumn2.HeaderText = "Pay Rate";
            gridViewDecimalColumn2.UniqueName = "column13";
            gridViewTextBoxColumn12.DisableHTMLRendering = false;
            gridViewTextBoxColumn12.HeaderText = "Net $";
            gridViewTextBoxColumn12.UniqueName = "column14";
            gridViewTextBoxColumn13.HeaderText = "Net %";
            gridViewTextBoxColumn13.UniqueName = "column15";
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn1);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn2);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn3);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn4);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn5);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn6);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn7);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn8);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn9);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn10);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn11);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewDecimalColumn1);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewDecimalColumn2);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn12);
            this.ContractorGrid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn13);
            this.ContractorGrid.Name = "ContractorGrid";
            this.ContractorGrid.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.ContractorGrid.ReadOnly = true;
            // 
            // 
            // 
            this.ContractorGrid.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.ContractorGrid.Size = new System.Drawing.Size(803, 249);
            this.ContractorGrid.TabIndex = 2;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(3, 282);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(89, 16);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "<html><b>W2 Employees";
            // 
            // W2Grid
            // 
            this.W2Grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.W2Grid.Location = new System.Drawing.Point(3, 306);
            // 
            // 
            // 
            this.W2Grid.MasterGridViewTemplate.AllowAddNewRow = false;
            this.W2Grid.MasterGridViewTemplate.AllowColumnReorder = false;
            gridViewTextBoxColumn14.HeaderText = "Num";
            gridViewTextBoxColumn14.UniqueName = "column1";
            gridViewTextBoxColumn15.DisableHTMLRendering = false;
            gridViewTextBoxColumn15.HeaderText = "Contractor";
            gridViewTextBoxColumn15.UniqueName = "column2";
            gridViewTextBoxColumn16.HeaderText = "Started";
            gridViewTextBoxColumn16.UniqueName = "column3";
            gridViewTextBoxColumn17.FieldAlias = "week1";
            gridViewTextBoxColumn17.HeaderText = "Week1";
            gridViewTextBoxColumn17.UniqueName = "week1";
            gridViewTextBoxColumn18.FieldAlias = "week2";
            gridViewTextBoxColumn18.HeaderText = "Week2";
            gridViewTextBoxColumn18.UniqueName = "week2";
            gridViewTextBoxColumn19.FieldAlias = "week3";
            gridViewTextBoxColumn19.HeaderText = "Week3";
            gridViewTextBoxColumn19.UniqueName = "week3";
            gridViewTextBoxColumn20.FieldAlias = "week4";
            gridViewTextBoxColumn20.HeaderText = "Week4";
            gridViewTextBoxColumn20.UniqueName = "week4";
            gridViewTextBoxColumn21.FieldAlias = "week5";
            gridViewTextBoxColumn21.HeaderText = "Week5";
            gridViewTextBoxColumn21.UniqueName = "week5";
            gridViewTextBoxColumn22.HeaderText = "Customer";
            gridViewTextBoxColumn22.UniqueName = "column9";
            gridViewTextBoxColumn23.HeaderText = "Vendor";
            gridViewTextBoxColumn23.UniqueName = "column10";
            gridViewTextBoxColumn24.HeaderText = "SR / RC";
            gridViewTextBoxColumn24.UniqueName = "column11";
            gridViewDecimalColumn3.DataType = typeof(decimal);
            gridViewDecimalColumn3.HeaderText = "Bill Rate";
            gridViewDecimalColumn3.UniqueName = "column12";
            gridViewDecimalColumn4.DataType = typeof(decimal);
            gridViewDecimalColumn4.HeaderText = "Pay Rate";
            gridViewDecimalColumn4.UniqueName = "column13";
            gridViewTextBoxColumn25.DisableHTMLRendering = false;
            gridViewTextBoxColumn25.HeaderText = "Net $";
            gridViewTextBoxColumn25.UniqueName = "column14";
            gridViewTextBoxColumn26.HeaderText = "Net %";
            gridViewTextBoxColumn26.UniqueName = "column15";
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn14);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn15);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn16);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn17);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn18);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn19);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn20);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn21);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn22);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn23);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn24);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewDecimalColumn3);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewDecimalColumn4);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn25);
            this.W2Grid.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn26);
            this.W2Grid.Name = "W2Grid";
            this.W2Grid.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.W2Grid.ReadOnly = true;
            // 
            // 
            // 
            this.W2Grid.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.W2Grid.Size = new System.Drawing.Size(803, 249);
            this.W2Grid.TabIndex = 4;
            // 
            // FormReportHealth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormReportHealth";
            this.Text = "FormReportHealth";
            this.VisibleChanged += new System.EventHandler(this.FormReportHealth_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.W2Grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGridView ContractorGrid;
        private Telerik.WinControls.UI.RadGridView W2Grid;
        private Telerik.WinControls.UI.RadLabel radLabel2;
    }
}