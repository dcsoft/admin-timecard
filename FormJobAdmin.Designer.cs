﻿using System.Windows.Forms;


namespace AdminTimecard
{
    partial class FormJobAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbJobs = new Telerik.WinControls.UI.RadListBox();
            this.jobsAllBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.winployDataSet = new AdminTimecard.winployDataSet();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.cbWelcomeContractor = new Telerik.WinControls.UI.RadCheckBox();
            this.cbWelcomeManager = new Telerik.WinControls.UI.RadCheckBox();
            this.SendEmailButton = new Telerik.WinControls.UI.RadButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbSendToAdmin = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabelComments = new Telerik.WinControls.UI.RadLabel();
            this.tbComments = new Telerik.WinControls.UI.RadTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.stopDatePicker = new System.Windows.Forms.DateTimePicker();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.jobsAllTableAdapter = new AdminTimecard.winployDataSetTableAdapters.JobsAllTableAdapter();
            this.radioSortContractor = new Telerik.WinControls.UI.RadRadioButton();
            this.radioSortManager = new Telerik.WinControls.UI.RadRadioButton();
            this.radioSortCompany = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.cbActiveOnly = new Telerik.WinControls.UI.RadCheckBox();
            this.labelJobs = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lbJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobsAllBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWelcomeContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWelcomeManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SendEmailButton)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbSendToAdmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbComments)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbActiveOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelJobs)).BeginInit();
            this.SuspendLayout();
            // 
            // lbJobs
            // 
            this.lbJobs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbJobs.DataSource = this.jobsAllBindingSource;
            this.lbJobs.DisplayMember = "JobId";
            this.lbJobs.FormatString = "{0}";
            this.lbJobs.Location = new System.Drawing.Point(15, 31);
            this.lbJobs.Name = "lbJobs";
            this.lbJobs.Size = new System.Drawing.Size(290, 493);
            this.lbJobs.TabIndex = 2;
            this.lbJobs.ThemeName = "ERGTheme";
            this.lbJobs.ValueMember = "JobId";
            this.lbJobs.SelectedIndexChanged += new System.EventHandler(this.lbJobs_SelectedIndexChanged);
            this.lbJobs.ItemDataBound += new Telerik.WinControls.UI.ItemDataBoundEventHandler(this.lbJobs_ItemDataBound);
            // 
            // jobsAllBindingSource
            // 
            this.jobsAllBindingSource.DataMember = "JobsAll";
            this.jobsAllBindingSource.DataSource = this.winployDataSet;
            // 
            // winployDataSet
            // 
            this.winployDataSet.DataSetName = "winployDataSet";
            this.winployDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(6, 20);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(50, 17);
            this.radLabel2.TabIndex = 0;
            this.radLabel2.Text = "E-mails";
            // 
            // cbWelcomeContractor
            // 
            this.cbWelcomeContractor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbWelcomeContractor.Location = new System.Drawing.Point(60, 19);
            this.cbWelcomeContractor.Name = "cbWelcomeContractor";
            // 
            // 
            // 
            this.cbWelcomeContractor.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbWelcomeContractor.Size = new System.Drawing.Size(123, 18);
            this.cbWelcomeContractor.TabIndex = 1;
            this.cbWelcomeContractor.Text = "Welcome Contractor";
            this.cbWelcomeContractor.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbWelcomeContractor_ToggleStateChanged);
            // 
            // cbWelcomeManager
            // 
            this.cbWelcomeManager.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbWelcomeManager.Location = new System.Drawing.Point(201, 19);
            this.cbWelcomeManager.Name = "cbWelcomeManager";
            // 
            // 
            // 
            this.cbWelcomeManager.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbWelcomeManager.Size = new System.Drawing.Size(115, 18);
            this.cbWelcomeManager.TabIndex = 2;
            this.cbWelcomeManager.Text = "Welcome Manager";
            this.cbWelcomeManager.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbWelcomeManager_ToggleStateChanged);
            // 
            // SendEmailButton
            // 
            this.SendEmailButton.Enabled = false;
            this.SendEmailButton.Location = new System.Drawing.Point(379, 17);
            this.SendEmailButton.Name = "SendEmailButton";
            this.SendEmailButton.Size = new System.Drawing.Size(96, 23);
            this.SendEmailButton.TabIndex = 4;
            this.SendEmailButton.Text = "Send Emails";
            this.SendEmailButton.ThemeName = "Office2007Black";
            this.SendEmailButton.Click += new System.EventHandler(this.SendEmailButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbSendToAdmin);
            this.groupBox1.Controls.Add(this.radLabel2);
            this.groupBox1.Controls.Add(this.SendEmailButton);
            this.groupBox1.Controls.Add(this.cbWelcomeContractor);
            this.groupBox1.Controls.Add(this.cbWelcomeManager);
            this.groupBox1.Location = new System.Drawing.Point(316, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(481, 81);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actions";
            // 
            // cbSendToAdmin
            // 
            this.cbSendToAdmin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbSendToAdmin.Location = new System.Drawing.Point(60, 43);
            this.cbSendToAdmin.Name = "cbSendToAdmin";
            // 
            // 
            // 
            this.cbSendToAdmin.RootElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cbSendToAdmin.Size = new System.Drawing.Size(268, 18);
            this.cbSendToAdmin.TabIndex = 3;
            this.cbSendToAdmin.Text = "Send to Admin only (Admin will edit and forward)";
            // 
            // radLabelComments
            // 
            this.radLabelComments.Location = new System.Drawing.Point(316, 159);
            this.radLabelComments.Name = "radLabelComments";
            this.radLabelComments.Size = new System.Drawing.Size(66, 17);
            this.radLabelComments.TabIndex = 5;
            this.radLabelComments.Text = "Comments";
            this.radLabelComments.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.radLabelComments_MouseDoubleClick);
            // 
            // tbComments
            // 
            this.tbComments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbComments.Location = new System.Drawing.Point(316, 182);
            this.tbComments.Multiline = true;
            this.tbComments.Name = "tbComments";
            this.tbComments.ReadOnly = true;
            // 
            // 
            // 
            this.tbComments.RootElement.StretchVertically = true;
            this.tbComments.Size = new System.Drawing.Size(481, 342);
            this.tbComments.TabIndex = 6;
            this.tbComments.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.stopDatePicker);
            this.groupBox2.Controls.Add(this.startDatePicker);
            this.groupBox2.Controls.Add(this.radLabel5);
            this.groupBox2.Controls.Add(this.radLabel4);
            this.groupBox2.Location = new System.Drawing.Point(316, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(481, 51);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Active Dates";
            // 
            // stopDatePicker
            // 
            this.stopDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.stopDatePicker.Location = new System.Drawing.Point(324, 20);
            this.stopDatePicker.MaxDate = new System.DateTime(2049, 12, 31, 0, 0, 0, 0);
            this.stopDatePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.stopDatePicker.Name = "stopDatePicker";
            this.stopDatePicker.ShowCheckBox = true;
            this.stopDatePicker.ShowUpDown = true;
            this.stopDatePicker.Size = new System.Drawing.Size(150, 21);
            this.stopDatePicker.TabIndex = 3;
            this.stopDatePicker.Validated += new System.EventHandler(this.stopDatePicker_Validated);
            // 
            // startDatePicker
            // 
            this.startDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDatePicker.Location = new System.Drawing.Point(74, 20);
            this.startDatePicker.MaxDate = new System.DateTime(2049, 12, 31, 0, 0, 0, 0);
            this.startDatePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.ShowCheckBox = true;
            this.startDatePicker.ShowUpDown = true;
            this.startDatePicker.Size = new System.Drawing.Size(150, 21);
            this.startDatePicker.TabIndex = 1;
            this.startDatePicker.Validated += new System.EventHandler(this.startDatePicker_Validated);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(257, 20);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(63, 17);
            this.radLabel5.TabIndex = 2;
            this.radLabel5.Text = "Stop Date";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(6, 20);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(64, 17);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Start Date";
            // 
            // jobsAllTableAdapter
            // 
            this.jobsAllTableAdapter.ClearBeforeFill = true;
            // 
            // radioSortContractor
            // 
            this.radioSortContractor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioSortContractor.Location = new System.Drawing.Point(70, 530);
            this.radioSortContractor.Name = "radioSortContractor";
            this.radioSortContractor.Size = new System.Drawing.Size(78, 18);
            this.radioSortContractor.TabIndex = 8;
            this.radioSortContractor.Text = "Contractor";
            this.radioSortContractor.ThemeName = "Office2007Black";
            this.radioSortContractor.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radioSortContractor.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radioSortContractor_ToggleStateChanged);
            // 
            // radioSortManager
            // 
            this.radioSortManager.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioSortManager.Location = new System.Drawing.Point(233, 530);
            this.radioSortManager.Name = "radioSortManager";
            this.radioSortManager.Size = new System.Drawing.Size(69, 18);
            this.radioSortManager.TabIndex = 9;
            this.radioSortManager.Text = "Manager";
            this.radioSortManager.ThemeName = "Office2007Black";
            this.radioSortManager.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radioSortManager_ToggleStateChanged);
            // 
            // radioSortCompany
            // 
            this.radioSortCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioSortCompany.Location = new System.Drawing.Point(154, 530);
            this.radioSortCompany.Name = "radioSortCompany";
            this.radioSortCompany.Size = new System.Drawing.Size(73, 18);
            this.radioSortCompany.TabIndex = 9;
            this.radioSortCompany.Text = "Company";
            this.radioSortCompany.ThemeName = "Office2007Black";
            this.radioSortCompany.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radioSortCompany_ToggleStateChanged);
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(12, 530);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(47, 17);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "Sort by";
            // 
            // cbActiveOnly
            // 
            this.cbActiveOnly.AutoSize = false;
            this.cbActiveOnly.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbActiveOnly.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.cbActiveOnly.Location = new System.Drawing.Point(154, 8);
            this.cbActiveOnly.Name = "cbActiveOnly";
            this.cbActiveOnly.Size = new System.Drawing.Size(151, 18);
            this.cbActiveOnly.TabIndex = 1;
            this.cbActiveOnly.Text = "Only show active jobs";
            this.cbActiveOnly.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.cbActiveOnly.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.cbActiveOnly_ToggleStateChanged);
            // 
            // labelJobs
            // 
            this.labelJobs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJobs.Location = new System.Drawing.Point(15, 8);
            this.labelJobs.Name = "labelJobs";
            this.labelJobs.Size = new System.Drawing.Size(34, 17);
            this.labelJobs.TabIndex = 0;
            this.labelJobs.Text = "Jobs";
            // 
            // FormJobAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.cbActiveOnly);
            this.Controls.Add(this.labelJobs);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radioSortCompany);
            this.Controls.Add(this.radioSortManager);
            this.Controls.Add(this.radioSortContractor);
            this.Controls.Add(this.tbComments);
            this.Controls.Add(this.radLabelComments);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbJobs);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormJobAdmin";
            this.Text = "FormJobEmail";
            this.VisibleChanged += new System.EventHandler(this.FormJobAdmin_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.lbJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobsAllBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWelcomeContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWelcomeManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SendEmailButton)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbSendToAdmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbComments)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioSortCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbActiveOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelJobs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadListBox lbJobs;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadCheckBox cbWelcomeContractor;
        private Telerik.WinControls.UI.RadCheckBox cbWelcomeManager;
        private Telerik.WinControls.UI.RadButton SendEmailButton;
        private winployDataSet winployDataSet;
        private System.Windows.Forms.BindingSource jobsAllBindingSource;
        private AdminTimecard.winployDataSetTableAdapters.JobsAllTableAdapter jobsAllTableAdapter;
        private System.Windows.Forms.GroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel radLabelComments;
        private Telerik.WinControls.UI.RadTextBox tbComments;
        private System.Windows.Forms.GroupBox groupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private DateTimePicker startDatePicker;
        private DateTimePicker stopDatePicker;
        private Telerik.WinControls.UI.RadCheckBox cbSendToAdmin;
        private Telerik.WinControls.UI.RadRadioButton radioSortContractor;
        private Telerik.WinControls.UI.RadRadioButton radioSortManager;
        private Telerik.WinControls.UI.RadRadioButton radioSortCompany;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadCheckBox cbActiveOnly;
        private Telerik.WinControls.UI.RadLabel labelJobs;
    }
}