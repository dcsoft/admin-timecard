﻿using System.Data.SqlClient;

//////////////////////////////////////////////////////////////////////////
// Partial class which extends the table adapters, allowing a new
// connection string to be used.  After setting the ConnectionString
// property, to get the data from the new connection, call the Fill()
// method.
//
// Source:  Data Binding with Windows Forms 2.0: Programming Smart Client Data Applications with .NET 
// By Brian Noyes
// Publisher: Addison Wesley Professional, 2006

namespace AdminTimecard.winployDataSetTableAdapters
{
    public partial class JobsAllTableAdapter
    {
        public string ConnectionString
        {
            set
            {
                Connection = new SqlConnection(value);
            }
        }
    }

    public partial class TimesheetsTableAdapter
    {
        public string ConnectionString
        {
            set
            {
                Connection = new SqlConnection(value);
            }
        }
    }

    public partial class ApprovedTimesheetsTableAdapter
    {
        public string ConnectionString
        {
            set
            {
                Connection = new SqlConnection(value);
            }
        }
    }

    public partial class SubmittedTimesheetsTableAdapter
    {
        public string ConnectionString
        {
            set
            {
                Connection = new SqlConnection(value);
            }
        }
    }
}
