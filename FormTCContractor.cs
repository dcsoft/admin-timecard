﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;
using Telerik.WinControls.UI;


namespace AdminTimecard
{
    public partial class FormTCContractor : Form
    {
        public FormTCContractor()
        {
            InitializeComponent();
            Visible = false;
        }

        private void FormTCContractor_VisibleChanged(object sender, EventArgs e)
        {
            // this is called several times even when Visible hasn't changed
            if (Visible == lastVisible)
                return;

            lastVisible = Visible;

            if (!Visible)
            {
                cbContractors.SelectedIndex = -1;
                return;
            }

            jobsAllTableAdapter.ConnectionString = Form1.ConnectionString;
            timesheetsTableAdapter.ConnectionString = Form1.ConnectionString;
            FillJobs();
        }

        private void FillJobs()
        {
            isFilling = true;
            jobsAllTableAdapter.Fill(winployDataSet.JobsAll);
            isFilling = false;
        }

        private void FillJobTimesheets(int jobId)
        {
            isFilling = true;
            timesheetsTableAdapter.FillByJobId(winployDataSet.Timesheets, jobId);
            isFilling = false;
        }


        private void cbContractors_ItemDataBound(object sender, ItemDataBoundEventArgs e)
        {
            // hack
            if (!Visible)
                return;

            // Fill combobox with Contractor's first and last name
            DataRowView drv = (DataRowView)e.DataItem;
            if (e.DataBoundItem != null && drv != null)
            {
                e.DataBoundItem.Text = string.Format("{0} {1}", drv["ContractorFirstName"], drv["ContractorLastName"]);
//                 string vendor = drv["VendorName"].ToString();
//                 if ( !string.IsNullOrEmpty(vendor) )
//                     e.DataBoundItem.Text += string.Format(" ({0})", vendor);
                string company = drv["CustomerCompany"].ToString();
                e.DataBoundItem.Text += string.Format(" ({0})", company);
            }
        }

        private void cbContractor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Visible && cbContractors.SelectedValue != null)
                {
                    FillJobTimesheets((int)cbContractors.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lbTimecards_ItemDataBound(object sender, ItemDataBoundEventArgs e)
        {
            // Fill combobox with Contractor's first and last name
            DataRowView drv = (DataRowView)e.DataItem;
            if (e.DataBoundItem != null && drv != null)
            {
                DateTime dt = (DateTime) drv["WeekEndingDate"];
                e.DataBoundItem.Text = dt.ToString("d");
            }
        }

        private void lbTimecards_SelectedIndexChanged(object sender, EventArgs e)
        {
            // hack
            if (!Visible)
                return;

            isFilling = true;

            if (lbTimecards.SelectedIndex < 0)     // no selected radItem
            {
                cbStatus.SelectedIndex = cbStatus.Items.Count - 1; // last radItem is blank
                cbStatus.Enabled = false;
            }
            else
            {
                try
                {
                    winployDataSet.TimesheetsRow row = winployDataSet.Timesheets[lbTimecards.SelectedIndex];

                    // Status combo is not data bound; no way to bind TimecardStatus
                    // to cbStatus.SelectedIndex.
                    cbStatus.Enabled = true;
                    cbStatus.SelectedIndex = row.TimecardStatus + 1;        // TimecardStatus == -1 (Saved) ==> .SelectedIndex == 0  
                }
                catch (DeletedRowInaccessibleException)
                {
                    cbStatus.SelectedIndex = cbStatus.Items.Count - 1; // last radItem is blank
                }
            }

            isFilling = false;
        }

        private delegate void ResetIndexDelegate(int i);
        private void ResetIndex(int index)
        {
            // Set cbStatus.SelectedIndex back to original index
            // Remove delegate and restore it afterwards so we don't get recursion
            // since the delegate calls this method.
            cbStatus.SelectedIndexChanged -= cbStatus_SelectedIndexChanged;
            cbStatus.SelectedIndex = index;
            cbStatus.SelectedIndexChanged += cbStatus_SelectedIndexChanged;
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            // hack
            if (!Visible)
                return;

            if (!isFilling)
            {
                if (!Program.IsLoggedIn)
                {
                    // Set the combobox back to the original value.
                    // Delay doing this until this method is finished, or else it causes
                    // strange side effects
                    SelectedIndexChangedEventArgs args = (SelectedIndexChangedEventArgs) e;
                    cbStatus.BeginInvoke( new ResetIndexDelegate(ResetIndex), args.OldIndex );
                }
                else
                {
                    // Update database table with new timecard status
                    winployDataSet.TimesheetsRow timesheetRow = winployDataSet.Timesheets[lbTimecards.SelectedIndex];
                    timesheetRow.TimecardStatus = (short)(cbStatus.SelectedIndex - 1);    // .SelectedIndex == 0 ==> TimecardStatus == -1 (Saved)

                    string newStatus = null;
                    switch ( (TimecardStage.TimecardStatus) timesheetRow.TimecardStatus)
                    {
                        //case TimecardStage.TimecardStatus.Saved: newStatus = "Saved"; break;

                        case TimecardStage.TimecardStatus.Saved: newStatus = "Saved"; break;
                        case TimecardStage.TimecardStatus.Submitted: newStatus = "Submitted"; break;
                        case TimecardStage.TimecardStatus.Approved: newStatus = "Approved"; break;
                        case TimecardStage.TimecardStatus.InvoicedBilled: newStatus = "InvoicedBilled (DONE)"; break;
                        case TimecardStage.TimecardStatus.Billed: newStatus = "Billed"; break;
                        case TimecardStage.TimecardStatus.Invoiced: newStatus = "Invoiced"; break;
                        default: Debug.Assert(false); break;
                    }

                    if (!string.IsNullOrEmpty(newStatus))
                    {
                        string newComment = string.Format("Switched to {0} on {1}", newStatus, DateTime.Now);
                        if (string.IsNullOrEmpty(timesheetRow["Comments"].ToString()))
                            timesheetRow.Comments = newComment;
                        else
                            timesheetRow.Comments += "\r\n" + newComment;

                        timesheetsTableAdapter.Update(timesheetRow);
                    }
                }
            }
        }

        private void btnDelTimecard_Click(object sender, EventArgs e)
        {
            int i = lbTimecards.SelectedIndex;
            if (i >= 0)     // valid
            {
                if (!Program.IsLoggedIn)
                    return;

                int timecardId = (int)lbTimecards.SelectedValue;

                // Remove from cached dataset - causes bound lbTimecards to automatically update
                winployDataSet.Timesheets.RemoveTimesheetsRow(winployDataSet.Timesheets[i]);

                // Remove from actual database
                timesheetsTableAdapter.Delete(timecardId);

                // Set new index
                lbTimecards.SelectedIndex = -1;
                if (i < lbTimecards.Items.Count)
                    lbTimecards.SelectedIndex = i;
                else if ((i - 1) < lbTimecards.Items.Count)
                    lbTimecards.SelectedIndex = i - 1;
                else if (0 < lbTimecards.Items.Count)
                    lbTimecards.SelectedIndex = 0;
            }
        }

        private void btnCreateTimecard_Click(object sender, EventArgs e)
        {
#if false
            if (cbContractors.SelectedValue == null)
                return;

            int jobId = (int)cbContractors.SelectedValue;

            // Insert new Timesheet into table - 
            // 3/10/2010 - quick hack to add a timecard for Bruce Smith, but this could be the basis of a new
            // Job screen
            string timesheetGuid = Guid.NewGuid().ToString();
            using (SqlConnection conn = new SqlConnection(Form1.ConnectionString))
            {
                try
                {
                    conn.Open();

                    string cmdText = "INSERT INTO [Timesheets] ([InvoiceNum], [JobId], [WeekEndingDate], [HoursMon], [HoursTue], [HoursWed], [HoursThu], [HoursFri], [HoursSat], [HoursSun], [HoursTotal], [WorkDetails], [TimecardStatus], [Guid], [SubmitalTimeStamp], [Comments])";
                    string cmdValues = "VALUES (@InvoiceNum, @JobId, @WeekEndingDate, @HoursMon, @HoursTue, @HoursWed, @HoursThu, @HoursFri, @HoursSat, @HoursSun, @HoursTotal, @WorkDetails, @TimecardStatus, @Guid, @SubmitalTimeStamp, @Comments)";
                    using (SqlCommand cmd = new SqlCommand(cmdText + " " + cmdValues))
                    {
                        cmd.Parameters.AddWithValue("@InvoiceNum", " ");    // invoice number is automatically generated
                        cmd.Parameters.AddWithValue("@JobId", jobId);
                        cmd.Parameters.AddWithValue("@WeekEndingDate", "2/28/2010 12:00:00 AM");
                        cmd.Parameters.AddWithValue("@HoursMon", "1.5");
                        cmd.Parameters.AddWithValue("@HoursTue", "2.5");
                        cmd.Parameters.AddWithValue("@HoursWed", "2");
                        cmd.Parameters.AddWithValue("@HoursThu", "1");
                        cmd.Parameters.AddWithValue("@HoursFri", "0");
                        cmd.Parameters.AddWithValue("@HoursSat", "2");
                        cmd.Parameters.AddWithValue("@HoursSun", "5");
                        cmd.Parameters.AddWithValue("@HoursTotal", "14");
                        cmd.Parameters.AddWithValue("@WorkDetails", "Initial meeting, software setup, tested various setups, trace analysis, tested additional pucks, and initial review of driver software.");
                        cmd.Parameters.AddWithValue("@TimecardStatus", 0);      // 0 means Submitted 
                        cmd.Parameters.AddWithValue("@Guid", timesheetGuid);
                        cmd.Parameters.AddWithValue("@SubmitalTimeStamp", "2/28/2010 09:31:00 PM");
                        cmd.Parameters.AddWithValue("@Comments", string.Format("Submitted on {0}", "2/28/2010 09:31:00 PM"));

                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Connection = conn;
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    string submitError = string.Format("There was a problem saving your timecard.");
                    MessageBox.Show(submitError);
                }
            }
#endif
        }


        private void radLabelComments_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // Clear Comments field if Control+Mouse double click on Comments label occurs
            int i = lbTimecards.SelectedIndex;
            if (i < 0)
                return;

            // Abort if Double-click without Control key being pressed
            if ((ModifierKeys & Keys.Control) != Keys.Control)       // Control key is not pressed
                return;

            // Clear Comments field if Control+Mouse double click on Comments label occurs
            if (!Program.IsLoggedIn)
                return;

            winployDataSet.TimesheetsRow timesheetRow = winployDataSet.Timesheets[i];
            timesheetRow.Comments = null;
            timesheetsTableAdapter.Update(timesheetRow);

            tbComments.Text = string.Empty;
        }


        bool lastVisible;
        bool isFilling;

    }
}
