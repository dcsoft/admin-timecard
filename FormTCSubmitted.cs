﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using System.Data;


namespace AdminTimecard
{
    public partial class FormTCSubmitted : Form
    {
        public FormTCSubmitted()
        {
            InitializeComponent();
            Visible = false;

            radGridView1.MultiSelect = false;

            // Q3 2008 SP1:  Setting FormatString in designer doesn't take effect.
            // Perhaps it's fixed in later versions.
//             radGridView1.MasterGridViewTemplate.Columns["WeekEndingDate"].FormatString = @"{0:d}";
//             radGridView1.MasterGridViewTemplate.Columns["SubmitalTimeStamp"].FormatString = @"{0:d}";
        }

        private void FormTCSubmitted_Load(object sender, EventArgs e)
        {
            // Default Report date to today
            datePicker.Value = DateTime.Now;    
        }

        private void FormTCUnsubmitted_VisibleChanged(object sender, EventArgs e)
        {
            // this is called several times even when Visible hasn't changed
            if (Visible == lastVisible)
                return;

            lastVisible = Visible;

            if (!Visible)
                return;

            submittedTimesheetsTableAdapter.ConnectionString = Form1.ConnectionString;

            // Re-run current report (if any) to reflect latest changes
            // (possibly made in other screens before switching back to this one.)
            if (gridFilled)
                FillGrid();
        }

        private void btnCreateReport_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            // Loading the Approved Timesheets data source causes the grid to fill
            // Force loading so any changes made in other pages to the approved 
            // timesheets are shown
            using (WaitCursor wc = new WaitCursor())
            {
                gridFilled = true;
                isFilling = true;

                try
                {
                    submittedTimesheetsTableAdapter.FillBefore(winployDataSet.SubmittedTimesheets, datePicker.Value);
                }
                finally
                {
                    isFilling = false;
                }

                int numSubmitted = winployDataSet.SubmittedTimesheets.Count;
                if (numSubmitted == 0)
                    radLabelHeading.Text = string.Format("No unapproved timecards as of {0}", datePicker.Value.ToString("d"));
                else if (numSubmitted == 1)
                    radLabelHeading.Text = string.Format("1 unapproved timecard as of {0}", datePicker.Value.ToString("d"));
                else
                    radLabelHeading.Text = string.Format("{0} unapproved timecards as of {1}", numSubmitted, datePicker.Value.ToString("d"));

                // Enable or disable Export button depending if there are any jobs in the report
                btnExport.Enabled = (numSubmitted >= 0);

                radGridView1.MasterGridViewTemplate.BestFitColumns();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            // Get filename to export to
            if (!FileExport.ShowSaveFileDialog(ref csvFilePath))
                return;

            // Write export file
            string header = @"Ending,Submitted,Hours,First,Last,Company,ManagerFirst,ManagerLast";
            try
            {
                using (FileStream fs = new FileStream(csvFilePath, FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs))
                    {
                        // Write header
                        w.WriteLine(header);

                        // Write each row
                        StringBuilder b = new StringBuilder();
                        foreach (winployDataSet.SubmittedTimesheetsRow row in winployDataSet.SubmittedTimesheets)
                        {
                            b.AppendFormat("{0}", FileExport.QuoteCSVItem(row.WeekEndingDate.ToString("d")));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.SubmitalTimeStamp.ToString("d")));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.SubmitalTimeStamp.ToString("d")));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.ContractorFirstName));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.ContractorLastName));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.CustomerCompany));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.CustomerFirstName));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.CustomerLastName));
                            b.AppendLine();
                        }
                        w.WriteLine(b);
                    }
                }
            }
            catch (Exception ex)
            {
                // Inform error
                MessageBox.Show(ex.Message, "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Prompt user to open newly exported file
            FileExport.OpenExportedFile(csvFilePath);
        }


        bool lastVisible;
        bool isFilling;
        bool gridFilled;
        string csvFilePath;

    }
}
