﻿using System.Windows.Forms;


namespace AdminTimecard
{
    partial class FormQBBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.rlShowingTitle = new Telerik.WinControls.UI.RadLabel();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.approvedTimesheetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.winployDataSet = new AdminTimecard.winployDataSet();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.rlSetStatusBilled = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.lbStatus = new Telerik.WinControls.UI.RadListBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.rlGenerateOneInvoice = new Telerik.WinControls.UI.RadLabel();
            this.rlGenerateBills = new Telerik.WinControls.UI.RadLabel();
            this.rlGenerateInvoices = new Telerik.WinControls.UI.RadLabel();
            this.rlGenerateBoth = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.rlSelectNone = new Telerik.WinControls.UI.RadLabel();
            this.rlSelectAll = new Telerik.WinControls.UI.RadLabel();
            this.rlNumSelected = new Telerik.WinControls.UI.RadLabel();
            this.approvedTimesheetsTableAdapter = new AdminTimecard.winployDataSetTableAdapters.ApprovedTimesheetsTableAdapter();
            this.timesheetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.timesheetsTableAdapter = new AdminTimecard.winployDataSetTableAdapters.TimesheetsTableAdapter();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.rlShowUnbilled = new Telerik.WinControls.UI.RadLabel();
            this.rlShowUninvoiced = new Telerik.WinControls.UI.RadLabel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            ((System.ComponentModel.ISupportInitialize)(this.rlShowingTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterGridViewTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.approvedTimesheetsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlSetStatusBilled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlGenerateOneInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlGenerateBills)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlGenerateInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlGenerateBoth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlSelectNone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlNumSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesheetsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlShowUnbilled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlShowUninvoiced)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rlShowingTitle
            // 
            this.rlShowingTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlShowingTitle.Location = new System.Drawing.Point(3, 3);
            this.rlShowingTitle.Name = "rlShowingTitle";
            this.rlShowingTitle.Size = new System.Drawing.Size(66, 17);
            this.rlShowingTitle.TabIndex = 6;
            this.rlShowingTitle.Text = "Timecards";
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.BackColor = System.Drawing.SystemColors.Control;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(159, 34);
            // 
            // 
            // 
            this.radGridView1.MasterGridViewTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterGridViewTemplate.AllowCellContextMenu = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnChooser = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnReorder = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnResize = false;
            this.radGridView1.MasterGridViewTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterGridViewTemplate.AllowDragToGroup = false;
            this.radGridView1.MasterGridViewTemplate.AllowRowResize = false;
            gridViewCheckBoxColumn3.AllowFiltering = false;
            gridViewCheckBoxColumn3.AllowGroup = false;
            gridViewCheckBoxColumn3.AllowHide = false;
            gridViewCheckBoxColumn3.AllowResize = false;
            gridViewCheckBoxColumn3.AllowSort = false;
            gridViewCheckBoxColumn3.DataType = typeof(bool);
            gridViewCheckBoxColumn3.HeaderText = global::AdminTimecard.Properties.Resources.String1;
            gridViewCheckBoxColumn3.UniqueName = "column1";
            gridViewCheckBoxColumn3.Width = 41;
            gridViewTextBoxColumn17.FieldAlias = "columnBill";
            gridViewTextBoxColumn17.HeaderText = "Billable";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.UniqueName = "columnBill";
            gridViewTextBoxColumn17.Width = 75;
            gridViewTextBoxColumn18.FieldAlias = "columnInvoice";
            gridViewTextBoxColumn18.HeaderText = "Invoicable";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.UniqueName = "columnInvoice";
            gridViewTextBoxColumn18.Width = 75;
            gridViewDateTimeColumn3.DataType = typeof(System.DateTime);
            gridViewDateTimeColumn3.FieldAlias = "WeekEndingDate";
            gridViewDateTimeColumn3.FieldName = "WeekEndingDate";
            gridViewDateTimeColumn3.FormatString = "{0:d}";
            gridViewDateTimeColumn3.HeaderText = "Ending";
            gridViewDateTimeColumn3.ReadOnly = true;
            gridViewDateTimeColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn3.UniqueName = "WeekEndingDate";
            gridViewDateTimeColumn3.Width = 116;
            gridViewTextBoxColumn19.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn19.FieldAlias = "Approved";
            gridViewTextBoxColumn19.FieldName = "ApprovalTimestamp";
            gridViewTextBoxColumn19.FormatString = "{0:d}";
            gridViewTextBoxColumn19.HeaderText = "Approved";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn19.UniqueName = "Approved";
            gridViewTextBoxColumn19.Width = 41;
            gridViewTextBoxColumn20.DataType = typeof(double);
            gridViewTextBoxColumn20.FieldAlias = "Hours";
            gridViewTextBoxColumn20.FieldName = "HoursTotal";
            gridViewTextBoxColumn20.HeaderText = "Hours";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn20.UniqueName = "Hours";
            gridViewTextBoxColumn20.Width = 41;
            gridViewTextBoxColumn21.FieldAlias = "ContractorFirstName";
            gridViewTextBoxColumn21.FieldName = "ContractorFirstName";
            gridViewTextBoxColumn21.HeaderText = "First";
            gridViewTextBoxColumn21.ReadOnly = true;
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn21.UniqueName = "ContractorFirstName";
            gridViewTextBoxColumn21.Width = 116;
            gridViewTextBoxColumn22.FieldAlias = "ContractorLastName";
            gridViewTextBoxColumn22.FieldName = "ContractorLastName";
            gridViewTextBoxColumn22.HeaderText = "Last";
            gridViewTextBoxColumn22.ReadOnly = true;
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn22.UniqueName = "ContractorLastName";
            gridViewTextBoxColumn22.Width = 116;
            gridViewTextBoxColumn23.FieldAlias = "CustomerCompany";
            gridViewTextBoxColumn23.FieldName = "CustomerCompany";
            gridViewTextBoxColumn23.HeaderText = "Customer";
            gridViewTextBoxColumn23.Multiline = true;
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn23.UniqueName = "CustomerCompany";
            gridViewTextBoxColumn23.Width = 116;
            gridViewTextBoxColumn24.DisableHTMLRendering = false;
            gridViewTextBoxColumn24.FieldAlias = "VendorName";
            gridViewTextBoxColumn24.FieldName = "VendorName";
            gridViewTextBoxColumn24.HeaderText = "Vendor";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn24.UniqueName = "VendorName";
            gridViewTextBoxColumn24.Width = 116;
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewCheckBoxColumn3);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn17);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn18);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewDateTimeColumn3);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn19);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn20);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn21);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn22);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn23);
            this.radGridView1.MasterGridViewTemplate.Columns.Add(gridViewTextBoxColumn24);
            this.radGridView1.MasterGridViewTemplate.DataSource = this.approvedTimesheetsBindingSource;
            this.radGridView1.MasterGridViewTemplate.EnableGrouping = false;
            this.radGridView1.MasterGridViewTemplate.ShowFilteringRow = false;
            this.radGridView1.MasterGridViewTemplate.ShowRowHeaderColumn = false;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radGridView1.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.Size = new System.Drawing.Size(638, 346);
            this.radGridView1.TabIndex = 7;
            this.radGridView1.Text = "radGridViewPreview";
            this.radGridView1.ThemeName = "Office2007Black";
            this.radGridView1.ValueChanged += new System.EventHandler(this.radGridView1_ValueChanged);
            // 
            // approvedTimesheetsBindingSource
            // 
            this.approvedTimesheetsBindingSource.DataMember = "ApprovedTimesheets";
            this.approvedTimesheetsBindingSource.DataSource = this.winployDataSet;
            // 
            // winployDataSet
            // 
            this.winployDataSet.DataSetName = "winployDataSet";
            this.winployDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radGroupBox3.Controls.Add(this.rlSetStatusBilled);
            this.radGroupBox3.FooterImageIndex = -1;
            this.radGroupBox3.FooterImageKey = global::AdminTimecard.Properties.Resources.String1;
            this.radGroupBox3.HeaderImageIndex = -1;
            this.radGroupBox3.HeaderImageKey = global::AdminTimecard.Properties.Resources.String1;
            this.radGroupBox3.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.radGroupBox3.HeaderText = "Timecard";
            this.radGroupBox3.Location = new System.Drawing.Point(11, 436);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox3.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox3.Size = new System.Drawing.Size(141, 110);
            this.radGroupBox3.TabIndex = 11;
            this.radGroupBox3.Text = "Timecard";
            this.radGroupBox3.ThemeName = "Office2007Black";
            // 
            // rlSetStatusBilled
            // 
            this.rlSetStatusBilled.AutoSize = false;
            this.rlSetStatusBilled.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlSetStatusBilled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlSetStatusBilled.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlSetStatusBilled.Location = new System.Drawing.Point(13, 23);
            this.rlSetStatusBilled.Name = "rlSetStatusBilled";
            // 
            // 
            // 
            this.rlSetStatusBilled.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlSetStatusBilled.Size = new System.Drawing.Size(114, 53);
            this.rlSetStatusBilled.TabIndex = 0;
            this.rlSetStatusBilled.Text = "Mark selected timecards as Billed";
            this.rlSetStatusBilled.Click += new System.EventHandler(this.rlSetStatusBilled_Click);
            // 
            // radLabel6
            // 
            this.radLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(159, 386);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(43, 17);
            this.radLabel6.TabIndex = 7;
            this.radLabel6.Text = "Status";
            // 
            // lbStatus
            // 
            this.lbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbStatus.FormatString = "{0}";
            this.lbStatus.Location = new System.Drawing.Point(159, 408);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(638, 138);
            this.lbStatus.TabIndex = 9;
            this.lbStatus.ThemeName = "ERGTheme";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.Controls.Add(this.rlGenerateOneInvoice);
            this.radGroupBox1.Controls.Add(this.rlGenerateBills);
            this.radGroupBox1.Controls.Add(this.rlGenerateInvoices);
            this.radGroupBox1.Controls.Add(this.rlGenerateBoth);
            this.radGroupBox1.FooterImageIndex = -1;
            this.radGroupBox1.FooterImageKey = global::AdminTimecard.Properties.Resources.String1;
            this.radGroupBox1.HeaderImageIndex = -1;
            this.radGroupBox1.HeaderImageKey = global::AdminTimecard.Properties.Resources.String1;
            this.radGroupBox1.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.radGroupBox1.HeaderText = "QuickBooks";
            this.radGroupBox1.Location = new System.Drawing.Point(12, 201);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox1.Size = new System.Drawing.Size(140, 229);
            this.radGroupBox1.TabIndex = 10;
            this.radGroupBox1.Text = "QuickBooks";
            this.radGroupBox1.ThemeName = "Office2007Black";
            // 
            // rlGenerateOneInvoice
            // 
            this.rlGenerateOneInvoice.AutoSize = false;
            this.rlGenerateOneInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlGenerateOneInvoice.Enabled = false;
            this.rlGenerateOneInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlGenerateOneInvoice.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlGenerateOneInvoice.Location = new System.Drawing.Point(13, 166);
            this.rlGenerateOneInvoice.Name = "rlGenerateOneInvoice";
            // 
            // 
            // 
            this.rlGenerateOneInvoice.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlGenerateOneInvoice.Size = new System.Drawing.Size(114, 53);
            this.rlGenerateOneInvoice.TabIndex = 4;
            this.rlGenerateOneInvoice.Text = "Create single invoice for selected timecards";
            this.rlGenerateOneInvoice.Click += new System.EventHandler(this.rlGenerateOneInvoice_Click);
            // 
            // rlGenerateBills
            // 
            this.rlGenerateBills.AutoSize = false;
            this.rlGenerateBills.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlGenerateBills.Enabled = false;
            this.rlGenerateBills.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlGenerateBills.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlGenerateBills.Location = new System.Drawing.Point(13, 23);
            this.rlGenerateBills.Name = "rlGenerateBills";
            // 
            // 
            // 
            this.rlGenerateBills.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlGenerateBills.Size = new System.Drawing.Size(114, 38);
            this.rlGenerateBills.TabIndex = 3;
            this.rlGenerateBills.Text = "Create selected bills";
            this.rlGenerateBills.Click += new System.EventHandler(this.rlGenerateBills_Click);
            // 
            // rlGenerateInvoices
            // 
            this.rlGenerateInvoices.AutoSize = false;
            this.rlGenerateInvoices.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlGenerateInvoices.Enabled = false;
            this.rlGenerateInvoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlGenerateInvoices.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlGenerateInvoices.Location = new System.Drawing.Point(13, 67);
            this.rlGenerateInvoices.Name = "rlGenerateInvoices";
            // 
            // 
            // 
            this.rlGenerateInvoices.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlGenerateInvoices.Size = new System.Drawing.Size(114, 42);
            this.rlGenerateInvoices.TabIndex = 2;
            this.rlGenerateInvoices.Text = "Create selected invoices";
            this.rlGenerateInvoices.Click += new System.EventHandler(this.rlGenerateInvoices_Click);
            // 
            // rlGenerateBoth
            // 
            this.rlGenerateBoth.AutoSize = false;
            this.rlGenerateBoth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlGenerateBoth.Enabled = false;
            this.rlGenerateBoth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlGenerateBoth.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlGenerateBoth.Location = new System.Drawing.Point(13, 115);
            this.rlGenerateBoth.Name = "rlGenerateBoth";
            // 
            // 
            // 
            this.rlGenerateBoth.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlGenerateBoth.Size = new System.Drawing.Size(114, 45);
            this.rlGenerateBoth.TabIndex = 1;
            this.rlGenerateBoth.Text = "Create selected bills and invoices";
            this.rlGenerateBoth.Click += new System.EventHandler(this.rlGenerate_Click);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.Controls.Add(this.rlSelectNone);
            this.radGroupBox2.Controls.Add(this.rlSelectAll);
            this.radGroupBox2.FooterImageIndex = -1;
            this.radGroupBox2.FooterImageKey = global::AdminTimecard.Properties.Resources.String1;
            this.radGroupBox2.HeaderImageIndex = -1;
            this.radGroupBox2.HeaderImageKey = global::AdminTimecard.Properties.Resources.String1;
            this.radGroupBox2.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.radGroupBox2.HeaderText = "Select Timecards";
            this.radGroupBox2.Location = new System.Drawing.Point(12, 120);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox2.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox2.Size = new System.Drawing.Size(140, 75);
            this.radGroupBox2.TabIndex = 11;
            this.radGroupBox2.Text = "Select Timecards";
            this.radGroupBox2.ThemeName = "Office2007Black";
            // 
            // rlSelectNone
            // 
            this.rlSelectNone.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlSelectNone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlSelectNone.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlSelectNone.Location = new System.Drawing.Point(10, 45);
            this.rlSelectNone.Name = "rlSelectNone";
            // 
            // 
            // 
            this.rlSelectNone.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlSelectNone.Size = new System.Drawing.Size(72, 17);
            this.rlSelectNone.TabIndex = 0;
            this.rlSelectNone.Text = "Select none";
            this.rlSelectNone.Click += new System.EventHandler(this.rlSelectNone_Click);
            // 
            // rlSelectAll
            // 
            this.rlSelectAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlSelectAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlSelectAll.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlSelectAll.Location = new System.Drawing.Point(10, 23);
            this.rlSelectAll.Name = "rlSelectAll";
            // 
            // 
            // 
            this.rlSelectAll.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlSelectAll.Size = new System.Drawing.Size(57, 17);
            this.rlSelectAll.TabIndex = 1;
            this.rlSelectAll.Text = "Select all";
            this.rlSelectAll.Click += new System.EventHandler(this.rlSelectAll_Click);
            // 
            // rlNumSelected
            // 
            this.rlNumSelected.Location = new System.Drawing.Point(75, 3);
            this.rlNumSelected.Name = "rlNumSelected";
            this.rlNumSelected.Size = new System.Drawing.Size(78, 17);
            this.rlNumSelected.TabIndex = 14;
            this.rlNumSelected.Text = "(33 selected)";
            // 
            // approvedTimesheetsTableAdapter
            // 
            this.approvedTimesheetsTableAdapter.ClearBeforeFill = true;
            // 
            // timesheetsBindingSource
            // 
            this.timesheetsBindingSource.DataMember = "Timesheets";
            this.timesheetsBindingSource.DataSource = this.winployDataSet;
            // 
            // timesheetsTableAdapter
            // 
            this.timesheetsTableAdapter.ClearBeforeFill = true;
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.Controls.Add(this.rlShowUnbilled);
            this.radGroupBox4.Controls.Add(this.rlShowUninvoiced);
            this.radGroupBox4.FooterImageIndex = -1;
            this.radGroupBox4.FooterImageKey = global::AdminTimecard.Properties.Resources.String1;
            this.radGroupBox4.HeaderImageIndex = -1;
            this.radGroupBox4.HeaderImageKey = global::AdminTimecard.Properties.Resources.String1;
            this.radGroupBox4.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.radGroupBox4.HeaderText = "Show Timecards";
            this.radGroupBox4.Location = new System.Drawing.Point(12, 12);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox4.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox4.Size = new System.Drawing.Size(140, 102);
            this.radGroupBox4.TabIndex = 12;
            this.radGroupBox4.Text = "Show Timecards";
            this.radGroupBox4.ThemeName = "Office2007Black";
            // 
            // rlShowUnbilled
            // 
            this.rlShowUnbilled.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlShowUnbilled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlShowUnbilled.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlShowUnbilled.Location = new System.Drawing.Point(10, 24);
            this.rlShowUnbilled.Name = "rlShowUnbilled";
            // 
            // 
            // 
            this.rlShowUnbilled.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlShowUnbilled.Size = new System.Drawing.Size(84, 17);
            this.rlShowUnbilled.TabIndex = 0;
            this.rlShowUnbilled.Text = "Show unbilled";
            this.rlShowUnbilled.Click += new System.EventHandler(this.rlShowUnbilled_Click);
            // 
            // rlShowUninvoiced
            // 
            this.rlShowUninvoiced.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rlShowUninvoiced.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlShowUninvoiced.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlShowUninvoiced.Location = new System.Drawing.Point(10, 45);
            this.rlShowUninvoiced.Name = "rlShowUninvoiced";
            // 
            // 
            // 
            this.rlShowUninvoiced.RootElement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rlShowUninvoiced.Size = new System.Drawing.Size(100, 17);
            this.rlShowUninvoiced.TabIndex = 1;
            this.rlShowUninvoiced.Text = "Show uninvoiced";
            this.rlShowUninvoiced.Click += new System.EventHandler(this.rlShowUninvoiced_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.rlShowingTitle);
            this.flowLayoutPanel1.Controls.Add(this.rlNumSelected);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(159, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(631, 23);
            this.flowLayoutPanel1.TabIndex = 15;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // FormQBBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.radGroupBox4);
            this.Controls.Add(this.radGroupBox3);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.lbStatus);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormQBBill";
            this.Text = "FormQBBill";
            this.VisibleChanged += new System.EventHandler(this.FormQBBill_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.rlShowingTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterGridViewTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.approvedTimesheetsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.winployDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rlSetStatusBilled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rlGenerateOneInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlGenerateBills)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlGenerateInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlGenerateBoth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlSelectNone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlNumSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesheetsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlShowUnbilled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlShowUninvoiced)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel rlShowingTitle;
        private Telerik.WinControls.UI.RadGridView radGridView1;

        private BindingSource approvedTimesheetsBindingSource;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadListBox lbStatus;
        private winployDataSet winployDataSet;
        private AdminTimecard.winployDataSetTableAdapters.ApprovedTimesheetsTableAdapter approvedTimesheetsTableAdapter;
        private BindingSource timesheetsBindingSource;
        private AdminTimecard.winployDataSetTableAdapters.TimesheetsTableAdapter timesheetsTableAdapter;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel rlSetStatusBilled;
        private Telerik.WinControls.UI.RadLabel rlGenerateBoth;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel rlSelectNone;
        private Telerik.WinControls.UI.RadLabel rlSelectAll;
        private Telerik.WinControls.UI.RadLabel rlGenerateBills;
        private Telerik.WinControls.UI.RadLabel rlGenerateInvoices;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadLabel rlGenerateOneInvoice;
        private Telerik.WinControls.UI.RadLabel rlNumSelected;
        private Telerik.WinControls.UI.RadLabel rlShowUnbilled;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadLabel rlShowUninvoiced;
        private FlowLayoutPanel flowLayoutPanel1;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
    }
}