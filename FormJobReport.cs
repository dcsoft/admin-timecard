﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;


namespace AdminTimecard
{
    public partial class FormJobReport : Form
    {
        public FormJobReport()
        {
            InitializeComponent();
            Visible = false;
        }

        private void FormJobReport_Load(object sender, EventArgs e)
        {
            // Default Report date to today
            datePicker.Value = DateTime.Now;    
        }

        private void FormJobEmail_VisibleChanged(object sender, EventArgs e)
        {
            // this is called several times even when Visible hasn't changed
            if (Visible == lastVisible)
                return;

            lastVisible = Visible;

            if (!Visible)
            {
                lbJobs.SelectedIndex = -1;
                return;
            }

            jobsAllTableAdapter.ConnectionString = Form1.ConnectionString;

            // Re-run current report (if any) to reflect latest changes
            // (possibly made in other screens before switching back to this one.)
            if (filled)
                FillActiveJobs();
        }

        

        private void FillActiveJobs()
        {
            using (WaitCursor wc = new WaitCursor())
            {
                filled = true;
                isFilling = true;

                try
                {
                    // There is some inconsistency between the database and the .xsd file that we must
                    // not enforce constraints, or else we get the exception:
                    // "Failed to enable constraints. One or more rows contain values violating non-null, unique, or foreign-key constraints."
                    winployDataSet.EnforceConstraints = false;

                    jobsAllTableAdapter.FillActiveJobs(winployDataSet.JobsAll, datePicker.Value.ToString("d"));

                    lbJobs.DataSource = winployDataSet.JobsAll.DefaultView;
                    SortJobs();
                }
                finally
                {
                    isFilling = false;
                }
            }

            int activeJobs = winployDataSet.JobsAll.Count;
            if (activeJobs == 0)
                radLabelHeading.Text = string.Format("No active jobs as of {0}", datePicker.Value.ToString("d"));
            else if (activeJobs == 1)
                radLabelHeading.Text = string.Format("1 active job as of {0}", datePicker.Value.ToString("d"));
            else
                radLabelHeading.Text = string.Format("{0} active jobs as of {1}", activeJobs, datePicker.Value.ToString("d"));

            // Enable or disable Export button depending if there are any jobs in the report
            btnExport.Enabled = (activeJobs >= 0);
        }

        private void SortJobs()
        {
            string sortFields = string.Empty;
            if (radioSortContractor.ToggleState == ToggleState.On)
                sortFields = "ContractorFirstName, ContractorLastName";
            else if (radioSortCompany.ToggleState == ToggleState.On)
                sortFields = "CustomerCompany";
            else if (radioSortManager.ToggleState == ToggleState.On)
                sortFields = "CustomerFirstName, CustomerLastName";

            if (!string.IsNullOrEmpty(sortFields))
                winployDataSet.JobsAll.DefaultView.Sort = sortFields;
        }

        private void lbJobs_ItemDataBound(object sender, ItemDataBoundEventArgs e)
        {
            // hack
            if (!Visible)
                return;

            // Format listbox items
            DataRowView drv = (DataRowView)e.DataItem;
            if (e.DataBoundItem != null && drv != null)
            {
                // First line is the current sort order
                // Second line is remaining fields
                string contractor = string.Format("{0} {1}", drv["ContractorFirstName"], drv["ContractorLastName"]);
                string company = drv["CustomerCompany"].ToString();
                string manager = string.Format("{0} {1}", drv["CustomerFirstName"], drv["CustomerLastName"]);
                string vendor = (drv["VendorName"] != null) ? drv["VendorName"].ToString() : null;

                string text = string.Empty;
                string descriptionText = string.Empty;
                if (radioSortContractor.ToggleState == ToggleState.On)
                {
                    text = string.Format("<html><b>{0}</b>", contractor);
                    if (!string.IsNullOrEmpty(vendor))
                        text += string.Format(" ({0})", vendor);

                    descriptionText = string.Format("{0}, {1}", company, manager);
                }
                else if (radioSortCompany.ToggleState == ToggleState.On)
                {
                    text = string.Format("<html><b>{0}</b>, {1}", company, manager); ;

                    descriptionText = contractor;
                    if (!string.IsNullOrEmpty(vendor))
                        descriptionText += string.Format(" ({0})", vendor);
                }
                else if (radioSortManager.ToggleState == ToggleState.On)
                {
                    text = string.Format("<html><b>{0}</b>, {1}", manager, company); ;

                    descriptionText = contractor;
                    if (!string.IsNullOrEmpty(vendor))
                        descriptionText += string.Format(" ({0})", vendor);
                }

                e.DataBoundItem.Text = text;
                RadListBoxItem item = (RadListBoxItem)e.DataBoundItem;
                item.Text = text;
                item.DescriptionText = descriptionText;
            }
        }

        private void btnCreateReport_Click(object sender, EventArgs e)
        {
            FillActiveJobs();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            // Get filename to export to
            if (!FileExport.ShowSaveFileDialog(ref csvFilePath))
                return;

            // Write export file
            string header = @"First,Last,Company,ManagerFirst,ManagerLast";
            try
            {
                using (FileStream fs = new FileStream(csvFilePath, FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs))
                    {
                        // Write header
                        w.WriteLine(header);

                        // Write each row
                        StringBuilder b = new StringBuilder();
                        foreach (winployDataSet.JobsAllRow row in winployDataSet.JobsAll)
                        {
                            b.AppendFormat("{0}", FileExport.QuoteCSVItem(row.ContractorFirstName));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.ContractorLastName));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.CustomerCompany));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.CustomerFirstName));
                            b.AppendFormat(",{0}", FileExport.QuoteCSVItem(row.CustomerLastName));
                            b.AppendLine();
                        }
                        w.WriteLine(b);
                    }
                }
            }
            catch (Exception ex)
            {
                // Inform error
                MessageBox.Show(ex.Message, "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Prompt user to open newly exported file
            FileExport.OpenExportedFile(csvFilePath);
        }

        private void radioSortContractor_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortJobs();
        }

        private void radioSortCompany_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortJobs();
        }

        private void radioSortManager_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortJobs();
        }


        bool lastVisible;
        bool isFilling;
        bool filled;
        string csvFilePath;
    }
}
