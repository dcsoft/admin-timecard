﻿using System;
using System.Data;
using System.Windows.Forms;
using AdminTimecard.AdminServices;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;


namespace AdminTimecard
{
    public partial class FormJobAdmin : Form
    {
        public FormJobAdmin()
        {
            InitializeComponent();

            Visible = false;
        }

        private void FormJobAdmin_VisibleChanged(object sender, EventArgs e)
        {
            // this is called several times even when Visible hasn't changed
            if (Visible == lastVisible)
                return;

            lastVisible = Visible;

            if (!Visible)
            {
                // The Date Time Picker's Validated() event is not fired when the checkbox is clicked
                // and the user then switches screens.  As a last resort, save any changes to the
                // date/time pickers in this page when this page is not used
                SaveDatePickers();

                lbJobs.SelectedIndex = -1;
                return;
            }

            jobsAllTableAdapter.ConnectionString = Form1.ConnectionString;
            FillJobs();
        }

        private void FillJobs()
        {
            isFilling = true;
            using (WaitCursor wc = new WaitCursor())
            {
                try
                {
                    // There is some inconsistency between the database and the .xsd file that we must
                    // not enforce constraints, or else we get the exception:
                    // "Failed to enable constraints. One or more rows contain values violating non-null, unique, or foreign-key constraints."
                    winployDataSet.EnforceConstraints = false;
                    if (cbActiveOnly.Checked)
                    {
                        jobsAllTableAdapter.FillActiveJobs(winployDataSet.JobsAll, DateTime.Now.ToString("d"));

                        // JobsAll dataset contains active jobs, so number is the Count.
                        int numActiveJobs = winployDataSet.JobsAll.Count;
                        labelJobs.Text = string.Format("{0} Active Job(s)", numActiveJobs);
                    }
                    else
                    {
                        jobsAllTableAdapter.Fill(winployDataSet.JobsAll);
                        int numJobs = winployDataSet.JobsAll.Count;
                        labelJobs.Text = string.Format("All {0} Job(s)", numJobs);
                    }

                    lbJobs.DataSource = winployDataSet.JobsAll.DefaultView;
                    SortJobs();
                }
                finally
                {
                    isFilling = false;
                }
            }

            lbJobs_SelectedIndexChanged(null, null);
        }

        private void SortJobs()
        {
            string sortFields = string.Empty;
            if (radioSortContractor.ToggleState == ToggleState.On)
                sortFields = "ContractorFirstName, ContractorLastName";
            else if (radioSortCompany.ToggleState == ToggleState.On)
                sortFields = "CustomerCompany";
            else if (radioSortManager.ToggleState == ToggleState.On)
                sortFields = "CustomerFirstName, CustomerLastName";

            if (!string.IsNullOrEmpty(sortFields))
                winployDataSet.JobsAll.DefaultView.Sort = sortFields;
        }


        private void lbJobs_ItemDataBound(object sender, ItemDataBoundEventArgs e)
        {
            // hack
            if (!Visible)
                return;

            // Format listbox items
            DataRowView drv = (DataRowView)e.DataItem;
            if (e.DataBoundItem != null && drv != null)
            {
                // First line is the current sort order
                // Second line is remaining fields
                winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow) drv.Row;
                string contractor = string.Format("{0} {1}", row.ContractorFirstName, row.ContractorLastName);
                string company = row.CustomerCompany;
                string manager = string.Format("{0} {1}", row.CustomerFirstName, row.CustomerLastName);
                string vendor = row.IsVendorNameNull() ? null : row.VendorName;

                string text = string.Empty;
                string descriptionText = string.Empty;
                if (radioSortContractor.ToggleState == ToggleState.On)
                {
                    text = string.Format("<html><b>{0}</b>", contractor);
                    if (!string.IsNullOrEmpty(vendor))
                        text += string.Format(" ({0})", vendor);

                    descriptionText = string.Format ("{0}, {1}", company, manager);
                }
                else if (radioSortCompany.ToggleState == ToggleState.On)
                {
                    text = string.Format ("<html><b>{0}</b>, {1}", company, manager);;

                    descriptionText = contractor;
                    if (!string.IsNullOrEmpty(vendor))
                        descriptionText += string.Format(" ({0})", vendor);
                }
                else if (radioSortManager.ToggleState == ToggleState.On)
                {
                    text = string.Format("<html><b>{0}</b>, {1}", manager, company); ;

                    descriptionText = contractor;
                    if (!string.IsNullOrEmpty(vendor))
                        descriptionText += string.Format(" ({0})", vendor);
                }

                RadListBoxItem item = (RadListBoxItem)e.DataBoundItem;
                item.Text = text;
                item.DescriptionText = descriptionText;
            }
        }

        private void cbWelcomeContractor_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            EnableSendMail();
        }

        private void cbWelcomeManager_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            EnableSendMail();
        }

        private void EnableSendMail()
        {
            // Enable Send button
            SendEmailButton.Enabled = cbWelcomeContractor.Checked || cbWelcomeManager.Checked;
        }

        private void SendEmailButton_Click(object sender, EventArgs e)
        {
            if (lbJobs.SelectedValue == null)    // no item selected in listbox
                return;

            // Execute webservice
            using (WaitCursor wc = new WaitCursor())
            {
                string comment = null;
                string error = null;
                try
                {
                    // Set authentication
                    ERGTimecardAdministratorServices svc = new ERGTimecardAdministratorServices();
                    svc.UserInfoHeaderValue = new UserInfoHeader();
                    svc.UserInfoHeaderValue.UserName = "xxx";     // Sent as cleartext over the wire; should send hash of username instead
                    svc.UserInfoHeaderValue.Password = "xxx";

                    // Url depends whether ergtimecard.com or winploy.com is selected
                    svc.Url = Form1.AdminServicesUrl;
                    string result = svc.SendWelcomeEmails((int)lbJobs.SelectedValue, cbWelcomeContractor.Checked, cbWelcomeManager.Checked, cbSendToAdmin.Checked);
                    if (result.StartsWith("Error:", StringComparison.InvariantCultureIgnoreCase))
                        error = result;     // the service has returned an error message
                    else
                        comment = result;   // the service has returned a successful message
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
                finally
                {
                    if (error != null)
                        MessageBox.Show(error);

                    if (comment != null)
                    {
                        // Add comment to job record
                        winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow) winployDataSet.JobsAll.DefaultView[lbJobs.SelectedIndex].Row;
                        if ( row.IsCommentsNull() )
                            row.Comments = comment;
                        else
                            row.Comments += "\r\n" + comment;
                        jobsAllTableAdapter.Update(row);

                        // Update textbox
                        tbComments.Text = row.Comments;
                    }
                }
            }
        }


        bool lastVisible;
        bool isFilling;

        private void radLabelComments_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // Clear Comments field
            int i = lbJobs.SelectedIndex;
            if (i < 0)
                return;

            // Abort if Double-click without Control key being pressed
            if ((ModifierKeys & Keys.Control) != Keys.Control)       // Control key is not pressed
                return;

            // Clear Comments field if Control+Mouse double click on Comments label occurs
            if (!Program.IsLoggedIn)
                return;

            winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow) winployDataSet.JobsAll.DefaultView[i].Row;
            row.Comments = null;
            jobsAllTableAdapter.Update(row);

            tbComments.Text = string.Empty;
        }

        private void lbJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!Visible || isFilling)
                return;

            int i = lbJobs.SelectedIndex;
            if (i < 0)
                return;

            winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow)winployDataSet.JobsAll.DefaultView[i].Row;
            DateTime dbDate = row.ActiveStartDate.Date;
            startDatePicker.Value = dbDate;
            startDatePicker.Checked = true;     // all jobs have a start date

            dbDate = row.ActiveStopDate.Date;
            stopDatePicker.Value = dbDate;
            stopDatePicker.Checked = (dbDate != defaultStopDate.Date);

            // For some reason, the data binding of the Comments label in the designer doesn't work.
            tbComments.Text = row.IsCommentsNull() ? null : row.Comments;
        }

        private void startDatePicker_Validated(object sender, EventArgs e)
        {
            int i = lbJobs.SelectedIndex;
            if (i < 0)
                return;

            winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow)winployDataSet.JobsAll.DefaultView[i].Row;
            row.ActiveStartDate = startDatePicker.Value.Date;
            jobsAllTableAdapter.Update(row);

            // Don't allow user to uncheck the start date
            if (!startDatePicker.Checked)
            {
                startDatePicker.Value = defaultStartDate.Date;

                // Force Start checkbox to be always checked (assume all jobs have a Start date.  One reason is because Active jobs
                // are defined if current date is greater than start date regardless of whether there is a start date defined.)
                startDatePicker.Checked = true;
            }

            // Update job list and number of active jobs
            FillJobs();
        }

        private void stopDatePicker_Validated(object sender, EventArgs e)
        {
            int i = lbJobs.SelectedIndex;
            if (i < 0)
                return;

            winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow)winployDataSet.JobsAll.DefaultView[i].Row;
            row.ActiveStopDate = (stopDatePicker.Checked) ? stopDatePicker.Value.Date : defaultStopDate.Date;
            jobsAllTableAdapter.Update(row);

            // Set default date if unchecked
            if (!stopDatePicker.Checked)
            {
                stopDatePicker.Value = defaultStopDate.Date;

                // Setting Value above causes Checked to be set to true!
                stopDatePicker.Checked = false;
            }

            // Update job list and number of active jobs
            FillJobs();
        }

        private void SaveDatePickers()
        {
            // Save any changes to Date/Time Picker's checkbox (only the checkbox);
            // changes in the dates themselves are handled in the _Validated.
            int i = lbJobs.SelectedIndex;
            if (i < 0)
                return;

            // Start
            bool updated = false;
            winployDataSet.JobsAllRow row = (winployDataSet.JobsAllRow) winployDataSet.JobsAll.DefaultView[i].Row;
            DateTime dbDate = row.ActiveStartDate.Date;
            if (dbDate != startDatePicker.Value.Date)
            {
                row.ActiveStartDate = startDatePicker.Value.Date;
                updated = true;
            }
            else if (!startDatePicker.Checked && dbDate != defaultStartDate)
            {
                row.ActiveStartDate = defaultStartDate;
                updated = true;
            }

            // Stop
            dbDate = row.ActiveStopDate.Date;
            if (stopDatePicker.Checked && dbDate != stopDatePicker.Value.Date)
            {
                row.ActiveStopDate = stopDatePicker.Value.Date;
                updated = true;
            }
            else if (!stopDatePicker.Checked && dbDate != defaultStopDate)
            {
                row.ActiveStopDate = defaultStopDate;
                updated = true;
            }

            if (updated)
                jobsAllTableAdapter.Update(row);
        }

        private void cbActiveOnly_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            FillJobs();
        }


        private void radioSortContractor_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortJobs();
        }

        private void radioSortCompany_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortJobs();
        }

        private void radioSortManager_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortJobs();
        }

        readonly DateTime defaultStartDate = new DateTime(1900, 1, 1);
        readonly DateTime defaultStopDate = new DateTime(2049, 12, 31);
    }
}
