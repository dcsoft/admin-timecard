﻿using System;
using System.Windows.Forms;
using System.Linq;
using Telerik.WinControls.UI;
using System.Data;


namespace AdminTimecard
{
    public partial class FormReportHealth : Form
    {
        public FormReportHealth()
        {
            InitializeComponent();

            Visible = false;
        }

        private void FormReportHealth_VisibleChanged(object sender, EventArgs e)
        {
            if ( Visible )
                FillGrid();
        }

        private void FillGrid()
        {
            ContractorGrid.Rows.Clear();
            W2Grid.Rows.Clear();

            // Set column header text
            DateTime now = new DateTime(2010, 11, 30);   // DateTime.Now;
            DateTime firstDayOfMonth = new DateTime(now.Year, now.Month, 1);
            int prevDays = 0;
            switch ( firstDayOfMonth.DayOfWeek)
            {
                case DayOfWeek.Sunday: prevDays = 0; break;
                case DayOfWeek.Monday: prevDays = 1; break;
                case DayOfWeek.Tuesday: prevDays = 2; break;
                case DayOfWeek.Wednesday: prevDays = 3; break;
                case DayOfWeek.Thursday: prevDays = 4; break;
                case DayOfWeek.Friday: prevDays = 5; break;
                case DayOfWeek.Saturday: prevDays = 6; break;
            }

            DateTime firstSunday = firstDayOfMonth.AddDays(-prevDays);
            DateTime week1 = firstSunday;
            DateTime week2 = week1.AddDays(7);
            DateTime week3 = week2.AddDays(7);
            DateTime week4 = week3.AddDays(7);
            DateTime week5 = week4.AddDays(7);
            DateTime week6 = week5.AddDays(7);  // workaround can't use .AddDays() in Linq query

            ContractorGrid.Columns.FindByUniqueName("week1").HeaderText = week1.ToShortDateString();
            ContractorGrid.Columns.FindByUniqueName("week2").HeaderText = week2.ToShortDateString();
            ContractorGrid.Columns.FindByUniqueName("week3").HeaderText = week3.ToShortDateString();
            ContractorGrid.Columns.FindByUniqueName("week4").HeaderText = week4.ToShortDateString();
            ContractorGrid.Columns.FindByUniqueName("week5").HeaderText = week5.ToShortDateString();

            W2Grid.Columns.FindByUniqueName("week1").HeaderText = week1.ToShortDateString();
            W2Grid.Columns.FindByUniqueName("week2").HeaderText = week2.ToShortDateString();
            W2Grid.Columns.FindByUniqueName("week3").HeaderText = week3.ToShortDateString();
            W2Grid.Columns.FindByUniqueName("week4").HeaderText = week4.ToShortDateString();
            W2Grid.Columns.FindByUniqueName("week5").HeaderText = week5.ToShortDateString();

            try
            {
                W2Grid.BeginInit();
                ContractorGrid.BeginInit();

                // Fill rows
                uint contractorNum = 1;
                uint w2Num = 1;
                uint rowNum;
                using (var context = new TimecardDbEntities(Form1.EntitiesConnectionString))
                using (var qbLoader = new QBLoader())
                {
                    RadGridView grid;
                    double contractorTotalNet = 0.0;
                    double w2TotalNet = 0.0;
                    foreach (var job in context.JobsAlls.Where(job => ((job.ActiveStartDate <= now) && (now <= job.ActiveStopDate))))
                    {
                        if (string.IsNullOrEmpty(job.VendorName))
                        {
                            grid = W2Grid;
                            rowNum = w2Num++;
                        }
                        else
                        {
                            grid = ContractorGrid;
                            rowNum = contractorNum++;
                        }

                        // Timesheet info

                        // Workaround can't use .AddDays() in Linq query
                        // var timecardsForMonth = context.Timesheets.Where(tc => ((tc.WeekEndingDate >= week1.AddDays(7)) && (tc.WeekEndingDate <= week5.AddDays(7)))).ToList();
                        double hoursWeek1 = 0.0;
                        double hoursWeek2 = 0.0;
                        double hoursWeek3 = 0.0;
                        double hoursWeek4 = 0.0;
                        double hoursWeek5 = 0.0;
                        var timecardsForMonth = job.Timesheets.Where(tc => ((tc.WeekEndingDate >= week2) && (tc.WeekEndingDate <= week6))).ToList();
                        var timecard1 = timecardsForMonth.Find(tc => tc.WeekEndingDate == week2);
                        if (timecard1 != null)
                            hoursWeek1 = timecard1.HoursTotal;

                        var timecard2 = timecardsForMonth.Find(tc => tc.WeekEndingDate == week3);
                        if (timecard2 != null)
                            hoursWeek2 = timecard2.HoursTotal;

                        var timecard3 = timecardsForMonth.Find(tc => tc.WeekEndingDate == week4);
                        if (timecard3 != null)
                            hoursWeek3 = timecard3.HoursTotal;

                        var timecard4 = timecardsForMonth.Find(tc => tc.WeekEndingDate == week5);
                        if (timecard4 != null)
                            hoursWeek4 = timecard4.HoursTotal;

                        var timecard5 = timecardsForMonth.Find(tc => tc.WeekEndingDate == week6);
                        if (timecard5 != null)
                            hoursWeek5 = timecard5.HoursTotal;

                        double purchaseCost = 0.0;
                        double salesPrice = 0.0;
                        qbLoader.GetItemRate(job.ContractorQB, out purchaseCost, out salesPrice);

                        double totalHours = hoursWeek1 + hoursWeek2 + hoursWeek3 + hoursWeek4 + hoursWeek5;
                        double netDollars = totalHours * (salesPrice - purchaseCost);
                        double netPercent = (salesPrice > 0.0) ? ((1.0 - (purchaseCost / salesPrice)) * 100) : 0.0;

                        grid.Rows.Add(rowNum,
                                        string.Format("{0} {1}", job.ContractorFirstName, job.ContractorLastName),
                                        job.ActiveStartDate.ToShortDateString(),
                                        hoursWeek1, hoursWeek2, hoursWeek3, hoursWeek4, hoursWeek5,
                                        job.CustomerCompany, job.VendorName,
                                        "",     // SR/RC field
                                        (salesPrice > 0.0) ? salesPrice.ToString() : "",
                                        (purchaseCost > 0.0) ? purchaseCost.ToString() : "",
                                        netDollars.ToString("C"),
                                        (netPercent > 0.0) ? netPercent.ToString("N0") : ""
                                        );

                        if (string.IsNullOrEmpty(job.VendorName))
                        {
                            w2TotalNet += netDollars;
                        }
                        else
                        {
                            contractorTotalNet += netDollars;
                        }
                    }

                    // Total rows
                    ContractorGrid.Rows.Add(contractorNum++,
                                    "<html><b>Total",
                                    "",
                                    0.0, 0.0, 0.0, 0.0, 0.0,
                                    "", "",
                                    "",     // SR/RC field
                                    "",
                                    "",
                                    contractorTotalNet.ToString("C"),
                                    ""
                                    );

                    W2Grid.Rows.Add(w2Num++,
                                    "<html><b>Total",
                                    "",
                                    0.0, 0.0, 0.0, 0.0, 0.0,
                                    "", "",
                                    "",     // SR/RC field
                                    "",
                                    "",
                                    w2TotalNet.ToString("C"),
                                    ""
                                    );


                    W2Grid.MasterGridViewTemplate.BestFitColumns();
                    ContractorGrid.MasterGridViewTemplate.BestFitColumns();
                }
            }
            finally
            {
                // Causes horizontal scrollbar to be recalibrated
                ContractorGrid.EndInit();
                W2Grid.EndInit();
            }
        }
    }
}
