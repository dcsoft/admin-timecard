﻿using System.Windows.Forms;


namespace AdminTimecard
{
    partial class FormTCUnsubmitted
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabelHeading = new Telerik.WinControls.UI.RadLabel();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCreateReport = new Telerik.WinControls.UI.RadButton();
            this.btnExport = new Telerik.WinControls.UI.RadButton();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabelHeading
            // 
            this.radLabelHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelHeading.Location = new System.Drawing.Point(12, 69);
            this.radLabelHeading.Name = "radLabelHeading";
            this.radLabelHeading.Size = new System.Drawing.Size(159, 17);
            this.radLabelHeading.TabIndex = 1;
            this.radLabelHeading.Text = "Please click Create Report";
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.BackColor = System.Drawing.SystemColors.Control;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(14, 91);
            // 
            // 
            // 
            this.radGridView1.MasterGridViewTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterGridViewTemplate.AllowCellContextMenu = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnChooser = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnReorder = false;
            this.radGridView1.MasterGridViewTemplate.AllowColumnResize = false;
            this.radGridView1.MasterGridViewTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterGridViewTemplate.AllowDragToGroup = false;
            this.radGridView1.MasterGridViewTemplate.AllowEditRow = false;
            this.radGridView1.MasterGridViewTemplate.AllowRowResize = false;
            this.radGridView1.MasterGridViewTemplate.EnableGrouping = false;
            this.radGridView1.MasterGridViewTemplate.EnableSorting = false;
            this.radGridView1.MultiSelect = true;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.ReadOnly = true;
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radGridView1.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.Size = new System.Drawing.Size(780, 428);
            this.radGridView1.TabIndex = 2;
            this.radGridView1.Text = "radGridViewPreview";
            this.radGridView1.ThemeName = "ControlDefault";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnCreateReport);
            this.groupBox2.Controls.Add(this.btnExport);
            this.groupBox2.Controls.Add(this.datePicker);
            this.groupBox2.Controls.Add(this.radLabel4);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(782, 51);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Show unsubmitted timecards as of";
            // 
            // btnCreateReport
            // 
            this.btnCreateReport.Location = new System.Drawing.Point(187, 20);
            this.btnCreateReport.Name = "btnCreateReport";
            this.btnCreateReport.Size = new System.Drawing.Size(115, 21);
            this.btnCreateReport.TabIndex = 2;
            this.btnCreateReport.Text = "Create Report";
            this.btnCreateReport.ThemeName = "Office2007Black";
            this.btnCreateReport.Click += new System.EventHandler(this.btnCreateReport_Click);
            // 
            // btnExport
            // 
            this.btnExport.Enabled = false;
            this.btnExport.Location = new System.Drawing.Point(308, 20);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(109, 21);
            this.btnExport.TabIndex = 3;
            this.btnExport.Text = "Export Report ...";
            this.btnExport.ThemeName = "Office2007Black";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // datePicker
            // 
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(86, 20);
            this.datePicker.MaxDate = new System.DateTime(2049, 12, 31, 0, 0, 0, 0);
            this.datePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(95, 21);
            this.datePicker.TabIndex = 1;
            this.datePicker.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(6, 20);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(76, 17);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Report Date";
            // 
            // FormTCUnsubmitted
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 558);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.radLabelHeading);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormTCUnsubmitted";
            this.Text = "FormTCUnsubmitted";
            this.Load += new System.EventHandler(this.FormTCUnsubmitted_Load);
            this.VisibleChanged += new System.EventHandler(this.FormTCUnsubmitted_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreateReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabelHeading;
        private Telerik.WinControls.UI.RadGridView radGridView1;

        private GroupBox groupBox2;
        private Telerik.WinControls.UI.RadButton btnCreateReport;
        private Telerik.WinControls.UI.RadButton btnExport;
        private DateTimePicker datePicker;
        private Telerik.WinControls.UI.RadLabel radLabel4;
    }
}